.game_achievement
{
	margin: 3px;
	padding: 5px;
	background: #DDD;
	position: relative;
	min-height: 114px;
}

.game_achievement:hover
{
	background: #E6E6E6;
}

.game_achievement h1
{
	font-size: 14px;
}

.game_achievement h2
{
	font-size: 12px;
	margin-bottom: 0px;
}

.game_achievement_info
{
	margin-left: 114px;
	margin-right: 100px;
	text-align: justify;
}

.achievement_list_icon_tiny
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 25px;
	height: 25px;
}

.achievement_list_icon_small
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 40px;
	height: 40px;
}

.achievement_list_icon_medium
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 100px;
	height: 100px;
}

.achievement_list_icon_large
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 200px;
	height: 200px;
}

.achievement_list_icon
{
	position: absolute;
}

.achievement_list_icon_shadow
{
	position: absolute;
	top: 0px;
	right: 0px;
	width: 100%;
	height: 100%;
	background-color: rgba(255,255,255,0.8);
}

.game_achievement_reqlist
{
	margin: 0px;
}

.game_achievement_reqlist_req
{
	margin-right: 2px;
	position: relative;
	display: inline-block;
}

.game_achievement_reqlist_req_hint
{
	background: rgba(0,0,0,0.9);
	position: absolute;
	top: 20px;
	left: 20px;
	color: white;
	padding: 4px;
	line-height: 12px;
	z-index: 999;
	width: 300px;
	display: none;
}

.game_achievement_reqlist_req_hint h1
{
	color: white;
	font-size: 11px;
	margin: 0px;
	margin-bottom: 2px;
}

.game_achievement_reqlist_req_hint p
{
	color: white;
	font-size: 10px;
	margin: 0px;
}

.game_achievement_admin_buttons
{
	position: absolute;
	right: 36px;
	bottom: 6px;
	clear: both;
}

.game_achievement_admin_edit
{
	background: url('<?php echo(elgg_get_site_url()) ?>mod/kpax_achievements/graphics/edit.png') no-repeat;
	width: 16px;
	height: 16px;
	float: right;
	margin: 4px;
}

.game_achievement_admin_delete
{
	background: url('<?php echo(elgg_get_site_url()) ?>mod/kpax_achievements/graphics/delete.png') no-repeat;
	width: 16px;
	height: 16px;
	float: right;
	margin: 4px;
}

.game_achievement_progress_bar
{
	width: 204px;
	height: 20px;
	background: white;
	border: 1px solid #AAA;
	position: relative;
}

.game_achievement_progress_bar_progress
{
	height: 16px;
	width: 100px;
	position: absolute;
	top: 2px;
	left: 2px;
	background-color: #c5f87d;
}

.game_achievement_progress_bar_percent
{
	position: absolute;
	right: 4px;
	top: 1px;
	font-size: 9px;
	font-color: #444;
	font-weight: bold;
}

.game_achievement_percentusers_bar
{
	width: 20px;
	height: 104px;
	background: white;
	border: 1px solid #AAA;
	position: absolute;
	top: 8px;
	right: 8px;
}

.game_achievement_percentusers_bar_progress
{
	height: 100px;
	width: 16px;
	position: absolute;
	top: 2px;
	left: 2px;
	background-color: #8dccfd;
}

.game_achievement_percentusers_bar_percent
{
	position: absolute;
	top: 0px;
	right: 26px;
	font-size: 9px;
	font-color: #444;
	font-weight: bold;
	text-align: right;
	white-space: nowrap;
	line-height: 12px;
}

select
{
	border: 1px solid rgb(204, 204, 204);
	color: rgb(102, 102, 102);
	font-family: "Trebuchet MS",Arial,Tahoma,Verdana,sans-serif;
	font-size: 1.1em;
	color: rgb(102, 102, 102);
	border: 1px solid rgb(204, 204, 204);
	padding: 5px;
	width: 100%;
	border-radius: 0px;
	-moz-box-sizing: border-box;
}

#achievement_icon_tocrop
{
	display: inline-block;
	border: 1px solid #AAA;
	padding: 2px;
}

#achievement_icon
{
	padding: 0px;
	margin: 0px;
}

#achievement_icon_preview
{
	display: inline-block;
	padding: 2px;
	border: 1px solid #AAA;
}

#achievement_icon_preview_container
{
	width: 100px;
	height: 100px;
	padding: 0px;
	overflow: hidden;
	background: black;
}

#achievement_icon_preview_container img
{
	display: none;
}