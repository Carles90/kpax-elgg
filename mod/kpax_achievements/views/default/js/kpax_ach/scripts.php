var reqs_iter = 1;
function addAchNewRequirement(){
	var sample_div = document.getElementById('add_requeriment_sample');
	var reqs_div = document.getElementById('add_requeriment_inputs');

	var sample_content = sample_div.innerHTML.split('%REQNUM%').join(reqs_iter);
	var new_div = document.createElement('div');
	new_div.id = 'ach_requeriment_' + reqs_iter;
	new_div.innerHTML = '<br/>' + sample_content;
	reqs_div.appendChild(new_div);

	reqs_iter++;
}

function deleteAchRequirement(id){
	var to_delete = document.getElementById('ach_requeriment_' + id);
	to_delete.parentNode.removeChild(to_delete);
}

function showAchievementHint(id){
	$("#achievement_hint_" + id).show();
}

function hideAchievementHint(id){
	$("#achievement_hint_" + id).hide();
}

function deleteAchievement(id){
	if(confirm("<?php echo(elgg_echo('kpax_ach:delform_ach_sure')) ?>")){
		window.location = "<?php echo(elgg_get_site_url()) ?>kpax_ach/delete_achievement/" + id;
	}
}