<?php
$guid = (int)get_input('guid');

$username = elgg_get_logged_in_user_entity()->username;

$objKpax = new kpaxSrv($username);
$achList = $objKpax->getGameAchievements($guid);

echo elgg_view('input/hidden', array('name' => 'guid', 'value' => $guid));
?>

<div>
	<label><?php echo(elgg_echo('kpax_ach:addform_name_title')); ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'ach_name'));
	?>
	<p><?php echo(elgg_echo('kpax_ach:addform_name_description')); ?></p>
</div>
<div>
	<label><?php echo(elgg_echo('kpax_ach:addform_description_title')); ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'ach_desc'));
	?>
	<p><?php echo(elgg_echo('kpax_ach:addform_description_description')); ?></p>
</div>
<div>
	<label><?php echo(elgg_echo('kpax_ach:addform_image_title')); ?></label><br/>
	<?php
	echo elgg_view('input/file', array('name' => 'ach_icon'));
	?>
	<p><?php echo(elgg_echo('kpax_ach:addform_image_description')); ?></p>
</div>
<div>
	<label><?php echo(elgg_echo('kpax_ach:addform_maxlevel_title')); ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'ach_maxlevel', 'value' => 0));
	?>
	<p><?php echo(elgg_echo('kpax_ach:addform_maxlevel_description')); ?></p>
</div>
<div id="add_requeriment_info">
	<label><?php echo(elgg_echo('kpax_ach:addform_addreq_title')); ?></label><br/>
	<p><?php echo(elgg_echo('kpax_ach:addform_addreq_description')); ?></p>
</div>
<div id="add_requeriment_sample" style="display:none;">
	<label><?php echo(elgg_echo('kpax_ach:addform_addreq_addreq')); ?> %REQNUM% (<a href="javascript:void(0)" onclick="deleteAchRequirement(%REQNUM%)"><?php echo(elgg_echo('kpax_ach:addform_reqdelete')) ?></a>)</label><br/>
	<?php
	$options = array();
	foreach($achList as $ach){
		$options[$ach->idAchievement] = '['.$ach->idAchievement.'] '.$ach->name;
	}
	echo elgg_view('input/dropdown', array('name' => 'ach_addreq_%REQNUM%', 'options_values' => $options));
	?>
</div>
<div id="add_requeriment_inputs">
</div>
<div>
	<p><a href="javascript:void(0)" onclick="addAchNewRequirement()"><?php echo(elgg_echo('kpax_ach:addform_addreq_addlink')); ?></a></p>
</div>
<div>
<?php
echo elgg_view('input/submit', array('value' => elgg_echo('kpax_ach:addform_create')));
?>
</div>
