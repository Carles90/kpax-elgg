<?php
$guid = (int)get_input('guid');

$username = elgg_get_logged_in_user_entity()->username;

$objKpax = new kpaxSrv($username);

$achId = (int)get_input('achid');
$objAch = $objKpax->getAchievement($achId);
$achList = $objKpax->getGameAchievements($objAch->idGame);

echo elgg_view('input/hidden', array('name' => 'achid', 'value' => $achId));
echo elgg_view('input/hidden', array('name' => 'form_action', 'value' => 'edit'));
echo elgg_view('input/hidden', array('name' => 'action', 'value' => 'kpax_ach/add_ach'))
?>

<div>
	<label><?php echo(elgg_echo('kpax_ach:addform_name_title')); ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'ach_name', 'value' => $objAch->name));
	?>
	<p><?php echo(elgg_echo('kpax_ach:addform_name_description')); ?></p>
</div>
<div>
	<label><?php echo(elgg_echo('kpax_ach:addform_description_title')); ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'ach_desc', 'value' => $objAch->description));
	?>
	<p><?php echo(elgg_echo('kpax_ach:addform_description_description')); ?></p>
</div>
<div>
	<label><?php echo(elgg_echo('kpax_ach:addform_image_title')); ?></label><br/>
	<?php
	echo elgg_view('kpax_ach/game/ach_icon', array('achId' => $objAch->idAchievement, 'size' => 'medium', 'hasIt' => true));
	echo('<div class="clear"></div><br/>');
	echo elgg_view('input/file', array('name' => 'ach_icon'));
	?>
	<p><?php echo(elgg_echo('kpax_ach:editform_image_info')); ?></p>
</div>
<div>
	<label><?php echo(elgg_echo('kpax_ach:addform_maxlevel_title')); ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'ach_maxlevel', 'value' => $objAch->maxLevel));
	?>
	<p><?php echo(elgg_echo('kpax_ach:addform_maxlevel_description')); ?></p>
</div>
<div id="add_requeriment_info">
	<label><?php echo(elgg_echo('kpax_ach:addform_addreq_title')); ?></label><br/>
	<p><?php echo(elgg_echo('kpax_ach:addform_addreq_description')); ?></p>
</div>
<div id="add_requeriment_sample" style="display:none;">
	<label><?php echo(elgg_echo('kpax_ach:addform_addreq_addreq')); ?> %REQNUM% (<a href="javascript:void(0)" onclick="deleteAchRequirement(%REQNUM%)"><?php echo(elgg_echo('kpax_ach:addform_reqdelete')) ?></a>)</label><br/>
	<?php
	$options = array();
	foreach($achList as $ach){
		$options[$ach->idAchievement] = '['.$ach->idAchievement.'] '.$ach->name;
	}
	echo elgg_view('input/dropdown', array('name' => 'ach_addreq_%REQNUM%', 'options_values' => $options));
	?>
</div>
<div id="add_requeriment_inputs">
	<?php
	$achReqs = $objKpax->getAchievementRequirements($objAch->idAchievement);
	$reqnum = 1;
	foreach($achReqs as $req)
	{
		?>
		<div id="ach_requeriment_<?php echo($reqnum) ?>">
			<br/>
			<label><?php echo(elgg_echo('kpax_ach:addform_addreq_addreq')); ?> <?php echo($reqnum) ?> (<a href="javascript:void(0)" onclick="deleteAchRequirement(<?php echo($reqnum) ?>)"><?php echo(elgg_echo('kpax_ach:addform_reqdelete')) ?></a>)</label><br/>
			<script type="text/javascript">
			reqs_iter++;
			</script>
			<?php
			$options = array();
			foreach($achList as $ach){
				if($req->idAchievement != $ach->idAchievement)
				{
					$options[$ach->idAchievement] = '['.$ach->idAchievement.'] '.$ach->name;
				}
			}
			echo elgg_view('input/dropdown', array('name' => 'ach_addreq_'.$reqnum, 'options_values' => $options, 'value' => $req->requires));

			$reqnum++;
			?>
		</div>
		<?php
	}
	?>
</div>
<div>
	<p><a href="javascript:void(0)" onclick="addAchNewRequirement()"><?php echo(elgg_echo('kpax_ach:addform_addreq_addlink')); ?></a></p>
</div>
<div>
<?php
echo elgg_view('input/submit', array('value' => elgg_echo('kpax_ach:editform_edit')));
?>
</div>
