<?php
$level = $vars['level'];
$maxLevel = $vars['maxLevel'];
$percent = ($level / $maxLevel) * 100;

$pct = kpaxNumberFormat($percent, 2)
?>

<div class="game_achievement_progress_bar">
	<div class="game_achievement_progress_bar_progress" style="width: <?php echo($percent * 2) ?>px;"></div>
	<div class="game_achievement_progress_bar_percent"><?php echo($pct) ?>%</div>
</div>