<?php
$size = $vars['size'];
$achId = $vars['achId'];
$hasIt = $vars['hasIt'];

$data_root = kpaxGetDataRoot();
$file = $data_root.'achievements/'.$achId.$size.'.png';

?>

<div class="achievement_list_icon_<?php echo($size) ?>">
	<div class="achievement_list_icon_shadow" <?php echo($hasIt ? 'style="display: none;"' : '') ?>></div>
	<?php
	//Comprovar si l'assoliment poseeix icona
	if(file_exists($file)){
		//Mostrar icona de l'assoliment
		echo('<img src="'.elgg_get_site_url().'mod/kpax_achievements/icondirect.php?achid='.$achId.'&amp;size='.$size.'" alt="Achievement Avatar" />');
	}
	else
	{
		//Mostrar icona per defecte
		echo('<img src="'.elgg_get_site_url().'mod/kpax_achievements/graphics/default_'.$size.'.png" alt="Achievement Avatar"/>');
	}
	?>
</div>