<?php

$guid = (int)get_input('guid');
$kpax = get_entity($guid);
$username = elgg_get_logged_in_user_entity()->username;

$objKpax = new kpaxSrv($username);

//Recuperar la llista de tots els assoliments del joc
$achList = $objKpax->getGameAchievements($guid);
$allAch = array();
foreach($achList as $ach)
{
	$allAch[$ach->idAchievement] = $ach;

	//Indicar que per defecte aquest usuari no poseeix l'assoliment posarlo a nivell 0 i 
	//donar una llista buida de requeriments
	$ach->having = false;
	$ach->level = 0;
	$ach->reqs = array();
	$ach->usershaving = 0;
}

//Recuperar la llista de tots els assoliments que poseeix l'usuari
$userAchList = $objKpax->getUserAchievementsForGame($guid, $username);


//Recorrer la llista d'assoliments que té l'usuari per a marcar amb un flag els que ja poseeix
foreach($userAchList as $uach)
{
	$allAch[$uach->idAchievement]->having = true;
	$allAch[$uach->idAchievement]->level = $uach->level;
}

//Recuperar la llista de tots els requeriments per a cada assoliment
$allReqList = $objKpax->getAllAchievementRequirements();
foreach($allReqList as $req)
{
	if(isset($allAch[$req->idAchievement]) && isset($allAch[$req->requires]))
	{
		array_push($allAch[$req->idAchievement]->reqs, $allAch[$req->requires]);
	}
}

//Mostrar botó per afegir un nou assoliment
if($kpax->canEdit())
{
	$allz = elgg_view('input/submit', array('value' => elgg_echo('kpax_ach:achievements_add_button')));
	echo elgg_view('input/form', array('body' => $allz, 'action' => "kpax_ach/add/$guid"));
}

//Recuperar la llista d'usuaris que juguen a aquest joc
$playingUsers = $objKpax->getPlayingUsers($guid);
$playerNum = count($playingUsers);

//Recuperar la llista d'usuaris que tenen cada assoliment
$userAch = $objKpax->getAllUserAchievement();
foreach($userAch as $ua)
{
	if(isset($allAch[$ua->idAchievement]))
	{
		$allAch[$ua->idAchievement]->usershaving++;
	}
}

//Mostrar la llista d'assoliments
$totalhints = 0;
foreach($allAch as $ach)
{

	?>
	<div class="game_achievement">
		<div class="achievement_list_icon">
			<?php
				echo(elgg_view('kpax_ach/game/ach_icon', array('achId' => $ach->idAchievement, 'size' => 'medium', 'hasIt' => $ach->having)));
			?>
		</div>
		<div class="game_achievement_info">
			<h1>
				<?php
				if($kpax->canEdit())
				{
					echo('[ID: '.$ach->idAchievement.'] ');
				}
				echo($ach->name);
				if($ach->maxLevel > 0)
				{
					echo(' ('.kpaxNumberFormat($ach->level).'/'.kpaxNumberFormat($ach->maxLevel).')');
				}
				?>
			</h1>
			<p><?php echo($ach->description) ?></p>
			<?php
			
			if($ach->maxLevel > 0)
			{
				echo('<h2>'.elgg_echo('kpax_ach:list_progress').'</h2>');
				echo(elgg_view('kpax_ach/game/ach_progress', array('level' => $ach->level, 'maxLevel' => $ach->maxLevel)));
			}

			echo(elgg_view('kpax_ach/game/ach_percentusers', array('users' => $ach->usershaving, 'totalUsers' => $playerNum)));

			if(count($ach->reqs) > 0)
			{
				?>
				<h2><?php echo(elgg_echo('kpax_ach:list_requirements')) ?></h2>
				<div class="game_achievement_reqlist">
					<?php
					foreach($ach->reqs as $req)
					{
						?>
						<div class="game_achievement_reqlist_req" onmouseover="showAchievementHint(<?php echo($totalhints) ?>)" onmouseout="hideAchievementHint(<?php echo($totalhints) ?>)">
							<?php echo(elgg_view('kpax_ach/game/ach_icon', array('achId' => $req->idAchievement, 'size' => 'tiny', 'hasIt' => $req->having))); ?>
							<div class="game_achievement_reqlist_req_hint" id="achievement_hint_<?php echo($totalhints) ?>">
								<h1><?php echo($req->name) ?></h1>
								<p><?php echo($req->description) ?></p>
							</div>
						</div>
						<?php
						$totalhints++;
					}
					?>
				</div>
				<?php
			}
			?>
		</div>
		<?php
		if($kpax->canEdit())
		{
			?>
			<div class="game_achievement_admin_buttons">
				<a href="javascript:void(0)" onclick="deleteAchievement(<?php echo($ach->idAchievement) ?>)"><div class="game_achievement_admin_delete" title="<?php echo(elgg_echo('kpax_ach:list_delete_button')); ?>"></div></a>
				<a href="<?php echo(elgg_get_site_url()) ?>kpax_ach/edit_achievement/<?php echo($ach->idAchievement) ?>"><div class="game_achievement_admin_edit" title="<?php echo(elgg_echo('kpax_ach:list_edit_button')); ?>"></div></a>
			</div>
			<?php
		}
		?>
	</div>
	<?php
}
?>