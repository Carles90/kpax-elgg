<?php
$achid = (int)get_input('achid');

$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$ach = $objKpax->getAchievement($achid);

if($ach != null){
	$kpax = get_entity($ach->idGame);

	if($kpax->canEdit()){
		echo elgg_view_form('kpax_ach/crop_icon', array('name' => 'kpax_icon_croping'));
	}
	else{
		echo(elgg_echo('kpax_ach:addform_ach_invalid_user'));
	}
}
else{
	echo(elgg_echo('kpax_ach:addform_ach_invalid_user'));
}
?>