<?php
$achId = (int)get_input('achid');

$username = elgg_get_logged_in_user_entity()->username;
$objKpax = new kpaxSrv($username);

$objAch = $objKpax->getAchievement($achId);

if($objAch == null)
{
	echo(elgg_echo('kpax_ach:editform_ach_not_exists'));
}
else
{
	$kpax = get_entity($objAch->idGame);

	if($kpax->canEdit())
	{
		echo elgg_view_form('kpax_ach/edit_ach', array('enctype' => 'multipart/form-data'));
	}
	else
	{
		echo(elgg_echo('kpax_ach:editform_invalid_user'));
	}
}
?>