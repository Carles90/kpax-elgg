<?php
$users = $vars['users'];
$totalUsers = $vars['totalUsers'];
$percent = min(($users / $totalUsers) * 100, 100);
$rpercent = round($percent);

$pct = kpaxNumberFormat($percent, 2)
?>

<div class="game_achievement_percentusers_bar">
	<div class="game_achievement_percentusers_bar_progress" style="height: <?php echo($rpercent) ?>px; top: <?php echo(100 + 2 - $rpercent) ?>px;"></div>
	<div class="game_achievement_percentusers_bar_percent"><?php echo(elgg_echo('kpax_ach:list_having')) ?><br/><?php echo($pct) ?>%</div>
</div>