<?php

$guid = (int)get_input('guid');
$kpax = get_entity($guid);

if($kpax->canEdit())
{
	echo elgg_view_form('kpax_ach/add_ach', array('enctype' => 'multipart/form-data'));
}
else
{
	echo(elgg_echo('kpax_ach:achievements_cannot_edit'));
}
?>