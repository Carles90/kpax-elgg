<?php

elgg_register_event_handler('init', 'system', 'kpax_ach_init');

function kpax_ach_init(){
	$root = dirname(__FILE__);

	elgg_register_page_handler('kpax_ach', 'kpax_ach_page_handler');

	//CSS
	elgg_extend_view('css/elgg', 'css/kpax_ach/style');

	//Javascript
	elgg_register_simplecache_view('js/kpax_ach/scripts');
	$url = elgg_get_simplecache_url('js', 'kpax_ach/scripts');
	elgg_register_js('ach_scripts', $url);

	//Libs
	elgg_register_library('elgg:kpax_ach', "$root/lib/functions.php");
	elgg_load_library('elgg:kpax_ach');

	// actions
    $action_path = "$root/actions/kpax_ach";
	elgg_register_action('kpax_ach/add_ach', "$action_path/add_ach.php");
	elgg_register_action('kpax_ach/crop_icon', "$action_path/crop_icon.php");
	elgg_register_action('kpax_ach/delete_ach', "$action_path/delete_ach.php");
}


function kpax_ach_page_handler($page){
	$pages = dirname(__FILE__) . '/pages/kpax_ach';

	elgg_push_breadcrumb(elgg_echo('kPAX:play'), 'kpax/play');
	elgg_load_js('ach_scripts');

	switch ($page[0]) {
		case "add":
			set_input('guid', $page[1]);
			include("$pages/add.php");
		break;
		case "crop_icon":
			set_input('achid', $page[1]);
			include("$pages/crop_icon.php");
		break;
		case "delete_achievement":
			set_input('achid', $page[1]);
			include("$pages/delete_achievement.php");
		break;
		case "edit_achievement":
			set_input('achid', $page[1]);
			include("$pages/edit_achievement.php");
		break;
	}

	return true;
}
?>