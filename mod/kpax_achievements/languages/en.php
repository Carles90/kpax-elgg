<?php
/**
 * Bookmarks Spanish language file
 */

$english = array(
	/**
	 * Main
	 */

	'kpax_ach:achievements_add_button' => 'Add Achievement',
	'kpax_ach:achievements_cannot_edit' => 'Sorry, but you cannot edit the achievements of this game.',
	'kpax_ach:achievements_add_title' => 'Add achievement for the game',

	/**
	 * Achievement Add Form
	 */
	'kpax_ach:addform_name_title' => 'Achievement name',
	'kpax_ach:addform_name_description' => 'Write down the name of the achievement that you are creating.',
	'kpax_ach:addform_description_title' => 'Achievement description',
	'kpax_ach:addform_description_description' => 'Add a brief description for this achievement.',
	'kpax_ach:addform_image_title' => 'Achievement icon',
	'kpax_ach:addform_image_description' => 'Add an small image that will be the achievement icon.',
	'kpax_ach:addform_maxlevel_title' => 'Maximum score',
	'kpax_ach:addform_maxlevel_description' => 'Numeric value of the maximum score that can win this achievement. If this achievement is not an score achievement, leave this field in 0 value.',
	'kpax_ach:addform_addreq_title' => 'Add requirements',
	'kpax_ach:addform_addreq_description' => 'In order to add a new requirement to earn this achievement click on the link that you will found below. You can add the number of requirements that you want. The user cannot earn this achievement until he has obtained all the requirements.',
	'kpax_ach:addform_addreq_addreq' => 'New requirement',
	'kpax_ach:addform_addreq_addlink' => '(+) Add requirement',
	'kpax_ach:addform_create' => 'Create Achievement',
	'kpax_ach:addform_reqdelete' => 'Delete',
	'kpax_ach:addform_success' => 'Achievement added:',
	'kpax_ach:addform_ach_submit_error' => 'Unknown error adding an achievement:',
	'kpax_ach:addform_ach_submit_invalid_session' => 'Error! You can\'t add this achievement because your session has not been validated.',
	'kpax_ach:addform_ach_invalid_user' => 'You can\'t add achievements to this game because it doesn\'t belong to you.',
	'kpax_ach:addform_req_error_null_achievements' => 'Error! One of the requirements that you wanted to add doesn\'t exist.',
	'kpax_ach:addform_req_error_different_games' => 'Error! One of the selected requirements belongs to another game.',
	'kpax_ach:addform_req_error_recursive_requirement' => 'Error! One achievement cannot be required by himself.',
	'kpax_ach:addform_req_error_invalid_session' => 'Error! You cannot add this requirement because your session has not been validated.',
	'kpax_ach:addform_req_error_unknown' => 'Unknown error adding a requirement:',
	'kpax_ach:addform_icon_failed' => 'We have detected an error with the achievement icon. This achievement has been created, but it will show a default image until you change it.',
	'kpax_ach:addform_name_failed' => 'You have not written any name for this achievement.',
	'kpax_ach:addform_desc_failed' => 'You have not written any description for this achievement.',
	'kpax_ach:addform_crop_success' => 'Achievement icon successfully cropped.',
	'kpax_ach:addform_crop_areaerror' => 'Error! The area of the achievement must be an square.',
	'kpax_ach:addform_icon_error_invalid' => 'The image that you have sent was invalid.',
	'kpax_ach:addform_icon_error_not_converted' => 'The image that you have sent cannot be converted.',

	/**
	 * Crop Image Form
	 */
	'kpax_ach:cropform_title' => 'Crop the achievement icon',
	'kpax_ach:cropform_explain' => 'This is the icon that you have sent from this achievement, but now, you should crop the zone of the image that will be the final icon. Please, select an squared area on the image that you\'ll found below.',
	'kpax_ach:cropform_ach_to_crop' => 'Achievement image to crop',
	'kpax_ach:cropform_preview' => 'Preview',
	'kpax_ach:cropform_crop' => 'Crop it',

	/**
	 * Achievement List
	 */
	'kpax_ach:list_edit_button' => 'Edit Achievement',
	'kpax_ach:list_delete_button' => 'Remove Achievement',
	'kpax_ach:list_progress' => 'Progress',
	'kpax_ach:list_having' => 'People having:',
	'kpax_ach:list_requirements' => 'If you want it, you need...',

	/**
	 * Achievement Delete Form
	 */
	'kpax_ach:delform_ach_invalid_user' => 'We are sorry, you do not have permission to delete this achievement.',
	'kpax_ach:delform_ach_invalid_ach' => 'The selected achievement doesn\'t exist and it can\'t be deleted.',
	'kpax_ach:delform_ach_sure' => 'Are you sure that you want to delete this achievement?',
	'kpax_ach:delform_ach_success' => 'Achievement successfully deleted.',
	'kpax_ach:delform_ach_unknown_error' => 'Unknown error while it was removing an achievement: ',

	/**
	 * Achievement Edit Form
	 */
	'kpax_ach:editform_ach_not_exists' => 'The achievement that you have selected doesn\'t exist.',
	'kpax_ach:editform_invalid_user' => 'We\'re sorry, you do not have permission to edit this achievement.',
	'kpax_ach:achievements_edit_title' => 'Achievement edit',
	'kpax_ach:editform_image_info' => 'Add here a file only if you want to change the achievement icon. Else leave this field in blank.',
	'kpax_ach:editform_edit' => 'Edit Achievement',
	'kpax_ach:editform_success' => 'Achievement successfully edit:'
	);

add_translation('en', $english);
?>