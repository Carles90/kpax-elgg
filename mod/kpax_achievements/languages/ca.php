<?php
/**
 * Bookmarks Catalan language file
 */

$catalan = array(
	/**
	 * Main
	 */

	'kpax_ach:achievements_add_button' => 'Afegir Assoliment',
	'kpax_ach:achievements_cannot_edit' => 'Ho sentim, no tens permís per a editar els assoliments d\'aquest joc.',
	'kpax_ach:achievements_add_title' => 'Afegir un assoliment per al joc',

	/**
	 * Achievement Add Form
	 */
	'kpax_ach:addform_name_title' => 'Nom de l\'assoliment',
	'kpax_ach:addform_name_description' => 'Escriu aquí el nom que hauria de tenir l\'assoliment que estàs creant.',
	'kpax_ach:addform_description_title' => 'Descripció de l\'assoliment',
	'kpax_ach:addform_description_description' => 'Afegeix una descripció curta per a aquest assoliment.',
	'kpax_ach:addform_image_title' => 'Icona de l\'assoliment',
	'kpax_ach:addform_image_description' => 'Afegeix una petita imatge que consistirà en la icona d\'aquest assoliment.',
	'kpax_ach:addform_maxlevel_title' => 'Puntuació màxima',
	'kpax_ach:addform_maxlevel_description' => 'Valor numèric de puntuació màxima que pot arribar a tenir aquest assoliment. Si l\'assoliment no és un assoliment per puntuació, deixar aquest camp amb el valor de 0.',
	'kpax_ach:addform_addreq_title' => 'Afegir requeriments',
	'kpax_ach:addform_addreq_description' => 'Per a afegir un nou requeriment per tal d\'aconseguir aquest assoliment fes clic en el link que hi a més avall. Podràs afegir tants requeriments com sigui necessari. L\'usuari no podrà aconseguir aquest assoliment fins que no hagi aconseguit tots els requeriments determinats.',
	'kpax_ach:addform_addreq_addreq' => 'Nou requeriment',
	'kpax_ach:addform_addreq_addlink' => '(+) Afegir requeriment',
	'kpax_ach:addform_create' => 'Crear Assoliment',
	'kpax_ach:addform_reqdelete' => 'Eliminar',
	'kpax_ach:addform_success' => 'Assoliment afegit correctament:',
	'kpax_ach:addform_ach_submit_error' => 'Error desconegut afegint un assoliment:',
	'kpax_ach:addform_ach_submit_invalid_session' => 'Error! No pots afegir assoliments perquè la teva sessió no s\'ha validat correctament.',
	'kpax_ach:addform_ach_invalid_user' => 'No pots afegir assoliments a aquest joc perquè no et pertany.',
	'kpax_ach:addform_req_error_null_achievements' => 'Error! Un dels requeriments que volies assignar a aquest assoliment no existeix.',
	'kpax_ach:addform_req_error_different_games' => 'Error! Un dels requeriments associats pertany a un altre joc.',
	'kpax_ach:addform_req_error_recursive_requirement' => 'Error! Un assoliment no pot tenir-se a ell mateix com a requeriment.',
	'kpax_ach:addform_req_error_invalid_session' => 'Error! No pots afegir requeriments a aquest assoliment perquè la teva sessió no ha estat validada.',
	'kpax_ach:addform_req_error_unknown' => 'Error desconegut al intentar associar un requeriment:',
	'kpax_ach:addform_icon_failed' => 'Hem detectat que s\'ha produit un error al tractar amb la icona del avatar. Aquest assoliment s\'ha creat, però mostrarà la imatge per defecte fins que aquesta sigui canviada.',
	'kpax_ach:addform_name_failed' => 'No has escrit cap nom per a aquest assoliment.',
	'kpax_ach:addform_desc_failed' => 'No has escrit cap descripció per a aquest assoliment.',
	'kpax_ach:addform_crop_success' => 'Icona de l\'assoliment retallada correctament.',
	'kpax_ach:addform_crop_areaerror' => 'Error! L\'àrea de la icona no és quadrada.',
	'kpax_ach:addform_icon_error_invalid' => 'La imatge enviada no era vàlida.',
	'kpax_ach:addform_icon_error_not_converted' => 'La imatge enviada no s\'ha pogut convertir.',

	/**
	 * Crop Image Form
	 */
	'kpax_ach:cropform_title' => 'Retalla l\'icona de l\'assoliment',
	'kpax_ach:cropform_explain' => 'Aquesta és la icona per a l\'assoliment que ens has enviat, però ara cal retallar la zona de la imatge que consistirà finalment en la icona d\'aquest assoliment. Si us plau, selecciona una àrea quadrada sobre la imatge de sota.',
	'kpax_ach:cropform_ach_to_crop' => 'Imatge de l\'assoliment a retallar',
	'kpax_ach:cropform_preview' => 'Visualització Prèvia',
	'kpax_ach:cropform_crop' => 'Retalla-ho',

	/**
	 * Achievement List
	 */
	'kpax_ach:list_edit_button' => 'Editar Assoliment',
	'kpax_ach:list_delete_button' => 'Eliminar Assoliment',
	'kpax_ach:list_progress' => 'Progrés',
	'kpax_ach:list_having' => 'En propietat:',
	'kpax_ach:list_requirements' => 'Per a aconseguir-lo, abans necessites...',

	/**
	 * Achievement Delete Form
	 */
	'kpax_ach:delform_ach_invalid_user' => 'Ho sentim, no tens permís per a eliminar aquest assoliment.',
	'kpax_ach:delform_ach_invalid_ach' => 'L\'assoliment seleccionat no existeix i per tant no s\'ha pogut eliminar.',
	'kpax_ach:delform_ach_sure' => 'Segur que vols eliminar aquest assoliment?',
	'kpax_ach:delform_ach_success' => 'Aquest assoliment s\'ha eliminat correctament.',
	'kpax_ach:delform_ach_unknown_error' => 'Error desconegut mentre s\'eliminava un assoliment: ',

	/**
	 * Achievement Edit Form
	 */
	'kpax_ach:editform_ach_not_exists' => 'L\'assoliment seleccionat no existeix.',
	'kpax_ach:editform_invalid_user' => 'Ho sentim, no tens permís per a editar aquest assoliment.',
	'kpax_ach:achievements_edit_title' => 'Edita un assoliment',
	'kpax_ach:editform_image_info' => 'Afegeix només un arxiu aquí si vols modificar la imatge. Sino deixa aquest camp en blanc.',
	'kpax_ach:editform_edit' => 'Editar Assoliment',
	'kpax_ach:editform_success' => 'Assoliment editat correctament:'
	);

add_translation('ca', $catalan);
?>