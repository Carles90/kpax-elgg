<?php
/**
 * Bookmarks Spanish language file
 */

$spanish = array(
	/**
	 * Main
	 */

	'kpax_ach:achievements_add_button' => 'Añadir Logro',
	'kpax_ach:achievements_cannot_edit' => 'Lo sentimos, no tienes permiso para editar los logros de este juego.',
	'kpax_ach:achievements_add_title' => 'Añadir logro para el juego',

	/**
	 * Achievement Add Form
	 */
	'kpax_ach:addform_name_title' => 'Nombre del logro',
	'kpax_ach:addform_name_description' => 'Escribe aquí el nombre que debería tener el logro que estás creando.',
	'kpax_ach:addform_description_title' => 'Descripción del logro',
	'kpax_ach:addform_description_description' => 'Añade una pequeña descripción para este logro.',
	'kpax_ach:addform_image_title' => 'Icono del logro',
	'kpax_ach:addform_image_description' => 'Añade una pequeña imagen que consistirá en el icono del logro.',
	'kpax_ach:addform_maxlevel_title' => 'Puntuación máxima',
	'kpax_ach:addform_maxlevel_description' => 'Valor numérico de puntuación a la que puede llegar este logro. Si el logro no es un logro por puntuación, dejar este campo con el valor de 0.',
	'kpax_ach:addform_addreq_title' => 'Añadir requerimientos',
	'kpax_ach:addform_addreq_description' => 'Para añadir un nuevo requerimiento para aconseguir este logro haz clic en el link que hay más abajo. Podrás añadir tantos requerimientos como creas necesario. El usuario no podrá conseguir este logro hasta que no haya conseguido antes todos los requerimientos.',
	'kpax_ach:addform_addreq_addreq' => 'Nuevo requerimiento',
	'kpax_ach:addform_addreq_addlink' => '(+) Añadir requerimiento',
	'kpax_ach:addform_create' => 'Crear Logro',
	'kpax_ach:addform_reqdelete' => 'Eliminar',
	'kpax_ach:addform_success' => 'Logro añadido correctamente:',
	'kpax_ach:addform_ach_submit_error' => 'Error desconocido añadiendo un logro:',
	'kpax_ach:addform_ach_submit_invalid_session' => '¡Error! No puedes añadir este logro porque tu sesión no fue validada correctamente.',
	'kpax_ach:addform_ach_invalid_user' => 'No puedes añadir logros a este juego porque no te pertenece.',
	'kpax_ach:addform_req_error_null_achievements' => '¡Error! Uno de los requerimientos que querías añadir a este logro no existe.',
	'kpax_ach:addform_req_error_different_games' => '¡Error! Uno de los requerimientos asociados no pertenece a este juego.',
	'kpax_ach:addform_req_error_recursive_requirement' => '¡Error! Un logro no puede tenerse a él mismo como requerimiento.',
	'kpax_ach:addform_req_error_invalid_session' => '¡Error! No puedes añadir requerimientos a este logro porque tu sesión no fue validada correctamente.',
	'kpax_ach:addform_req_error_unknown' => 'Error desconocido al intentar asociar un requerimiento:',
	'kpax_ach:addform_icon_failed' => 'Hemos detectado que se ha producido un error al cargar la imagen de tu logro. Este logro se ha creado, pero se mostrará un icono por defecto hasta que esta sea cambiada.',
	'kpax_ach:addform_name_failed' => 'No has escrito ningún nombre para este logro.',
	'kpax_ach:addform_desc_failed' => 'No has escrito ninguna descripción para este logro.',
	'kpax_ach:addform_crop_success' => 'Icono del logro recortado correctamente.',
	'kpax_ach:addform_crop_areaerror' => '¡Error! El area del icono tiene que ser cuadrada.',
	'kpax_ach:addform_icon_error_invalid' => 'La imagen que has enviado no era válida.',
	'kpax_ach:addform_icon_error_not_converted' => 'La imagen que has enviado no se ha podido convertir.',

	/**
	 * Crop Image Form
	 */
	'kpax_ach:cropform_title' => 'Recortar el icono de este logro',
	'kpax_ach:cropform_explain' => 'Este es el icono para el logro que nos has enviado, pero ahora es necesario recortar la zona de la imagen que consistirá en el icono final del logro. Por favor, recorta una area cuadrada sobre la imagen que hay debajo.',
	'kpax_ach:cropform_ach_to_crop' => 'Imagen del logro a recortar',
	'kpax_ach:cropform_preview' => 'Vista Previa',
	'kpax_ach:cropform_crop' => 'Recortar',

	/**
	 * Achievement List
	 */
	'kpax_ach:list_edit_button' => 'Editar Logro',
	'kpax_ach:list_delete_button' => 'Eliminar Logro',
	'kpax_ach:list_progress' => 'Progreso',
	'kpax_ach:list_having' => 'En propiedad:',
	'kpax_ach:list_requirements' => 'Para conseguirlo, primero necesitas...',

	/**
	 * Achievement Delete Form
	 */
	'kpax_ach:delform_ach_invalid_user' => 'Lo sentimos, no tienes permiso para eliminar este logro.',
	'kpax_ach:delform_ach_invalid_ach' => 'El logro seleccionado no se ha encontrado y por lo tanto no se ha podido eliminar.',
	'kpax_ach:delform_ach_sure' => '¿Estás seguro de querer eliminar este logro?',
	'kpax_ach:delform_ach_success' => 'El logro se ha borrado correctamente.',
	'kpax_ach:delform_ach_unknown_error' => 'Error desconocido mientras se estaba eliminando un logro: ',

	/**
	 * Achievement Edit Form
	 */
	'kpax_ach:editform_ach_not_exists' => 'El logro seleccionado no existe.',
	'kpax_ach:editform_invalid_user' => 'Lo sentimos, no tienes permiso para editar este logro.',
	'kpax_ach:achievements_edit_title' => 'Editar un logro',
	'kpax_ach:editform_image_info' => 'Añade aquí un archivo solamente si quieres modificar el icono del logro. De lo contrario deja este campo en blanco.',
	'kpax_ach:editform_edit' => 'Editar Logro',
	'kpax_ach:editform_success' => 'Logro editador correctamente:'
	);

add_translation('es', $spanish);
?>