<?php
// get the form input
$title = get_input('ach_name');
$description = get_input('ach_desc');
$image = $_FILES['ach_icon'];
$action = get_input('form_action');
$maxLevel = (int)get_input('ach_maxlevel');

$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

if($action == 'edit')
{
	$achId = get_input('achid');
	$objAch = $objKpax->getAchievement($achId);

	if($objAch != null)
	{
		$guid = $objAch->idGame;
	}
	else
	{
		register_error(elgg_echo('kpax_ach:editform_ach_not_exists'));
		forward(REFERRER);
		die();
	}
}
else
{
	$guid = (int)get_input('guid');
}

gatekeeper();

$kpax = get_entity($guid);

$formok = true;

if($title == ''){
	register_error(elgg_echo('kpax_ach:addform_name_failed'));
	$formok = false;
}

if($description == ''){
	register_error(elgg_echo('kpax_ach:addform_desc_failed'));
	$formok = false;
}

if(!$formok){
	forward(REFERRER);
}

if($kpax->canEdit() && $formok){
	$userguid = elgg_get_logged_in_user_guid();

	if($action == 'edit')
	{
		$response = $objKpax->editAchievement($_SESSION["campusSession"], $achId, $title, $description, $maxLevel);
	}
	else
	{
		$response = $objKpax->addAchievementToGame($_SESSION["campusSession"], $guid, $title, $description, $maxLevel);
	}


	if($response == 'OK'){
		//Creació del directori de cache d'imatges si aquest no es troba creat
		$targetdir = kpaxGetDataRoot().'cache/';

		if(!is_dir($targetdir)){
			mkdir($targetdir);
		}

		//Obtenció de l'últim assoliment per a llegir la seva ID (o l'identificador, si s'estava editant)
		if($action == 'edit')
		{
			$achLast = $objAch;
		}
		else
		{
			$achLast = kpaxGetLastAchievement($guid);
		}

		//Comprovar que l'arxiu enviat sigui una imatge
		$imgok = false;
		if(kpaxIsImage($image)){
			$target = $targetdir.'achievement_'.$achLast->idAchievement.'.png';
			if(kpaxConvertImageToPng($image, $target)){
				$imgok = true;
			}
			else{
				register_error(elgg_echo('kpax_ach:addform_icon_error_not_converted'));
			}
		}
		else{
			if($action != 'edit')
			{
				register_error(elgg_echo('kpax_ach:addform_icon_error_invalid'));
			}
		}

		//Eliminar tots els requeriments anteriors
		if($action == 'edit')
		{
			$objKpax->deleteAllAchievementRequirements($_SESSION["campusSession"], $achId);
		}
		
		//Afegir requeriments
		$added_reqs = array();
		foreach($_POST as $key => $value){
			if(stristr($key, 'ach_addreq_')){
				if($key != 'ach_addreq_%REQNUM%'){
					if(!in_array($value, $added_reqs)){
						array_push($added_reqs, $value);
						$response = $objKpax->addRequirementToAchievement($_SESSION["campusSession"], $achLast->idAchievement, (int)$value);
						if($response != 'OK')
						{
							switch($response)
							{
								case "NULL_ACHIEVEMENTS":
									register_error(elgg_echo('kpax_ach:addform_req_error_null_achievements'));
								break;
								case "DIFFERENT_GAMES":
									register_error(elgg_echo('kpax_ach:addform_req_error_different_games'));
								break;
								case "RECURSIVE_REQUIREMENT":
									register_error(elgg_echo('kpax_ach:addform_req_error_recursive_requirement'));
								break;
								case "INVALID_SESSION":
									register_error(elgg_echo('kpax_ach:addform_req_error_invalid_session'));
								break;
								default:
									register_error(elgg_echo('kpax_ach:addform_req_error_unknown').' '.$response);
								break;
							}
						}
					}
				}
			}
		}

		
		if($imgok){
			system_message(elgg_echo('kpax_ach:addform_success').' '.$title);
			forward('kpax_ach/crop_icon/'.$achLast->idAchievement.'');
		}
		elseif($action != 'edit')
		{
			register_error(elgg_echo('kpax_ach:addform_icon_failed'));
			forward('kpax/view/'.$guid.'/achievements');
		}
		else
		{
			system_message(elgg_echo('kpax_ach:editform_success').' '.$title);
			forward('kpax/view/'.$guid.'/achievements');
		}
	}
	else{
		if($response == 'INVALID_SESSION'){
			register_error(elgg_echo('kpax_ach:addform_ach_submit_invalid_session'));
		}
		else
		{
			register_error(elgg_echo('kpax_ach:addform_ach_submit_error').' '.$response);
		}
		forward(REFERRER);
	}
}
else
{
	register_error(elgg_echo('kpax_ach:addform_ach_invalid_user'));
	forward(REFERRER);
}
?>