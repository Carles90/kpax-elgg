<?php
$achid = (int)get_input('achid');

$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$ach = $objKpax->getAchievement($achid);

//Si l'assoliment no existeix, ignorar la acció
if($ach != null){
	$kpax = get_entity($ach->idGame);

	//Comprovar que l'usuari tingui permís per a editar aquest assoliment
	if($kpax->canEdit()){
		$x = (int)get_input('crop_x');
		$y = (int)get_input('crop_y');
		$w = (int)get_input('crop_w');
		$h = (int)get_input('crop_h');

		//Comprovar que l'àrea seleccionada sigui quadrada
		if($w != $h){
			register_error(elgg_echo('kpax_ach:addform_crop_areaerror'));
			forward(REFERRER);
		}
		else{
			//Obtenir la imatge original ({CACHE_ELGG}/kpax/cache)
			$data_root = kpaxGetDataRoot();

			if(!is_dir($data_root.'achievements/'))
			{
				mkdir($data_root.'achievements/');
			}

			$origindir = $data_root.'cache/achievement_'.$ach->idAchievement.'.png';
			$origin = imagecreatefrompng($origindir);

			//Retallar imatge gran
			$targ_w = 200;
			$targ_h = 200;

			$dest = imagecreatetruecolor($targ_w, $targ_h);
			imagecopyresampled($dest, $origin, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
			imagepng($dest, $data_root.'achievements/'.$ach->idAchievement.'large.png');

			//Retallar imatge mitjana
			$targ_w = 100;
			$targ_h = 100;

			$dest = imagecreatetruecolor($targ_w, $targ_h);
			imagecopyresampled($dest, $origin, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
			imagepng($dest, $data_root.'achievements/'.$ach->idAchievement.'medium.png');

			//Retallar imatge petita
			$targ_w = 40;
			$targ_h = 40;

			$dest = imagecreatetruecolor($targ_w, $targ_h);
			imagecopyresampled($dest, $origin, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
			imagepng($dest, $data_root.'achievements/'.$ach->idAchievement.'small.png');

			//Retallar imatge minúscula
			$targ_w = 25;
			$targ_h = 25;

			$dest = imagecreatetruecolor($targ_w, $targ_h);
			imagecopyresampled($dest, $origin, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
			imagepng($dest, $data_root.'achievements/'.$ach->idAchievement.'tiny.png');

			//Eliminar icona de la cache
			unlink($origindir);

			//Acabar
			system_message(elgg_echo('kpax_ach:addform_crop_success'));
			forward('kpax/view/'.$ach->idGame.'/achievements');
		}
	}
	else{
		register_error(elgg_echo('kpax_ach:addform_ach_invalid_user'));
		forward(REFERRER);
	}
}
else{
	register_error(elgg_echo('kpax_ach:addform_ach_invalid_user'));
	forward(REFERRER);
}
?>