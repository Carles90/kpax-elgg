<?php
$kpax = get_entity(get_input('guid'));
$guid = (int)get_input('guid');

$title = $kpax->title;

if($title == ''){
	$content = elgg_echo('kpax:game_not_exists');
}
else{
	elgg_push_breadcrumb($title);
	elgg_push_breadcrumb(elgg_echo('kpax_ach:achievements_add_button'));

	$content = elgg_view_title(elgg_echo('kpax_ach:achievements_add_title').': '.$title);
	$content .= elgg_view('kpax_ach/game/ach_create');
}

$body = elgg_view_layout('content', array(
    'content' => $content,
    'title' => $title,
    'filter' => '',
    'header' => '',
        ));

echo elgg_view_page($title, $body);

?>