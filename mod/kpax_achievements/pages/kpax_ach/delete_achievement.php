<?php
$achId = get_input('achid');

$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$ach = $objKpax->getAchievement($achId);

if($ach != null){
	$kpax = get_entity($ach->idGame);

	if($kpax->canEdit()){
		$status = $objKpax->deleteAchievementFromGame($_SESSION["campusSession"], $ach->idAchievement);

		switch($status)
		{
			case "OK":
				system_message(elgg_echo('kpax_ach:delform_ach_success'));
			break;
			case "INVALID_SESSION":
				register_error(elgg_echo('kpax_ach:delform_ach_invalid_user'));
			break;
			case "NULL_ACHIEVEMENT":
				register_error(elgg_echo('kpax_ach:delform_ach_invalid_ach'));
			break;
			default:
				register_error(elgg_echo('kpax_ach:delform_ach_unknown_error').$status);
			break;
		}
	}
	else{
		register_error(elgg_echo('kpax_ach:delform_ach_invalid_user'));
	}
}
else{
	register_error(elgg_echo('kpax_ach:delform_ach_invalid_ach'));
}

forward(REFERRER);
?>