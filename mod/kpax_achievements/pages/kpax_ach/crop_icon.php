<?php
elgg_load_js('kpax_jcrop');

$content = elgg_view_title(elgg_echo('kpax_ach:cropform_title'));
$content .= elgg_view('kpax_ach/game/crop_icon', array());

$body = elgg_view_layout('content', array(
    'content' => $content,
    'title' => $title,
    'filter' => '',
    'header' => '',
        ));

echo elgg_view_page($title, $body);
?>