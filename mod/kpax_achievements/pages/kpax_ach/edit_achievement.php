<?php
elgg_push_breadcrumb(elgg_echo('kpax_ach:achievements_edit_title'));

$content = elgg_view_title(elgg_echo('kpax_ach:achievements_edit_title'));
$content .= elgg_view('kpax_ach/game/ach_edit');


$body = elgg_view_layout('content', array(
    'content' => $content,
    'title' => $title,
    'filter' => '',
    'header' => '',
        ));

echo elgg_view_page($title, $body);

?>