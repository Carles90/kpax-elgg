<?php
function kpaxGetLastAchievement($idGame){
	$username = elgg_get_logged_in_user_entity()->username;

	$objKpax = new kpaxSrv($username);

	$achList = $objKpax->getGameAchievements($idGame);

	$achRet = null;

	foreach($achList as $ach){
		if($ach->idAchievement > $achRet->idAchievement){
			$achRet = $ach;
		}
	}

	return $achRet;
}
?>