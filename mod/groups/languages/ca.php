<?php

// Generat per la traducció del navegador 

$catalan = array( 
	 'groups'  =>  "Grups" , 
	 'groups:owned'  =>  "Grups que has creat" , 
	 'groups:yours'  =>  "Grups dels que formes part" , 
	 'groups:user'  =>  "grups de %s" , 
	 'groups:all'  =>  "Tots els grups" , 
	 'groups:new'  =>  "Crear un nou grup" , 
	 'groups:edit'  =>  "Editar grup" , 
	 'groups:delete'  =>  "Eliminar grup" , 
	 'groups:membershiprequests'  =>  "Gestionar peticions de membres" , 
	 'groups:icon'  =>  "Icona del grup" , 
	 'groups:name'  =>  "Nom del grup" , 
	 'groups:username'  =>  "Nom curt del grup (veure a les URLs)" , 
	 'groups:description'  =>  "Descripció" , 
	 'groups:briefdescription'  =>  "Descripció breu" , 
	 'groups:interests'  =>  "Etiquetes" , 
	 'groups:website'  =>  "Pàgina web" , 
	 'groups:members'  =>  "Membres del grup" , 
	 'groups:membership'  =>  "Permisos de les membres del grup" , 
	 'groups:access'  =>  "Permisos d'accés" , 
	 'groups:owner'  =>  "Creat per" , 
	 'groups:widget:num_display'  =>  "Nombre de grups a veure" , 
	 'groups:widget:membership'  =>  "Grups" , 
	 'groups:widgets:description'  =>  "Veure al teu perfil, els grups dels que formes part" , 
	 'groups:noaccess'  =>  "Accés no permès al grup" , 
	 'groups:cantedit'  =>  "No pots editar aquest grup" , 
	 'groups:saved'  =>  "Grup desat" , 
	 'groups:featured'  =>  "Característiques dels grups" , 
	 'groups:makeunfeatured'  =>  "Sense destacar" , 
	 'groups:makefeatured'  =>  "Destacar grup" , 
	 'groups:featuredon'  =>  "Has destacat el grup i les seves característiques" , 
	 'groups:unfeature'  =>  "El grup ja no està destacat" , 
	 'groups:joinrequest'  =>  "Sol·licitud per formar part del grup" , 
	 'groups:join'  =>  "Formar part del grup" , 
	 'groups:leave'  =>  "Deixar el grup" , 
	 'groups:invite'  =>  "Convidar a amistats" , 
	 'groups:inviteto'  =>  "Convidar a amistats des de '%s'" , 
	 'groups:nofriends'  =>  "No tens amistats que no estiguin convidades a aquest grup" , 
	 'groups:viagroups'  =>  "via grups" , 
	 'groups:group'  =>  "Grup" , 
	 'groups:notfound'  =>  "Grup no trobat" , 
	 'groups:notfound:details'  =>  "La petició d'accés al grup no existeix o no tens permisos per accedir-hi" , 
	 'groups:requests:none'  =>  "No existeixen peticions pendents en aquests moments." , 
	 'item:object:groupforumtopic'  =>  "Temes de debat" , 
	 'groupforumtopic:new'  =>  "Publicar nou debat" , 
	 'groups:count'  =>  "grups creats" , 
	 'groups:open'  =>  "grup obert" , 
	 'groups:closed'  =>  "grup tancat" , 
	 'groups:member'  =>  "membres" , 
	 'groups:searchtag'  =>  "Cercar grups per etiquetes" , 
	 'groups:access:private'  =>  "Tancat - Les usuàries han de ser convidades" , 
	 'groups:access:public'  =>  "Obert - Qualsevol usuària pot formar part" , 
	 'groups:closedgroup'  =>  "Aquest grup és privat. Si vols participar envia un correu electrònic a xe@elbaixllobregat.cat" , 
	 'groups:enablepages'  =>  "Activar pàgines del grup" , 
	 'groups:enableforum'  =>  "Activar debats de grup" , 
	 'groups:enablefiles'  =>  "Activar fitxers del grup" , 
	 'groups:yes'  =>  "si" , 
	 'groups:no'  =>  "no" , 
	 'group:created'  =>  "Creat %s amb %d entrades" , 
	 'groups:lastupdated'  =>  "Darrera actualització %s per %s" , 
	 'groups:pages'  =>  "Pàgines del grup" , 
	 'groups:files'  =>  "Fitxers del grup" , 
	 'group:replies'  =>  "Respostes" , 
	 'groups:forum'  =>  "Debats del grup" , 
	 'groups:addtopic'  =>  "Afegir un títol" , 
	 'groups:forumlatest'  =>  "Darrer debat" , 
	 'groups:latestdiscussion'  =>  "Darrers comentaris" , 
	 'groups:newest'  =>  "Darrers" , 
	 'groups:popular'  =>  "Populars" , 
	 'groupspost:success'  =>  "El teu comentari ha estat publicat correctament" , 
	 'groups:alldiscussion'  =>  "Totes les entrades" , 
	 'groups:edittopic'  =>  "Editar títol" , 
	 'groups:topicmessage'  =>  "Missatge sobre tema" , 
	 'groups:topicstatus'  =>  "Estat del tema" , 
	 'groups:reply'  =>  "Escriure un comentari" , 
	 'groups:topic'  =>  "Tema" , 
	 'groups:posts'  =>  "Entrades" , 
	 'groups:lastperson'  =>  "Darrera persona" , 
	 'groups:when'  =>  "Quan" , 
	 'grouptopic:notcreated'  =>  "No hi han temes creats" , 
	 'groups:topicopen'  =>  "Obert" , 
	 'groups:topicclosed'  =>  "Tancat" , 
	 'groups:topicresolved'  =>  "Resolt" , 
	 'grouptopic:created'  =>  "El tema ha esta creat" , 
	 'groupstopic:deleted'  =>  "Tema esborrat" , 
	 'groups:topicsticky'  =>  "Permanent" , 
	 'groups:topicisclosed'  =>  "Aquest tema ha estat tancat" , 
	 'groups:topiccloseddesc'  =>  "El tema ha estat tancat i no accepta nous comentaris" , 
	 'grouptopic:error'  =>  "El tema del teu grup no es pot creat. Posa't en contacte amb l'equip administrador de la xarxa si veus que no pots" , 
	 'groups:forumpost:edited'  =>  "Has editat correctament l'entrada al foro." , 
	 'groups:forumpost:error'  =>  "Existeix algun problema en l'edició de l'entrada." , 
	 'groups:privategroup'  =>  "Aquest grup és privat, demana l'admissió." , 
	 'groups:notitle'  =>  "Els grups han de tenir un títol" , 
	 'groups:cantjoin'  =>  "No pots unir-te al grup" , 
	 'groups:cantleave'  =>  "No pots deixar el grup" , 
	 'groups:river:create'  =>  "%s ha creat el grup" , 
	 'groups:invitekilled'  =>  "La invitada ha estat eliminada" , 
	 'groups:addedtogroup'  =>  "Afegit de forma correcta la usuària al grup" , 
	 'groups:joinrequestnotmade'  =>  "La petició d'entrada al grup no pot ser enviada" , 
	 'groups:joinrequestmade'  =>  "Petició d'entrada al grup realitzada correctament" , 
	 'groups:joined'  =>  "Ja formes part del grup!" , 
	 'groups:left'  =>  "Ja has deixat el grup" , 
	 'groups:notowner'  =>  "Perdona!, no ets la propietària d'aquest grup." , 
	 'groups:alreadymember'  =>  "Ja formes part d'aquest grup!" , 
	 'groups:userinvited'  =>  "Persona convidada" , 
	 'groups:usernotinvited'  =>  "La usuària no pot ser convidada" , 
	 'groups:useralreadyinvited'  =>  "La usuària ja ha estat convidada" , 
	 'groups:updated'  =>  "Darrer comentari" , 
	 'groups:invite:subject'  =>  "%s ha estat convidada a formar part a %s!" , 
	 'groups:started'  =>  "Iniciat per" , 
	 'groups:joinrequest:remove:check'  =>  "Estàs segura que vols esborrar aquesta petició d'entrada?" , 
	 'groups:invite:body'  =>  "Hola %s,

%s t'ha convidat a formar part al grup '%s', fes clic a sota per a confirmar-ho:

%s" , 
	 'groups:welcome:subject'  =>  "Benvinguda i/o benvingut al grup %s!" , 
	 'groups:welcome:body'  =>  "Hola %s!

En aquest moment ets membre del grup '%s', fes clic a sota per començar a fer comentaris:

%s" , 
	 'groups:request:subject'  =>  "%s has demanat formar part de %s" , 
	 'groups:request:body'  =>  "Hola %s,

%s ha sol·licitat participar al grup '%s', fes clic a sota per veure el perfil:

%s

o fes clic aquí sota per confirmar la teva petició directament:

%s" , 
	 'groups:river:member'  =>  "%s ara ets membre de " , 
	 'groupforum:river:updated'  =>  "%s ha estat actualitzat" , 
	 'groupforum:river:update'  =>  "aquest tema de discussió" , 
	 'groupforum:river:created'  =>  "%s ha creat" , 
	 'groupforum:river:create'  =>  "un nou tema de discussió titulat" , 
	 'groupforum:river:posted'  =>  "%s ha escrit un comentari nou" , 
	 'groupforum:river:annotate:create'  =>  "en aquest tema de discussió" , 
	 'groupforum:river:postedtopic'  =>  "%s ha començat un nou fil amb el títol " , 
	 'groups:nowidgets'  =>  "No hi ha widgets definits per a aquest grup" , 
	 'groups:widgets:members:title'  =>  "Membres del grup" , 
	 'groups:widgets:members:description'  =>  "Llistat de membres del grup" , 
	 'groups:widgets:members:label:displaynum'  =>  "Llista de membres del grup" , 
	 'groups:widgets:members:label:pleaseedit'  =>  "Si  us plau, configura el widget," , 
	 'groups:widgets:entities:title'  =>  "Objectes en el grup" , 
	 'groups:widgets:entities:description'  =>  "Llistat d'objectes desats en aquest grup" , 
	 'groups:widgets:entities:label:displaynum'  =>  "Llistat d'objectes del grup" , 
	 'groups:widgets:entities:label:pleaseedit'  =>  "Si us plau, configura el widget" , 
	 'groups:forumtopic:edited'  =>  "Títol del foro editat correctament" , 
	 'group:deleted'  =>  "El grup i els seus continguts han estat eliminats" , 
	 'group:notdeleted'  =>  "El grup no pot liminar-se" , 
	 'grouppost:deleted'  =>  "Entrades del grup eliminades correctament" , 
	 'grouppost:notdeleted'  =>  "Les entrades del grup no poden ser eliminades" , 
	 'groupstopic:notdeleted'  =>  "Tema no eliminat" , 
	 'grouptopic:blank'  =>  "Sense temes" , 
	 'groups:deletewarning'  =>  "Estàs segur/a que vols eliminar el grup? Aquesta acció no es pot desfer!" , 
	 'groups:joinrequestkilled'  =>  "La petició d'admissió ha estat esborrada" , 
	 'groups:widget:normal'  =>  "Llistat normal" , 
	 'groups:widget:small'  =>  "Llistat petit" , 
	 'groups:visibility'  =>  "Qui pot veure aquest grup?" , 
	 'groups:allowhiddengroups'  =>  "Vols permetre grups privats (invisibles)?" , 
	 'groups:invitations'  =>  "Invitacions a grups" , 
	 'groups:search:tags'  =>  "etiqueta" , 
	 'groups:invitations:none'  =>  "No hi ha invitacions pendents en aquest moment." , 
	 'groups:notmember'  =>  "Ho sento, no ets membre d'aquest grup." , 
	 'groups:river:togroup'  =>  "al grup" , 
	 'grouptopic:notfound'  =>  "No es pot trobar el tema" , 
	 'grouppost:nopost'  =>  "Publicació buida" , 
	 'groups:memberlist'  =>  "Membres del grup" , 
	 'groups:membersof'  =>  "Membres de %s" , 
	 'groups:members:more'  =>  "Veure més membres" , 
	 'groups:closedgroup:request'  =>  "Per a sol·licitar ser admesa, fes clic en \"Sol·licitar admissió\" a l'enllaç del menú" , 
	 'groups:lastcomment'  =>  "Darrer comentari %s per %s" , 
	 'groups:invite:remove:check'  =>  "Estàs segura que vols eliminar aquesta invitada?"
); 

add_translation('ca', $catalan); 

?>