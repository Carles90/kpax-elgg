<?php
$idLeague = get_input("idLeague");
$leagueEntity = get_entity($idLeague);
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

if($leagueEntity->title != '')
{
	if($leagueEntity->canEdit())
	{
		$objLeague = new League($idLeague);
		$response = $objLeague->removeLeague($objKpax);
		
		switch($response)
		{
			case "OK":
				if($leagueEntity->delete())
				{
					system_message(elgg_echo('kpax_leagues:deleteform_success'));
					forward('kpax_leagues/overview/');
				}
				else
				{
					register_error(elgg_echo('kpax_leagues:deleteform_error_elgg_entity'));
					forward(REFERRER);
				}
			break;
			case "VALIDATION_ERROR":
				register_error(elgg_echo('kpax_leagues:deleteform_error_not_permission'));
				forward(REFERRER);
			break;
			case "DELETING_ERROR":
				register_error(elgg_echo('kpax_leagues:deleteform_error_deleting'));
				forward(REFERRER);
			break;
			case "INVALID_LEAGUE":
				register_error(elgg_echo('kpax_leagues:deleteform_error_not_exists'));
				forward(REFERRER);
			break;
			default:
				register_error(elgg_echo('kpax_leagues:deleteform_error_unknown').': '.$response);
				forward(REFERRER);
			break;
		}
	}
	else
	{
		register_error(elgg_echo('kpax_leagues:deleteform_error_not_permission'));
		forward(REFERRER);
	}
}
else
{
	register_error(elgg_echo('kpax_leagues:deleteform_error_not_exists'));
	forward(REFERRER);
}
?>