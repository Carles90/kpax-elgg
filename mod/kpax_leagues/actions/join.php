<?php
$idLeague = get_input("idLeague");
$leagueEntity = get_entity($idLeague);

if($leagueEntity->title != '')
{
	$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
	$objLeague = new League($idLeague);

	$response = $objLeague->joinLeague($objKpax);

	switch($response)
	{
		case "OK":
			system_message(elgg_echo('kpax_leagues:actions_join_success'));
			if($objLeague->getInfo($objKpax)->distribution == "teams")
			{
				//Si la competició funciona per equips, redirigir a l'usuari per a que esculli un equip
				forward('kpax_leagues/view/'.$idLeague.'/teams');
			}
			else
			{
				forward('kpax_leagues/view/'.$idLeague);
			}
		break;
		case "VALIDATION_ERROR":
			register_error(elgg_echo('kpax_leagues:actions_join_error_validation'));
			forward(REFERRER);
		break;
		case "INVALID_LEAGUE":
			register_error(elgg_echo('kpax_leagues:actions_join_error_invalid_league'));
			forward(REFERRER);
		break;
		case "LEAGUE_IS_FULL":
			register_error(elgg_echo('kpax_leagues:actions_join_error_league_is_full'));
			forward(REFERRER);
		break;
		case "ALREADY_A_MEMBER":
			register_error(elgg_echo('kpax_leagues:actions_join_error_already_a_member'));
			forward(REFERRER);
		break;
		case "SUBSCRIPTION_CLOSED":
			register_error(elgg_echo('kpax_leagues:actions_join_error_subscription_closed'));
			forward(REFERRER);
		break;
		case "SAVING_ERROR":
			register_error(elgg_echo('kpax_leagues:actions_join_error_saving_error'));
			forward(REFERRER);
		break;
		default:
			register_error(elgg_echo('kpax_leagues:actions_join_error_unknown').': '.$response);
			forward(REFERRER);
		break;
	}
}
else
{
	register_error(elgg_echo('kpax_leagues:leagueview_notfound_desc'));
	forward(REFERRER);
}
?>