<?php
$idTeam = get_input("idTeam");
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$objLeague = new League($idLeague);
$password = md5(get_input("password"));

$response = $objLeague->joinTeam($objKpax, $idTeam, $password);

switch($response)
{
	case "OK":
		system_message(elgg_echo('kpax_leagues:leagueview_teamjoin_success'));
	break;
	case "INVALID_TEAM":
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_invalid_team'));
	break;
	case "INVALID_LEAGUE":
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_invalid_league'));
	break;
	case "LEAGUE_CLOSED":
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_closed'));
	break;
	case "INVALID_PASSWORD":
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_invalid_password'));
	break;
	case "ALREADY_ON_A_TEAM":
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_already_on_a_team'));
	break;
	case "NOT_A_MEMBER":
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_not_a_member'));
	break;
	case "TEAM_IS_FULL":
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_team_is_full'));
	break;
	case "VALIDATION_ERROR":
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_validation_error'));
	break;
	case "SAVING_ERROR":
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_saving_error'));
	break;
	default:
		register_error(elgg_echo('kpax_leagues:leagueview_teamjoin_error_unknown_error').': '.$response);
	break;
}

forward(REFERRER);
?>