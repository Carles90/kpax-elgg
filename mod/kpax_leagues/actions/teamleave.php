<?php
$idTeam = get_input("idTeam");
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$objLeague = new League($idLeague);

$response = $objLeague->leaveTeam($objKpax, $idTeam);

switch($response)
{
	case "OK":
		system_message(elgg_echo('kpax_leagues:leagueview_teamleave_success'));
	break;
	case "INVALID_TEAM":
		register_error(elgg_echo('kpax_leagues:leagueview_teamleave_error_invalid_team'));
	break;
	case "INVALID_LEAGUE":
		register_error(elgg_echo('kpax_leagues:leagueview_teamleave_error_invalid_league'));
	break;
	case "LEAGUE_CLOSED":
		register_error(elgg_echo('kpax_leagues:leagueview_teamleave_error_closed'));
	break;
	case "NOT_IN_THIS_TEAM":
		register_error(elgg_echo('kpax_leagues:leagueview_teamleave_error_not_in_this_team'));
	break;
	case "NOT_A_MEMBER":
		register_error(elgg_echo('kpax_leagues:leagueview_teamleave_error_not_a_member'));
	break;
	case "VALIDATION_ERROR":
		register_error(elgg_echo('kpax_leagues:leagueview_teamleave_error_validation_error'));
	break;
	default:
		register_error(elgg_echo('kpax_leagues:leagueview_teamleave_error_unknown_error').': '.$response);
	break;
}

forward(REFERRER);
?>