<?php
$idTeam = get_input("idTeam");
$idUser = get_input("idUser");
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

$objTeam = $objKpax->getLeagueTeam($_SESSION['campusSession'], $idTeam);

if($objTeam != null)
{
	$objLeague = new League($objTeam->idLeague);
	$leagueEntity = get_entity($objTeam->idLeague);
	$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);

	if($kpaxUser == $idUser)
	{
		register_error(elgg_echo('kpax_leagues:leagueview_teamkick_error_kick_yourself'));
		forward(REFERRER);
	}

	if($leagueEntity->canEdit() || $objTeam->idOwner == $kpaxUser)
	{
		$response = $objLeague->kickUserFromTeam($objKpax, $idTeam, $idUser);

		switch($response)
		{
			case "OK":
				system_message(elgg_echo('kpax_leagues:leagueview_teamkick_success'));
			break;
			case "INVALID_TEAM":
				register_error(elgg_echo('kpax_leagues:leagueview_teamkick_error_invalid_team'));
			break;
			case "INVALID_LEAGUE":
				register_error(elgg_echo('kpax_leagues:leagueview_teamkick_error_invalid_league'));
			break;
			case "LEAGUE_CLOSED":
				register_error(elgg_echo('kpax_leagues:leagueview_teamkick_error_closed'));
			break;
			case "VALIDATION_ERROR":
				register_error(elgg_echo('kpax_leagues:leagueview_teamkick_error_validation_error'));
			break;
			case "NOT_IN_THIS_TEAM":
				register_error(elgg_echo('kpax_leagues:leagueview_teamkick_error_not_in_this_team'));
			break;
			default:
				register_error(elgg_echo('kpax_leagues:leagueview_teamkick_error_unknown_error').': '.$response);
			break;
		}
	}
	else
	{
		register_error(elgg_echo('kpax_leagues:leagueview_teamkick_error_validation_error'));
	}
}
else
{
	register_error(elgg_echo('kpax_leagues:leagueview_teamkick_error_invalid_team'));
}

forward(REFERRER);
?>