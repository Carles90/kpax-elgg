<?php
$idLeague = get_input("idLeague");
$leagueEntity = get_entity($idLeague);

if($leagueEntity->title != '')
{
	$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
	$objLeague = new League($idLeague);

	$response = $objLeague->leaveLeague($objKpax);

	switch($response)
	{
		case "OK":
			system_message(elgg_echo('kpax_leagues:actions_leave_success'));
			forward('kpax_leagues/view/'.$idLeague);
		break;
		case "VALIDATION_ERROR":
			register_error(elgg_echo('kpax_leagues:actions_leave_error_validation'));
			forward(REFERRER);
		break;
		case "INVALID_LEAGUE":
			register_error(elgg_echo('kpax_leagues:actions_leave_error_invalid_league'));
			forward(REFERRER);
		break;
		case "NOT_A_MEMBER":
			register_error(elgg_echo('kpax_leagues:actions_leave_error_not_a_member'));
			forward(REFERRER);
		break;
		case "DELETING_ERROR":
			register_error(elgg_echo('kpax_leagues:actions_leave_error_deleting_error'));
			forward(REFERRER);
		break;
		case "SUBSCRIPTION_CLOSED":
			register_error(elgg_echo('kpax_leagues:actions_leave_error_subscription_closed'));
			forward(REFERRER);
		break;
		default:
			register_error(elgg_echo('kpax_leagues:actions_leave_error_unknown').': '.$response);
			forward(REFERRER);
		break;
	}
}
else
{
	register_error(elgg_echo('kpax_leagues:leagueview_notfound_desc'));
	forward(REFERRER);
}
?>