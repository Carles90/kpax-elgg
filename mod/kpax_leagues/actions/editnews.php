<?php
$idLeague = (int)get_input("idLeague");
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$leagueEntity = get_entity($idLeague);
$leagueObj = new League($idLeague);
$leagueInfo = $leagueObj->getInfo($objKpax);

//Comprovar si la competició existeix i es té permís per a editar-la
if($leagueEntity->title == '')
{
	register_error(elgg_echo('kpax_leagues:editform_league_not_exists'));
	forward(REFERRER);
	die;
}

if(!$leagueEntity->canEdit())
{
	register_error(elgg_echo('kpax_leagues:editform_not_permission'));
	forward(REFERRER);
	die;
}

$news = utf8_encode(html_entity_decode(get_input('news')));

$response = $objKpax->editNews($_SESSION['campusSession'], $idLeague, $news);

switch($response)
{
	case "OK":
		system_message(elgg_echo('kpax_leagues:leagueview_news_success'));
		forward('kpax_leagues/view/'.$idLeague.'/news');
	break;
	case "VALIDATION_ERROR":
		register_error(elgg_echo('kpax_leagues:createform_error_validation'));
		forward(REFERRER);
	break;
	case "LEAGUE_NOT_EXISTS":
		register_error(elgg_echo('kpax_leagues:editform_league_not_exists'));
		forward(REFERRER);
	break;
	case "SAVING_ERROR":
		register_error(elgg_echo('kpax_leagues:createform_error_saving'));
		forward(REFERRER);
	break;
	default:
		register_error(elgg_echo('kpax_leagues:createform_error_unknown').': '.$response);
		forward(REFERRER);
	break;
}
?>