<?php
//Llegir inputs del formulari
$title = get_input('league_title');
$description = utf8_encode(html_entity_decode(get_input('league_description')));
$start_date = (int)get_input('league_start_date');
$start_time = (int)get_input('league_start_time');
$end_date = (int)get_input('league_end_date');
$end_time = (int)get_input('league_end_time');
$scoretype = get_input('league_scoretype');
$distribution = get_input('league_distribution');
$maxusers = (int)get_input('league_maxusers');
$allowteams = get_input('league_allowteams');
$maxgroups = (int)get_input('league_maxgroups');
$maxusergroup = (int)get_input('league_maxusergroup');
$categories = get_input('league_categories');

// Conversió de les dates a una unix timestamp.
// La data arriba en format unix timestamp i se li ha de sumar la quantitat de minuts, que és el nombre
// que arriba del input de la hora.
$start = $start_date + $start_time * 60;
$end = $end_date + $end_time * 60;
$now = time(); // Hora actual

//Obligar a que la informació del formulari es mantingui en cas de fallida
elgg_make_sticky_form('kpax_league');

//Crear objecte de kPAX
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

//Comprovar que el formulari s'hagi emplenat correctament
$formok = true;

//Comprovar que s'hagi escrit un títol
if($title == ''){
	register_error(elgg_echo('kpax_leagues:createform_name_failed'));
	$formok = false;
}

//Comprovar que s'hagi escrit una descripció
if($description == ''){
	register_error(elgg_echo('kpax_leagues:createform_desc_failed'));
	$formok = false;
}

//Comprovar que la competició no comenci en un moment anterior a l'actual
if($start < $now)
{
	register_error(elgg_echo('kpax_leagues:createform_start_failed'));
	$formok = false;
}

//Comprovar que la competició no acabi abans que comenci
if($end < $start)
{
	register_error(elgg_echo('kpax_leagues:createform_end_failed'));
	$formok = false;
}

//Comprovar que el tipus de puntuació sigui correcte
if($scoretype != 'scoretable' && $scoretype != 'tree' && $scoretype != 'knockout')
{
	register_error(elgg_echo('kpax_leagues:createform_scoretype_failed'));
	$formok = false;
}

//Comprovar que la distribució d'usuaris sigui la correcta
if($distribution != 'single' && $distribution != 'teams')
{
	register_error(elgg_echo('kpax_leagues:createform_distribution_failed'));
	$formok = false;
}

//Si s'ha triat un nombre màxim d'usuaris negatiu, establir a 0
if($maxusers < 0)
{
	$maxusers = 0;
}

//Si s'ha triat un nombre màxim d'equips negatiu o la competició no funciona per equips, establir a 0
if($maxgroups < 0 || $distribution == 'single')
{
	$maxgroups = 0;
}

//Si s'ha triat un nombre màxim d'usuaris per equip o la competició no funciona per equips, establir a 0
if($maxusergroup < 0 || $distribution == 'single')
{
	$maxusergroup = 0;
}

//Si s'ha enviat un valor erroni per gestionar els equips, deixar-ho a 0
if($allowteams != 0 && $allowteams != 1)
{
	$allowteams = 0;
}

//Si alguna cosa ha fallat al formulari, acabar aquí i donar l'error
if(!$formok){
	forward(REFERRER);
	die;
}

//Crear un objecte d'Elgg
$objElgg = new ElggObject;
$objElgg->subtype = "kpax_league";
$objElgg->container_guid = (int)get_input('container_guid', $_SESSION['user']->getGUID());
$objElgg->title = $title;
$objElgg->owner_guid = elgg_get_logged_in_user_guid();
$objElgg->access_id = ACCESS_LOGGED_IN;
if(!$objElgg->save())
{
	register_error(elgg_echo('kpax_leagues:createform_error_elgg'));
	forward(REFERRER);
	die;
}

//Cridar al servei per a crear una competició
$response = $objKpax->addLeague($_SESSION['campusSession'], $objElgg->getGUID(), $title, $description, $start, $end, $scoretype, $distribution, $maxusers, $maxgroups, $maxusergroup, $allowteams);

switch($response)
{
	case "OK":
		//Afegir categories a la competició
		$caterror = false;
		foreach($categories as $cat)
		{
			$response = $objKpax->addCategoryToLeague($_SESSION['campusSession'], $objElgg->getGUID(), $cat);
			if($response != 'OK')
			{
				$caterror = true;
			}
		}

		if($caterror)
		{
			register_error(elgg_echo('kpax_leagues:createform_categories_failed'));
		}

		//Afegir jocs a la playlist
		$gameorder = 0;
		$gameerror = false;
		foreach($_POST as $key => $value)
		{
			if(stristr($key, 'league_addgame_'))
			{
				if($key != 'league_addgame_%GAMENUM%')
				{
					$response = $objKpax->addGameToPlaylist($_SESSION['campusSession'], $objElgg->getGUID(), (int)$value, $gameorder);
					if($response != 'OK')
					{
						$gameerror = true;
					}
					else
					{
						$gameorder++;
					}
				}
			}
		}

		if($gameerror)
		{
			register_error(elgg_echo('kpax_leagues:createform_playlist_failed'));
		}

		elgg_clear_sticky_form('kpax_league');
		system_message(elgg_echo('kpax_leagues:createform_sucess'));
		forward('kpax_leagues/view/'.$objElgg->getGUID());
	break;
	case "VALIDATION_ERROR":
		register_error(elgg_echo('kpax_leagues:createform_error_validation'));
		forward(REFERRER);
	break;
	case "SAVING_ERROR":
		register_error(elgg_echo('kpax_leagues:createform_error_saving'));
		forward(REFERRER);
	break;
	default:
		register_error(elgg_echo('kpax_leagues:createform_error_unknown').': '.$response);
		forward(REFERRER);
	break;
}
?>