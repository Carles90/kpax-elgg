<?php
$idLeague = get_input("idLeague");
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$leagueEntity = get_entity($idLeague);
$leagueObj = new League($idLeague);
$leagueInfo = $leagueObj->getInfo($objKpax);

//Comprovar si la competició existeix i es té permís per a editar-la
if($leagueEntity->title == '')
{
	register_error(elgg_echo('kpax_leagues:editform_league_not_exists'));
	forward(REFERRER);
	die;
}

if(!$leagueEntity->canEdit())
{
	register_error(elgg_echo('kpax_leagues:editform_not_permission'));
	forward(REFERRER);
	die;
}

//Llegir inputs del formulari
$title = get_input('league_title');
$description = utf8_encode(html_entity_decode(get_input('league_description')));
$start_date = (int)get_input('league_start_date');
$start_time = (int)get_input('league_start_time');
$end_date = (int)get_input('league_end_date');
$end_time = (int)get_input('league_end_time');
$scoretype = get_input('league_scoretype');
$categories = get_input('league_categories');

// Conversió de les dates a una unix timestamp.
// La data arriba en format unix timestamp i se li ha de sumar la quantitat de minuts, que és el nombre
// que arriba del input de la hora.
$start = $start_date + $start_time * 60;
$end = $end_date + $end_time * 60;
$now = time(); // Hora actual

//Crear objecte de kPAX
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

//Comprovar que el formulari s'hagi emplenat correctament
$formok = true;

//Comprovar que s'hagi escrit un títol
if($title == ''){
	register_error(elgg_echo('kpax_leagues:createform_name_failed'));
	$formok = false;
}

//Comprovar que s'hagi escrit una descripció
if($description == ''){
	register_error(elgg_echo('kpax_leagues:createform_desc_failed'));
	$formok = false;
}

//Comprovar que la competició no comenci en un moment anterior a l'actual i que la competició no acabi abans que comenci
if($leagueInfo->status == 'waiting')
{
	if($start < $now)
	{
		register_error(elgg_echo('kpax_leagues:createform_start_failed'));
		$formok = false;
	}

	if($end < $start)
	{
		register_error(elgg_echo('kpax_leagues:createform_end_failed'));
		$formok = false;
	}
}
else
{
	if($end < $leagueInfo->start / 1000)
	{
		register_error(elgg_echo('kpax_leagues:createform_end_failed'));
		$formok = false;
	}
}

//Comprovar que el tipus de puntuació sigui correcte (si la competició estava en espera)
if($leagueInfo->status == 'waiting')
{
	if($scoretype != 'scoretable' && $scoretype != 'tree' && $scoretype != 'knockout')
	{
		register_error(elgg_echo('kpax_leagues:createform_scoretype_failed'));
		$formok = false;
	}
}

//Si alguna cosa ha fallat al formulari, acabar aquí i donar l'error
if(!$formok){
	forward(REFERRER);
	die;
}

//Guardar propietats de la competició a Elgg
$leagueEntity->title = $title;

if(!$leagueEntity->save())
{
	register_error(elgg_echo('kpax_leagues:createform_error_elgg'));
	forward(REFERRER);
	die;
}

//Cridar al servei per a editar una competició
$response = $objKpax->editLeague($_SESSION['campusSession'], $idLeague, $title, $description, $start, $end, $scoretype);

switch($response)
{
	case "OK":
		//Afegir categories a la competició eliminant les anteriors
		$objKpax->clearLeagueCategories($_SESSION['campusSession'], $idLeague);
		$caterror = false;
		foreach($categories as $cat)
		{
			$response = $objKpax->addCategoryToLeague($_SESSION['campusSession'], $idLeague, $cat);
			if($response != 'OK')
			{
				$caterror = true;
			}
		}

		if($caterror)
		{
			register_error(elgg_echo('kpax_leagues:createform_categories_failed'));
		}

		//Afegir jocs a la playlist eliminant la playlist anterior

		if($leagueInfo->status == 'waiting')
		{
			$objKpax->clearPlaylist($_SESSION['campusSession'], $idLeague);
			$gameorder = 0;
			$gameerror = false;
			foreach($_POST as $key => $value)
			{
				if(stristr($key, 'league_addgame_'))
				{
					if($key != 'league_addgame_%GAMENUM%')
					{
						$response = $objKpax->addGameToPlaylist($_SESSION['campusSession'], $idLeague, (int)$value, $gameorder);
						if($response != 'OK')
						{
							$gameerror = true;
						}
						else
						{
							$gameorder++;
						}
					}
				}
			}

			if($gameerror)
			{
				register_error(elgg_echo('kpax_leagues:createform_playlist_failed'));
			}
		}

		system_message(elgg_echo('kpax_leagues:createform_sucess'));
		forward('kpax_leagues/view/'.$idLeague);
	break;
	case "VALIDATION_ERROR":
		register_error(elgg_echo('kpax_leagues:createform_error_validation'));
		forward(REFERRER);
	break;
	case "LEAGUE_NOT_EXISTS":
		register_error(elgg_echo('kpax_leagues:editform_league_not_exists'));
		forward(REFERRER);
	break;
	case "LEAGUE_FINALIZED":
		register_error(elgg_echo('kpax_leagues:editform_league_finalized'));
		forward(REFERRER);
	break;
	case "SAVING_ERROR":
		register_error(elgg_echo('kpax_leagues:createform_error_saving'));
		forward(REFERRER);
	break;
	default:
		register_error(elgg_echo('kpax_leagues:createform_error_unknown').': '.$response);
		forward(REFERRER);
	break;
}
?>