<?php
$idLeague = (int)get_input("idLeague");
$leagueEntity = get_entity($idLeague);
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

//Comprovar que la competició existeixi
if($leagueEntity->title != '')
{
	//Comprovar que l'usuari tingui permís per a editar la competició
	if($leagueEntity->canEdit())
	{
		$x = (int)get_input('crop_x');
		$y = (int)get_input('crop_y');
		$w = (int)get_input('crop_w');
		$h = (int)get_input('crop_h');

		//Comprovar que l'àrea seleccionada sigui quadrada
		if($w != $h)
		{
			register_error(elgg_echo('kpax_leagues:editform_crop_error_area'));
			forward(REFERRER);
		}
		else{
			//Obtenir la imatge original ({CACHE_ELGG}/kpax/leagues)
			$data_root = kpaxGetDataRoot();

			if(!is_dir($data_root.'leagues/'))
			{
				mkdir($data_root.'leagues/');
			}

			$origindir = $data_root.'cache/league_'.$idLeague.'.png';
			$origin = imagecreatefrompng($origindir);

			//Retallar imatge gran
			$targ_w = 200;
			$targ_h = 200;

			$dest = imagecreatetruecolor($targ_w, $targ_h);
			imagecopyresampled($dest, $origin, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
			imagepng($dest, $data_root.'leagues/'.$idLeague.'large.png');

			//Retallar imatge mitjana
			$targ_w = 100;
			$targ_h = 100;

			$dest = imagecreatetruecolor($targ_w, $targ_h);
			imagecopyresampled($dest, $origin, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
			imagepng($dest, $data_root.'leagues/'.$idLeague.'medium.png');

			//Retallar imatge petita
			$targ_w = 40;
			$targ_h = 40;

			$dest = imagecreatetruecolor($targ_w, $targ_h);
			imagecopyresampled($dest, $origin, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
			imagepng($dest, $data_root.'leagues/'.$idLeague.'small.png');

			//Retallar imatge minúscula
			$targ_w = 25;
			$targ_h = 25;

			$dest = imagecreatetruecolor($targ_w, $targ_h);
			imagecopyresampled($dest, $origin, 0, 0, $x, $y, $targ_w, $targ_h, $w, $h);
			imagepng($dest, $data_root.'leagues/'.$idLeague.'tiny.png');

			//Eliminar icona de la cache
			unlink($origindir);

			//Acabar
			system_message(elgg_echo('kpax_leagues:editicon_icon_crop_ok'));
			forward('kpax_leagues/view/'.$idLeague);
		}
	}
	else
	{
		register_error(elgg_echo('kpax_leagues:editform_not_permission'));
		forward(REFERRER);
	}
}
else
{
	register_error(elgg_echo('kpax_leagues:editform_league_not_exists'));
	forward(REFERRER);
}
?>