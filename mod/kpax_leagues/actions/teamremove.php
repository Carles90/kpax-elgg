<?php
$idTeam = get_input("idTeam");
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

$objTeam = $objKpax->getLeagueTeam($_SESSION['campusSession'], $idTeam);

if($objTeam != null)
{
	$objLeague = new League($objTeam->idLeague);
	$leagueEntity = get_entity($objTeam->idLeague);
	$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);

	if($leagueEntity->canEdit() || $objTeam->idOwner == $kpaxUser)
	{
		$response = $objLeague->removeTeam($objKpax, $idTeam);

		switch($response)
		{
			case "OK":
				system_message(elgg_echo('kpax_leagues:leagueview_teamremove_success'));
			break;
			case "INVALID_TEAM":
				register_error(elgg_echo('kpax_leagues:leagueview_teamremove_error_invalid_team'));
			break;
			case "INVALID_LEAGUE":
				register_error(elgg_echo('kpax_leagues:leagueview_teamremove_error_invalid_league'));
			break;
			case "LEAGUE_CLOSED":
				register_error(elgg_echo('kpax_leagues:leagueview_teamremove_error_closed'));
			break;
			case "VALIDATION_ERROR":
				register_error(elgg_echo('kpax_leagues:leagueview_teamremove_error_validation_error'));
			break;
			default:
				register_error(elgg_echo('kpax_leagues:leagueview_teamremove_error_unknown_error').': '.$response);
			break;
		}
	}
	else
	{
		register_error(elgg_echo('kpax_leagues:leagueview_teamremove_error_validation_error'));
	}
}
else
{
	register_error(elgg_echo('kpax_leagues:leagueview_teamremove_error_invalid_team'));
}

forward(REFERRER);
?>