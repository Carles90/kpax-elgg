<?php
$idLeague = get_input("idLeague");
$leagueEntity = get_entity($idLeague);
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

if($leagueEntity->title != '')
{
	if($leagueEntity->canEdit())
	{
		$objLeague = new League($idLeague);
		
		$image = $_FILES['icon'];
		$targetdir = kpaxGetDataRoot().'cache/';

		//Comprovar que l'arxiu enviat sigui una imatge
		$imgok = false;
		if(kpaxIsImage($image))
		{
			$target = $targetdir.'league_'.$idLeague.'.png';
			if(!kpaxConvertImageToPng($image, $target))
			{
				register_error(elgg_echo('kpax_leagues:editicon_icon_error_not_converted'));
				forward(REFERRER);
			}
		}
		else
		{
			register_error(elgg_echo('kpax_leagues:editicon_icon_error_invalid'));
			forward(REFERRER);
		}

		forward('kpax_leagues/view/'.$idLeague.'/cropicon');
	}
	else
	{
		register_error(elgg_echo('kpax_leagues:editform_not_permission'));
		forward(REFERRER);
	}
}
else
{
	register_error(elgg_echo('kpax_leagues:editform_league_not_exists'));
	forward(REFERRER);
}
?>