<?php
//Legir inputs del formulari
$name = get_input('team_name');
$password = get_input('team_password');
$idLeague = (int)get_input('idLeague');

//Obligar a que la informació del formulari es mantingui en cas de fallida
elgg_make_sticky_form('kpax_league_team');

//Crear objecte de kPAX
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

//Llegir la entitat de la competició d'Elgg
$leagueEntity = get_entity($idLeague);

//Obtenir informació sobre la competició
$objLeague = new League($idLeague);
$leagueInfo = $objLeague->getInfo($objKpax);

//Comprovar que el formulari s'hagi emplenat correctament
$formok = true;

//Comprovar que s'hagi escrit un títol
if($name == ''){
	register_error(elgg_echo('kpax_leagues:leagueview_createteam_error_name'));
	$formok = false;
}

if(!$formok)
{
	forward(REFERRER);
	die;
}

//Si existeix contrasenya, guardar el seu hash MD5
if($password != "")
{
	$password = md5($password);
}

//Comprovar que l'usuari tingui permís per a crear l'equip
$createteam = false;
$createdByAdmin = false;

if($leagueEntity->canEdit())
{
	//Si és l'administrador qui està creant l'equip (pot crear tants com vulgui sense excedir el límit)
	//No necessàriament l'administrador de la lliga ha de participar en aquesta, per tant no entrarà a l'equip
	$createteam = true;
	$createdByAdmin = true;
}
elseif($leagueInfo->allowTeamCreation == "1")
{
	//Si és un usuari qui crea l'equip i a més pot crear equips en aquesta competició.
	//S'ha de comprovar que l'usuari no estigui actualment dins d'un equip
	if($objLeague->isUserOnATeam($objKpax))
	{
		register_error(elgg_echo('kpax_leagues:leagueview_createteam_error_already_on_a_team'));
	}
	else
	{
		$createteam = true;
	}
}

if(!$createteam)
{
	forward(REFERRER);
	die;
}

$response = $objKpax->createLeagueTeam($_SESSION['campusSession'], $idLeague, $name, $password, $createdByAdmin);

switch($response)
{
	case "OK":
		system_message(elgg_echo('kpax_leagues:leagueview_createteam_success'));
		elgg_clear_sticky_form('kpax_league_team');
		forward('kpax_leagues/view/'.$idLeague.'/teams');
	break;
	case "VALIDATION_ERROR":
		register_error(elgg_echo('kpax_leagues:leagueview_createteam_error_validation'));
	break;
	case "LEAGUE_CLOSED":
		register_error(elgg_echo('kpax_leagues:leagueview_createteam_error_closed'));
	break;
	case "INVALID_LEAGUE":
		register_error(elgg_echo('kpax_leagues:leagueview_createteam_error_invalid_league'));
	break;
	case "TEAM_LIMIT_EXCEEDED":
		register_error(elgg_echo('kpax_leagues:leagueview_createteam_error_limit'));
	break;
	case "SAVING_ERROR":
		register_error(elgg_echo('kpax_leagues:leagueview_createteam_error_saving'));
	break;
	default:
		register_error(elgg_echo('kpax_leagues:leagueview_createteam_error_unknown').': '.$response);
	break;
}

forward(REFERRER);
?>