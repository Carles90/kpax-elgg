<?php
$idLeague = get_input("idLeague");
$leagueEntity = get_entity($idLeague);
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

if($leagueEntity->title != '')
{
	if($leagueEntity->canEdit())
	{
		$objLeague = new League($idLeague);
		$response = $objLeague->lockLeague($objKpax);
		
		switch($response)
		{
			case "OK":
				system_message(elgg_echo('kpax_leagues:actions_lock_success'));
				forward('kpax_leagues/view/'.$idLeague);
			break;
			case "VALIDATION_ERROR":
				register_error(elgg_echo('kpax_leagues:actions_lock_error_validation'));
				forward(REFERRER);
			break;
			case "INVALID_LEAGUE":
				register_error(elgg_echo('kpax_leagues:actions_lock_error_invalid_league'));
				forward(REFERRER);
			break;
			case "SAVING_ERROR":
				register_error(elgg_echo('kpax_leagues:actions_lock_error_saving_error'));
				forward(REFERRER);
			break;
			case "STATUS_ERROR":
				register_error(elgg_echo('kpax_leagues:actions_lock_error_status_error'));
				forward(REFERRER);
			break;
			case "EMPTY_TEAMS":
				register_error(elgg_echo('kpax_leagues:actions_lock_error_empty_teams'));
				forward(REFERRER);
			break;
			case "EMPTY_PLAYLIST":
				register_error(elgg_echo('kpax_leagues:actions_lock_error_empty_playlist'));
				forward(REFERRER);
			break;
			default:
				register_error(elgg_echo('kpax_leagues:actions_lock_error_unknown').': '.$response);
				forward(REFERRER);
			break;
		}
	}
	else
	{
		register_error(elgg_echo('kpax_leagues:actions_lock_error_validation'));
		forward(REFERRER);
	}
}
else
{
	register_error(elgg_echo('kpax_leagues:actions_lock_error_invalid_league'));
	forward(REFERRER);
}
?>