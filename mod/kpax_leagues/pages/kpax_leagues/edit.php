<?php
$guid = (int)get_input('guid');

$title = elgg_echo('kpax_leagues:editform_title');

elgg_push_breadcrumb($title);

$content = elgg_view_title($title);
$content .= elgg_view('kpax_leagues/edit');

$body = elgg_view_layout('content', array(
    'content' => $content,
    'title' => $title,
    'filter' => '',
    'header' => '',
        ));

echo elgg_view_page($title, $body);

?>