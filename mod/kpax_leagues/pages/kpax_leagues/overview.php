<?php
$title = elgg_echo('kpax_leagues:title');
$content = elgg_view('kpax_leagues/overview');

$body = elgg_view_layout('content', array(
    'content' => $content,
    'title' => $title,
    'filter' => '',
    'header' => '',
        ));

echo elgg_view_page($title, $body);
?>