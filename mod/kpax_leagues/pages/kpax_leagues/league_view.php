<?php
$idLeague = (int)get_input("idLeague");
$league_entity = get_entity($idLeague);

if($league_entity->title != '')
{
	$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
	$objLeague = new League($idLeague);
	
	$title = elgg_echo('kpax_leagues:title').': '.$objLeague->getInfo($objKpax)->title;
	
	elgg_push_breadcrumb($objLeague->getInfo($objKpax)->title);

	$content = elgg_view('kpax_leagues/view', array(
		'idLeague' => $objLeague->getId(),
	    'objKpax' => $objKpax,
	    'objLeague' => $objLeague,
	    'leagueEntity' => $league_entity
		));

	$body = elgg_view_layout('content', array(
	    'content' => $content,
	    'title' => $objLeague->getInfo($objKpax)->title,
	    'header' => '',
	    'filter' => ''
        ));
}
else
{
	$title = elgg_echo('kpax_leagues:leagueview_notfound_title');
	$content = elgg_view_title($title);
	$content .= elgg_echo('kpax_leagues:leagueview_notfound_desc');
	$body = elgg_view_layout('content', array(
	    'content' => $content,
	    'title' => $title,
	    'header' => '',
	    'filter' => ''
        ));
}


echo elgg_view_page($title, $body);
?>