<?php
/**
 * Bookmarks English language file
 */

$english = array(
	/**
	 * Main
	 */

	'kpax_leagues:title' => 'Leagues',
	'kpax_leagues:tab_all_leagues' => 'All leagues',
	'kpax_leagues:tab_waiting_leagues' => 'Waiting leagues',
	'kpax_leagues:tab_running_leagues' => 'Running leagues',
	'kpax_leagues:tab_finalized_leagues' => 'Finalized leagues',
	'kpax_leagues:no_leagues' => 'For now there aren\'t any league to show.',


	/**
	 * League box
	 */

	'kpax_leagues:leaguebox_participants' => 'Participants',
	'kpax_leagues:leaguebox_view' => 'View',
	'kpax_leagues:leaguebox_started' => 'Started',
	'kpax_leagues:leaguebox_willstart' => 'Will start',
	'kpax_leagues:leaguebox_ended' => 'Ended',
	'kpax_leagues:leaguebox_willend' => 'Will end',
	'kpax_leagues:leaguebox_status' => 'Status',
	'kpax_leagues:leaguebox_status_waiting' => 'Waiting for players...',
	'kpax_leagues:leaguebox_status_running' => 'Running',
	'kpax_leagues:leaguebox_status_finalized' => 'Finalized',
	'kpax_leagues:leaguebox_scoretype' => 'Modality',
	'kpax_leagues:leaguebox_scoretype_scoretable' => 'Score table',
	'kpax_leagues:leaguebox_scoretype_tree' => 'Match tree',
	'kpax_leagues:leaguebox_scoretype_knockout' => 'Knockout',
	'kpax_leagues:leaguebox_distribution_single' => 'single',
	'kpax_leagues:leaguebox_distribution_teams' => 'teams',
	'kpax_leagues:leaguebox_abilities' => 'Abilities',
	'kpax_leagues:leaguebox_abilities_none' => 'none',
	'kpax_leagues:leaguebox_games_none' => 'none',
	'kpax_leagues:leaguebox_games' => 'Games',
	'kpax_leagues:leaguebox_description' => 'Description',
	'kpax_leagues:leaguebox_of' => 'of',
	'kpax_leagues:button_create' => 'Create a league',

	/**
	 * League View
	 */
	'kpax_leagues:leagueview_overview' => 'Main Page',
	'kpax_leagues:leagueview_score' => 'Score',
	'kpax_leagues:leagueview_news' => 'News',
	'kpax_leagues:leagueview_teams' => 'Teams',
	'kpax_leagues:leagueview_notfound_title' => 'League not found',
	'kpax_leagues:leagueview_notfound_desc' => 'We are sorry but we cannot found the selected league.',
	'kpax_leagues:leagueview_button_delete' => 'Delete',
	'kpax_leagues:leagueview_button_edit' => 'Edit',
	'kpax_leagues:leagueview_button_join' => 'Join',
	'kpax_leagues:leagueview_button_leave' => 'Leave',
	'kpax_leagues:leagueview_button_single_lock' => 'Lock members',
	'kpax_leagues:leagueview_button_single_unlock' => 'Unlock members',
	'kpax_leagues:leagueview_button_teams_lock' => 'Lock teams',
	'kpax_leagues:leagueview_button_teams_unlock' => 'Unlock teams',
	'kpax_leagues:leagueview_alert_waiting' => 'This league should have started, but the users cannot begin it because the teams or the members are still unlocked. Please, lock in the league.',
	'kpax_leagues:leagueview_status_waiting' => 'Waiting for players',
	'kpax_leagues:leagueview_status_waiting_to_start' => 'Waiting to start date',
	'kpax_leagues:leagueview_status_running' => 'Running',
	'kpax_leagues:leagueview_status_finalized' => 'Finalized',
	'kpax_leagues:leagueview_playlist' => 'Playlist',

	//Teams
	'kpax_leagues:leagueview_teams_create_button' => 'Create a team',
	'kpax_leagues:leagueview_teams_noteams' => 'Currently there are no teams on this league.',
	'kpax_leagues:leagueview_teams_noteam_alert' => 'You are not in a team. If you don\'t suscribe to any team you won\'t can play to this league.',
	'kpax_leagues:leagueview_teams_join' => 'Join',
	'kpax_leagues:leagueview_teams_leave' => 'Leave',
	'kpax_leagues:leagueview_teams_delete' => 'Delete',
	'kpax_leagues:leagueview_teams_write_password' => 'Please, write down the team password:',
	'kpax_leagues:leagueview_teams_without_team' => 'Users without team',
	'kpax_leagues:leagueview_teams_assign' => 'Assign to a team',

	//News
	'kpax_leagues:leagueview_news_nonews' => 'This league has no news yet.',
	'kpax_leagues:leagueview_news_edit' => 'Edit news',
	'kpax_leagues:leagueview_news_edit_desc' => 'Those are the news that will be shown on the news tab of this league.',
	'kpax_leagues:leagueview_news_submit' => 'Submit',
	'kpax_leagues:leagueview_news_success' => 'The news has been successfully edited.',

	//Join to Team
	'kpax_leagues:leagueview_teamjoin_success' => 'You have been joined to this team.',
	'kpax_leagues:leagueview_teamjoin_error_invalid_team' => 'This team does not exist.',
	'kpax_leagues:leagueview_teamjoin_error_invalid_league' => 'This league does not exist.',
	'kpax_leagues:leagueview_teamjoin_error_invalid_password' => 'You have entered an invalid password.',
	'kpax_leagues:leagueview_teamjoin_error_already_on_a_team' => 'You are currently on another team.',
	'kpax_leagues:leagueview_teamjoin_error_not_a_member' => 'You cannot join to a team if you don\'t belong to this league.',
	'kpax_leagues:leagueview_teamjoin_error_team_is_full' => 'The selected team is full.',
	'kpax_leagues:leagueview_teamjoin_error_validation_error' => 'You do not have permission to enter to this team.',
	'kpax_leagues:leagueview_teamjoin_error_closed' => 'This league is currently closed and the teams cannot be modified.',
	'kpax_leagues:leagueview_teamjoin_error_saving_error' => 'Internal error while you was joining to this team.',
	'kpax_leagues:leagueview_teamjoin_error_unknown_error' => 'Unknown error while you was joining to this team',

	//Leave Team
	'kpax_leagues:leagueview_teamleave_success' => 'You have left this team.',
	'kpax_leagues:leagueview_teamleave_error_invalid_team' => 'This team does not exist.',
	'kpax_leagues:leagueview_teamleave_error_invalid_league' => 'This league does not exist.',
	'kpax_leagues:leagueview_teamleave_error_not_in_this_team' => 'You cannot leave a team that you don\'t belong.',
	'kpax_leagues:leagueview_teamleave_error_not_a_member' => 'You don\'t belong to this league.',
	'kpax_leagues:leagueview_teamleave_error_validation_error' => 'You do not have permission to leave this team.',
	'kpax_leagues:leagueview_teamleave_error_closed' => 'This league is currently closed and the teams cannot be modified.',
	'kpax_leagues:leagueview_teamleave_error_unknown_error' => 'Unknown error while you was leaving this team',

	//Remove Team
	'kpax_leagues:leagueview_teamremove_success' => 'This team has been deleted.',
	'kpax_leagues:leagueview_teamremove_error_invalid_team' => 'The selected team does not exist.',
	'kpax_leagues:leagueview_teamremove_error_invalid_league' => 'Error selecting the league.',
	'kpax_leagues:leagueview_teamremove_error_validation_error' => 'You do not have permission to remove this team.',
	'kpax_leagues:leagueview_teamremove_error_closed' => 'This league is currently closed and the teams cannot be modified.',
	'kpax_leagues:leagueview_teamremove_error_unknown_error' => 'Unknown error while it was removing the team.',

	//Kick User
	'kpax_leagues:leagueview_teamkick_success' => 'You have kicked an user.',
	'kpax_leagues:leagueview_teamkick_error_kick_yourself' => 'You are kicking yourself. Maybe you wanted to delete this team?',
	'kpax_leagues:leagueview_teamkick_error_invalid_team' => 'The selected team does not exist.',
	'kpax_leagues:leagueview_teamkick_error_invalid_league' => 'Error selecting the league.',
	'kpax_leagues:leagueview_teamkick_error_validation_error' => 'You do not have permission to kick users from this team.',
	'kpax_leagues:leagueview_teamkick_error_not_in_this_team' => 'This user is not in the selected team.',
	'kpax_leagues:leagueview_teamkick_error_closed' => 'This league is currently closed and the teams cannot be modified.',
	'kpax_leagues:leagueview_teamkick_error_unknown_error' => 'Unknown error while it was kicking an user.',

	//Assign User to a Team
	'kpax_leagues:leagueview_teamassign_success' => 'The user has been assigned to a team.',
	'kpax_leagues:leagueview_teamassign_error_invalid_team' => 'The selected team does not exist.',
	'kpax_leagues:leagueview_teamassign_error_invalid_league' => 'Error selecting the league.',
	'kpax_leagues:leagueview_teamassign_error_validation_error' => 'You do not have permission to assign users to teams.',
	'kpax_leagues:leagueview_teamassign_error_user_in_a_team' => 'This user is currently on a team.',
	'kpax_leagues:leagueview_teamassign_error_team_is_full' => 'The selected team is full.',
	'kpax_leagues:leagueview_teamassign_error_closed' => 'This league is currently closed and the teams cannot be modified.',
	'kpax_leagues:leagueview_teamassign_error_unknown_error' => 'Unknown error while it was assigning an user to a team.',

	//Create Team
	'kpax_leagues:leagueview_createteam_not_allowed' => 'We are sorry, you do not have permission to create teams on this league.',
	'kpax_leagues:leagueview_createteam_name_label' => 'Team name',
	'kpax_leagues:leagueview_createteam_name_desc' => 'Write down the name of the team that you are creating.',
	'kpax_leagues:leagueview_createteam_password_label' => 'Password',
	'kpax_leagues:leagueview_createteam_password_desc' => 'If you want you can protect this team with a password that gets in who you want. If you do not want to have a password in this team, leave blank this field.',
	'kpax_leagues:leagueview_createteam_submit' => 'Create team',
	'kpax_leagues:leagueview_createteam_success' => 'The team was successfully created.',
	'kpax_leagues:leagueview_createteam_error_name' => 'Error! The team must have a name.',
	'kpax_leagues:leagueview_createteam_error_already_on_a_team' => 'You cannot create a team because you are currently on another team.',
	'kpax_leagues:leagueview_createteam_error_validation' => 'Error! You do not have permission to create teams on this league.',
	'kpax_leagues:leagueview_createteam_error_closed' => 'Error! This league is currently closed.',
	'kpax_leagues:leagueview_createteam_error_invalid_league' => 'Error! The selected league does not exist.',
	'kpax_leagues:leagueview_createteam_error_limit' => 'Error! The team has not been created because the time limit has been exceeded.',
	'kpax_leagues:leagueview_createteam_error_saving' => 'Error! An internal error has occurred while it was creating the team.',
	'kpax_leagues:leagueview_createteam_error_unknown' => 'Error! An unknown error has occurred while it was creating the team ',

	//Score
	'kpax_leagues:leagueview_score_scoretable_user' => 'User',
	'kpax_leagues:leagueview_score_scoretable_team' => 'Team',
	'kpax_leagues:leagueview_score_scoretable_score' => 'Score',
	'kpax_leagues:leagueview_score_scoretable_avgscore' => 'Average Score',
	'kpax_leagues:leagueview_score_scoretable_pos' => 'Position',
	'kpax_leagues:leagueview_score_unavailable' => 'This league doesn\'t show any score because it hasn\'t started yet.',
	'kpax_leagues:leagueview_score_tree_winner' => 'Winner',
	'kpax_leagues:leagueview_score_tree_final' => 'Final',
	'kpax_leagues:leagueview_score_tree_semifinal' => 'Semifinal',
	'kpax_leagues:leagueview_score_tree_quarters' => 'Quarter finals',
	'kpax_leagues:leagueview_score_tree_round' => 'Round',

	/**
	 * League Create/Edit Form
	 */
	'kpax_leagues:createform_title' => 'Create a league',
	'kpax_leagues:createform_label_title' => 'League title',
	'kpax_leagues:createform_desc_title' => 'The name that the league should have.',
	'kpax_leagues:createform_label_desc' => 'League description',
	'kpax_leagues:createform_desc_desc' => 'Write down a description for these league. This description will be shown at the main page of the league.',
	'kpax_leagues:createform_label_start' => 'Start date',
	'kpax_leagues:createform_desc_start' => 'Specify the starting date of the league.',
	'kpax_leagues:createform_label_starttime' => 'Start hour',
	'kpax_leagues:createform_desc_starttime' => 'Specify the starting hour of the league.',
	'kpax_leagues:createform_label_end' => 'End date',
	'kpax_leagues:createform_desc_end' => 'Specify the end date of the league.',
	'kpax_leagues:createform_label_endtime' => 'End hour',
	'kpax_leagues:createform_desc_endtime' => 'Specify the end hour of the league.',
	'kpax_leagues:createform_label_scoretype' => 'Score type',
	'kpax_leagues:createform_desc_scoretype' => 'Chose the score type that should have the league.',
	'kpax_leagues:createform_scoretype_scoretable' => 'Score table',
	'kpax_leagues:createform_scoretype_tree' => 'Match tree',
	'kpax_leagues:createform_scoretype_knockout' => 'Knockout',
	'kpax_leagues:createform_label_distribution' => 'Member distribution',
	'kpax_leagues:createform_desc_distribution' => 'Chose if you want that users will play single or in teams.',
	'kpax_leagues:createform_distribution_single' => 'Single',
	'kpax_leagues:createform_distribution_teams' => 'Teams',
	'kpax_leagues:createform_label_maxusers' => 'Maximum of players',
	'kpax_leagues:createform_desc_maxusers' => 'Write the number of maximum players that should have the league. Leave a 0 if you don\'t want to have a limit.',
	'kpax_leagues:createform_label_allowteams' => 'Who can create teams?',
	'kpax_leagues:createform_desc_allowteams' => 'Chose if you want that all the members can create teams or only you will modify the teams.',
	'kpax_leagues:createform_allowteams_me' => 'Only me',
	'kpax_leagues:createform_allowteams_all' => 'All the members',
	'kpax_leagues:createform_label_maxgroups' => 'Maximum of teams',
	'kpax_leagues:createform_desc_maxgroups' => 'Write the number of maximum teams that should have the league. Leave a 0 if you don\'t want to have a limit.',
	'kpax_leagues:createform_label_maxusergroup' => 'Maximum of players per team',
	'kpax_leagues:createform_desc_maxusergroup' => 'Write the number of maximum players per team that should have the league. Leave a 0 if you don\'t want to have a limit.',
	'kpax_leagues:createform_label_categories' => 'Abilities',
	'kpax_leagues:createform_desc_categories' => 'Chose the abilities that you will train with this league.',
	'kpax_leagues:createform_label_playlist' => 'Playlist',
	'kpax_leagues:createform_desc_playlist' => 'This will be the playlist of your league. The users will have to start the games in the same order to play.',
	'kpax_leagues:createform_playlist_addgame' => 'Adding game',
	'kpax_leagues:createform_playlist_delete' => 'Delete',
	'kpax_leagues:createform_addgame' => 'Add another game',
	'kpax_leagues:createform_button_add' => 'Create league',

	'kpax_leagues:createform_name_failed' => 'Error! You have not written any title for this league.',
	'kpax_leagues:createform_desc_failed' => 'Error! You have not written any description for this league.',
	'kpax_leagues:createform_start_failed' => 'Error! The league cannot start in a past time.',
	'kpax_leagues:createform_end_failed' => 'Error! The end date cannot be in a moment before than the start date.',
	'kpax_leagues:createform_scoretype_failed' => 'Error! The score type is invalid.',
	'kpax_leagues:createform_distribution_failed' => 'Error! The user distribution is invalid.',
	'kpax_leagues:createform_error_elgg' => 'An internal error from Elgg has occurred and the league cannot be created.',
	'kpax_leagues:createform_sucess' => 'The league has been successfully created.',
	'kpax_leagues:createform_error_validation' => 'Error! You have not enough permission to create leagues.',
	'kpax_leagues:createform_error_saving' => 'Error! This league cannot be saved by kPAX and hasn\'t been created.',
	'kpax_leagues:createform_error_unknown' => 'And unknown error has occurred and this league cannot be created.',
	'kpax_leagues:createform_categories_failed' => 'The league has been created but it has occurred an error while adding some of his categories.',
	'kpax_leagues:createform_playlist_failed' => 'The league has been created but it has occurred and error while the playlist was creating. Please, modify it.',
	
	//Edit
	'kpax_leagues:editform_title' => 'Edit league',
	'kpax_leagues:editform_league_not_exists' => 'The selected league does not exist.',
	'kpax_leagues:editform_not_permission' => 'We are sorry, you don\'t have permission to edit a league.',
	'kpax_leagues:editform_league_finalized' => 'The selected league had finished and can\'t be edited.',

	//Edit icon
	'kpax_leagues:editicon_title' => 'Edit icon',
	'kpax_leagues:editicon_icon_title' => 'Select icon',
	'kpax_leagues:editicon_icon_desc' => 'Insert here the file that will represent the icon of this league. The image must be in format JPG, GIF or PNG.',
	'kpax_leagues:editicon_icon_submit' => 'Edit icon',
	'kpax_leagues:editicon_icon_error_invalid' => 'The image that you have sent is invalid.',
	'kpax_leagues:editicon_icon_error_not_converted' => 'The image that you have sent cannot be converted.',
	'kpax_leagues:editicon_icon_crop_ok' => 'The icon has been successfully cropped.',
	'kpax_leagues:editform_crop_noimage' => 'Before you crop an icon you must send an image.',
	'kpax_leagues:editform_crop_alt' => 'Image that will be cropped.',
	'kpax_leagues:editform_crop_preview' => 'Preview',
	'kpax_leagues:editform_crop_button' => 'Crop image',
	'kpax_leagues:editform_crop_explain' => 'This is the icon for this league that you have sent, but now you have to crop the zone of the image that will be the final icon of this league. Please, select an squared area from the next image.',
	'kpax_leagues:editform_crop_error_area' => 'The selected ara was not an square.',

	/**
	 * League User Actions
	 */
	//Leave
	'kpax_leagues:actions_leave_success' => 'You have been left this league.',
	'kpax_leagues:actions_leave_error_validation' => 'Error! You do not have permission to leave this league.',
	'kpax_leagues:actions_leave_error_invalid_league' => 'Error! This league does not exist.',
	'kpax_leagues:actions_leave_error_not_a_member' => 'Error! You cannot leave this league because you\'re not a member.',
	'kpax_leagues:actions_leave_error_subscription_closed' => 'Error! You cannot leave a league that is currently running or finalized.',
	'kpax_leagues:actions_leave_error_deleting_error' => 'Internal error while leaving the league.',
	'kpax_leagues:actions_leave_error_unknown' => 'Unknown error while leaving the league',

	//Join
	'kpax_leagues:actions_join_success' => 'Now you are a member of this league.',
	'kpax_leagues:actions_join_error_validation' => 'Error! You do not have permission to join to this league.',
	'kpax_leagues:actions_join_error_invalid_league' => 'Error! This league does not exist.',
	'kpax_leagues:actions_join_error_league_is_full' => 'Error! You cannot join to this league because it is currently full.',
	'kpax_leagues:actions_join_error_already_a_member' => 'Error! You cannot join to this league because you are currently a member.',
	'kpax_leagues:actions_join_error_subscription_closed' => 'Error! You cannot join a league that is currently running or finalized.',
	'kpax_leagues:actions_join_error_saving_error' => 'Internal error while joining this league.',
	'kpax_leagues:actions_join_error_unknown' => 'Unknown error while joining this league',

	//Lock
	'kpax_leagues:actions_lock_success' => 'The members or the teams from this league have been locked.',
	'kpax_leagues:actions_lock_error_validation' => 'Error! You do not have permission to lock this league.',
	'kpax_leagues:actions_lock_error_invalid_league' => 'Error! This league does not exist.',
	'kpax_leagues:actions_lock_error_status_error' => 'Error! You cannot lock a league that is currently locked.',
	'kpax_leagues:actions_lock_error_empty_teams' => 'Error! This league cannot be locked because it has empty teams.',
	'kpax_leagues:actions_lock_error_empty_playlist' => 'Error! This league cannot be locked because it doesn\'t have any game in his playlist.',
	'kpax_leagues:actions_lock_error_saving_error' => 'Internal error while locking this league.',
	'kpax_leagues:actions_lock_error_unknown' => 'Unknown error while locking this league',

	//Unlock
	'kpax_leagues:actions_unlock_success' => 'The members or the teams from this league have been unlocked.',
	'kpax_leagues:actions_unlock_error_validation' => 'Error! You do not have permission to unlock this league.',
	'kpax_leagues:actions_unlock_error_invalid_league' => 'Error! This league does not exist.',
	'kpax_leagues:actions_unlock_error_status_error' => 'Error! You cannot unlock a league that is currently unlocked.',
	'kpax_leagues:actions_unlock_error_started' => 'Error! You cannot unlock a league that has already started.',
	'kpax_leagues:actions_unlock_error_saving_error' => 'Internal error while unlocking this league.',
	'kpax_leagues:actions_unlock_error_unknown' => 'Unknown error while unlocking this league',

	/**
	 * League Deleting
	 */
	'kpax_leagues:deleteform_success' => 'The league has been successfully deleted.',
	'kpax_leagues:deleteform_error_elgg_entity' => 'Error! The league has been deleted but Elgg cannot delete the reference to this league.',
	'kpax_leagues:deleteform_error_not_permission' => 'Error! You do not have permission to delete this league.',
	'kpax_leagues:deleteform_error_deleting' => 'Error! An internal error has occurred while kPAX was deleting the league.',
	'kpax_leagues:deleteform_error_not_exists' => 'Error! This league does not exist.',
	'kpax_leagues:deleteform_error_unknown' => 'Unknown error while the league was deleting',
	);

add_translation('en', $english);
?>