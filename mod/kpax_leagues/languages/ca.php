<?php
/**
 * Bookmarks Catalan language file
 */

$catalan = array(
	/**
	 * Main
	 */

	'kpax_leagues:title' => 'Competicions',
	'kpax_leagues:tab_all_leagues' => 'Totes les competicions',
	'kpax_leagues:tab_waiting_leagues' => 'Competicions en espera',
	'kpax_leagues:tab_running_leagues' => 'Competicions en curs',
	'kpax_leagues:tab_finalized_leagues' => 'Competicions acabades',
	'kpax_leagues:button_create' => 'Crea una competició',
	'kpax_leagues:no_leagues' => 'Actualment no hi ha cap competició per mostrar.',

	/**
	 * League box
	 */

	'kpax_leagues:leaguebox_participants' => 'Participants',
	'kpax_leagues:leaguebox_view' => 'Veure',
	'kpax_leagues:leaguebox_started' => 'Va començar',
	'kpax_leagues:leaguebox_willstart' => 'Començarà',
	'kpax_leagues:leaguebox_ended' => 'Va acabar',
	'kpax_leagues:leaguebox_willend' => 'Acabarà',
	'kpax_leagues:leaguebox_status' => 'Estat',
	'kpax_leagues:leaguebox_status_waiting' => 'Esperant participants...',
	'kpax_leagues:leaguebox_status_running' => 'En curs',
	'kpax_leagues:leaguebox_status_finalized' => 'Finalitzada',
	'kpax_leagues:leaguebox_scoretype' => 'Modalitat',
	'kpax_leagues:leaguebox_scoretype_scoretable' => 'Taula de puntuacions',
	'kpax_leagues:leaguebox_scoretype_tree' => 'Arbre d\'enfrontaments',
	'kpax_leagues:leaguebox_scoretype_knockout' => 'Eliminatòria',
	'kpax_leagues:leaguebox_distribution_single' => 'individual',
	'kpax_leagues:leaguebox_distribution_teams' => 'per equips',
	'kpax_leagues:leaguebox_abilities' => 'Competències',
	'kpax_leagues:leaguebox_abilities_none' => 'cap',
	'kpax_leagues:leaguebox_games_none' => 'cap',
	'kpax_leagues:leaguebox_games' => 'Jocs',
	'kpax_leagues:leaguebox_description' => 'Descripció',
	'kpax_leagues:leaguebox_of' => 'de',

	/**
	 * League View
	 */
	'kpax_leagues:leagueview_overview' => 'Pàgina Inicial',
	'kpax_leagues:leagueview_score' => 'Puntuacions',
	'kpax_leagues:leagueview_news' => 'Notícies',
	'kpax_leagues:leagueview_teams' => 'Equips',
	'kpax_leagues:leagueview_notfound_title' => 'Competició no trobada',
	'kpax_leagues:leagueview_notfound_desc' => 'Ho sentim, la competició seleccionada no s\'ha trobat.',
	'kpax_leagues:leagueview_button_delete' => 'Eliminar',
	'kpax_leagues:leagueview_button_edit' => 'Editar',
	'kpax_leagues:leagueview_button_join' => 'Unir-se',
	'kpax_leagues:leagueview_button_leave' => 'Abandonar',
	'kpax_leagues:leagueview_button_single_lock' => 'Bloquejar membres',
	'kpax_leagues:leagueview_button_single_unlock' => 'Desbloquejar membres',
	'kpax_leagues:leagueview_button_teams_lock' => 'Bloquejar equips',
	'kpax_leagues:leagueview_button_teams_unlock' => 'Desbloquejar equips',
	'kpax_leagues:leagueview_alert_waiting' => 'Aquesta competició hauria d\'haver començat, però els usuaris no poden iniciar-la degut a que els equips o els membres encara estan bloquejats. Si us plau, bloquejeu-los.',
	'kpax_leagues:leagueview_status_waiting' => 'Esperant participants',
	'kpax_leagues:leagueview_status_waiting_to_start' => 'Encara no ha començat',
	'kpax_leagues:leagueview_status_running' => 'En curs',
	'kpax_leagues:leagueview_status_finalized' => 'Finalitzada',
	'kpax_leagues:leagueview_playlist' => 'Llista de reproducció',

	//News
	'kpax_leagues:leagueview_news_nonews' => 'Aquesta competició encara no té cap notícia disponible.',
	'kpax_leagues:leagueview_news_edit' => 'Editar notícies',
	'kpax_leagues:leagueview_news_edit_desc' => 'Aquestes són les notícies que es mostraran a l\'apartat de noticies de cada competició.',
	'kpax_leagues:leagueview_news_submit' => 'Trametre',
	'kpax_leagues:leagueview_news_success' => 'Les noticies s\'han editat correctament.',

	//Teams
	'kpax_leagues:leagueview_teams_create_button' => 'Crear equip',
	'kpax_leagues:leagueview_teams_noteams' => 'Per ara no hi ha cap equip en aquesta competició.',
	'kpax_leagues:leagueview_teams_noteam_alert' => 'No estàs a cap equip. Si no t\'inscrius a cap equip no podràs participar en aquesta competició.',
	'kpax_leagues:leagueview_teams_join' => 'Unir-se',
	'kpax_leagues:leagueview_teams_leave' => 'Abandonar',
	'kpax_leagues:leagueview_teams_delete' => 'Eliminar',
	'kpax_leagues:leagueview_teams_write_password' => 'Si us plau, escriviu aquí la contrasenya d\'aquest equip:',
	'kpax_leagues:leagueview_teams_without_team' => 'Usuaris sense equip',
	'kpax_leagues:leagueview_teams_assign' => 'Assignar a un equip',

	//Join to Team
	'kpax_leagues:leagueview_teamjoin_success' => 'T\'has unit a l\'equip.',
	'kpax_leagues:leagueview_teamjoin_error_invalid_team' => 'L\'equip seleccionat no existeix.',
	'kpax_leagues:leagueview_teamjoin_error_invalid_league' => 'La competició seleccionada no existeix.',
	'kpax_leagues:leagueview_teamjoin_error_invalid_password' => 'La contrasenya introduida no és correcta.',
	'kpax_leagues:leagueview_teamjoin_error_already_on_a_team' => 'Actualment ja et trobes en un altre equip.',
	'kpax_leagues:leagueview_teamjoin_error_not_a_member' => 'No et pots unir a un equip si no participes en aquesta competició.',
	'kpax_leagues:leagueview_teamjoin_error_team_is_full' => 'L\'equip seleccionat està ple.',
	'kpax_leagues:leagueview_teamjoin_error_validation_error' => 'No tens permís per a unir-te a aquest equip.',
	'kpax_leagues:leagueview_teamjoin_error_closed' => 'Aquesta competició ja està tancada i els equips no es poden modificar.',
	'kpax_leagues:leagueview_teamjoin_error_saving_error' => 'Error intern a l\'hora d\'unir-te a un equip.',
	'kpax_leagues:leagueview_teamjoin_error_unknown_error' => 'Error desconegut a l\'hora d\'unir-te a un equip',

	//Leave Team
	'kpax_leagues:leagueview_teamleave_success' => 'Has abandonat aquest equip.',
	'kpax_leagues:leagueview_teamleave_error_invalid_team' => 'L\'equip seleccionat no existeix.',
	'kpax_leagues:leagueview_teamleave_error_invalid_league' => 'La competició seleccionada no existeix.',
	'kpax_leagues:leagueview_teamleave_error_not_in_this_team' => 'No pots abandonar un equip al que no pertanys.',
	'kpax_leagues:leagueview_teamleave_error_not_a_member' => 'No formes part d\'aquesta competició.',
	'kpax_leagues:leagueview_teamleave_error_validation_error' => 'No tens permís per a abandonar aquest equip.',
	'kpax_leagues:leagueview_teamleave_error_closed' => 'Aquesta competició ja està tancada i els equips no es poden modificar.',
	'kpax_leagues:leagueview_teamleave_error_unknown_error' => 'Error desconegut a l\'hora d\'abandonar un equip',

	//Remove Team
	'kpax_leagues:leagueview_teamremove_success' => 'L\'equip s\'ha eliminat.',
	'kpax_leagues:leagueview_teamremove_error_invalid_team' => 'L\'equip seleccionat no existeix.',
	'kpax_leagues:leagueview_teamremove_error_invalid_league' => 'Error al seleccionar la competició.',
	'kpax_leagues:leagueview_teamremove_error_validation_error' => 'No tens permís per a eliminar aquest equip.',
	'kpax_leagues:leagueview_teamremove_error_closed' => 'Aquesta competició ja està tancada i els equips no es poden modificar.',
	'kpax_leagues:leagueview_teamremove_error_unknown_error' => 'Error desconegut mentre s\'eliminava un equip.',

	//Kick User
	'kpax_leagues:leagueview_teamkick_success' => 'Has expulsat a un usuari de l\'equip.',
	'kpax_leagues:leagueview_teamkick_error_kick_yourself' => 'T\'estàs expulsant a tu mateix. Potser preferies esborrar l\'equip?',
	'kpax_leagues:leagueview_teamkick_error_invalid_team' => 'L\'equip seleccionat no existeix.',
	'kpax_leagues:leagueview_teamkick_error_invalid_league' => 'Error al seleccionar la competició.',
	'kpax_leagues:leagueview_teamkick_error_validation_error' => 'No tens permís per a expulsar usuaris d\'aquest equip.',
	'kpax_leagues:leagueview_teamkick_error_not_in_this_team' => 'Aquest usuari no es troba en l\'equip seleccionat.',
	'kpax_leagues:leagueview_teamkick_error_closed' => 'Aquesta competició ja està tancada i els equips no es poden modificar.',
	'kpax_leagues:leagueview_teamkick_error_unknown_error' => 'Error desconegut mentre s\'expulsava a un usuari d\'un equip un equip.',

	//Assign User to a Team
	'kpax_leagues:leagueview_teamassign_success' => 'Has assignat a l\'usuari a un equip.',
	'kpax_leagues:leagueview_teamassign_error_invalid_team' => 'L\'equip seleccionat no existeix.',
	'kpax_leagues:leagueview_teamassign_error_invalid_league' => 'Error al seleccionar la competició.',
	'kpax_leagues:leagueview_teamassign_error_validation_error' => 'No tens permís per assignar usuaris a cap equip.',
	'kpax_leagues:leagueview_teamassign_error_user_in_a_team' => 'Aquest usuari ja es troba en un equip.',
	'kpax_leagues:leagueview_teamassign_error_team_is_full' => 'L\'equip seleccionat està ple.',
	'kpax_leagues:leagueview_teamassign_error_closed' => 'Aquesta competició ja està tancada i els equips no es poden modificar.',
	'kpax_leagues:leagueview_teamassign_error_unknown_error' => 'Error desconegut mentre s\'assignava a un usuari a un equip.',

	//Create Team
	'kpax_leagues:leagueview_createteam_not_allowed' => 'Ho sentim, no tens permís per a crear equips dins d\'aquesta competició.',
	'kpax_leagues:leagueview_createteam_name_label' => 'Nom de l\'equip',
	'kpax_leagues:leagueview_createteam_name_desc' => 'Introdueix un nom per a l\'equip que estàs creant.',
	'kpax_leagues:leagueview_createteam_password_label' => 'Contrasenya',
	'kpax_leagues:leagueview_createteam_password_desc' => 'Si vols pots protegir aquest equip per contrasenya per a que entri només qui vulguis. Si no vols donar-li cap contrasenya, deixa aquest camp en blanc.',
	'kpax_leagues:leagueview_createteam_submit' => 'Crear equip',
	'kpax_leagues:leagueview_createteam_success' => 'L\'equip s\'ha creat correctament.',
	'kpax_leagues:leagueview_createteam_error_name' => 'Error! L\'equip hauria de tenir un nom.',
	'kpax_leagues:leagueview_createteam_error_already_on_a_team' => 'No pots crear un equip perquè ja et trobes a dins d\'un altre.',
	'kpax_leagues:leagueview_createteam_error_validation' => 'Error! No tens permís per a crear un equip.',
	'kpax_leagues:leagueview_createteam_error_closed' => 'Error! Aquesta competició ja està tancada.',
	'kpax_leagues:leagueview_createteam_error_invalid_league' => 'Error! La competició seleccionada no existeix.',
	'kpax_leagues:leagueview_createteam_error_limit' => 'Error! No s\'ha creat l\'equip perquè s\'ha excedit el màxim nombre d\'equips per a aquesta competició.',
	'kpax_leagues:leagueview_createteam_error_saving' => 'Error! S\'ha produit un error intern metre s\'intentava crear l\'equip.',
	'kpax_leagues:leagueview_createteam_error_unknown' => 'Error! S\'ha produit un error desconegut mentre s\'intentava crear l\'equip',

	//Score
	'kpax_leagues:leagueview_score_scoretable_user' => 'Usuari',
	'kpax_leagues:leagueview_score_scoretable_team' => 'Equip',
	'kpax_leagues:leagueview_score_scoretable_score' => 'Puntuació',
	'kpax_leagues:leagueview_score_scoretable_avgscore' => 'Puntuació Mitjana',
	'kpax_leagues:leagueview_score_scoretable_pos' => 'Posició',
	'kpax_leagues:leagueview_score_unavailable' => 'Aquesta competició no mostra cap puntuació degut a que encara no ha començat.',
	'kpax_leagues:leagueview_score_tree_winner' => 'Guanyador/a',
	'kpax_leagues:leagueview_score_tree_final' => 'Final',
	'kpax_leagues:leagueview_score_tree_semifinal' => 'Semifinal',
	'kpax_leagues:leagueview_score_tree_quarters' => 'Quarts de final',
	'kpax_leagues:leagueview_score_tree_round' => 'Ronda',

	/**
	 * League Create/Edit Form
	 */
	'kpax_leagues:createform_title' => 'Crea una competició',
	'kpax_leagues:createform_label_title' => 'Nom de la competició',
	'kpax_leagues:createform_desc_title' => 'Introdueix el nom que vols que tingui la competició que es crearà.',
	'kpax_leagues:createform_label_desc' => 'Descripció de la competició',
	'kpax_leagues:createform_desc_desc' => 'Introdueix una descripció per a aquesta competició. Aquesta descripció es mostrarà a la pàgina principal de la competició.',
	'kpax_leagues:createform_label_start' => 'Data d\'inici',
	'kpax_leagues:createform_desc_start' => 'Especifica la data en la que començarà aquesta competició.',
	'kpax_leagues:createform_label_starttime' => 'Hora d\'inici',
	'kpax_leagues:createform_desc_starttime' => 'Especifica la hora en la que començarà aquesta competició.',
	'kpax_leagues:createform_label_end' => 'Data de finalització',
	'kpax_leagues:createform_desc_end' => 'Especifica la data en la que acabarà aquesta competició.',
	'kpax_leagues:createform_label_endtime' => 'Hora de finalització',
	'kpax_leagues:createform_desc_endtime' => 'Especifica la hora en la que acabarà aquesta competició.',
	'kpax_leagues:createform_label_scoretype' => 'Tipus de puntuació',
	'kpax_leagues:createform_desc_scoretype' => 'Tria el tipus de puntuació que tindrà la competició.',
	'kpax_leagues:createform_scoretype_scoretable' => 'Taula de puntuacions',
	'kpax_leagues:createform_scoretype_tree' => 'Arbre d\'enfrontaments',
	'kpax_leagues:createform_scoretype_knockout' => 'Classificatòria',
	'kpax_leagues:createform_label_distribution' => 'Distribució dels participants',
	'kpax_leagues:createform_desc_distribution' => 'Escull si vols que la competició funcioni de manera individual o per equips.',
	'kpax_leagues:createform_distribution_single' => 'Individual',
	'kpax_leagues:createform_distribution_teams' => 'Per equips',
	'kpax_leagues:createform_label_maxusers' => 'Màxim de participants',
	'kpax_leagues:createform_desc_maxusers' => 'Introdueix quin serà el nombre màxim de participants per a aquesta competició. Deixa 0 si vols que no hi hagi limit.',
	'kpax_leagues:createform_label_allowteams' => 'Qui pot els crear equips?',
	'kpax_leagues:createform_desc_allowteams' => 'Selecciona si tots els usuaris podran crear equips dins d\'aquesta competició o si només tu podràs crear-los.',
	'kpax_leagues:createform_allowteams_me' => 'Només jo',
	'kpax_leagues:createform_allowteams_all' => 'Tothom que hi participi',
	'kpax_leagues:createform_label_maxgroups' => 'Màxim d\'equips',
	'kpax_leagues:createform_desc_maxgroups' => 'Introdueix quin serà el nombre màxim d\'equips per a aquesta competició. Deixa 0 si vols que no hi hagi limit.',
	'kpax_leagues:createform_label_maxusergroup' => 'Màxim de participants per equip',
	'kpax_leagues:createform_desc_maxusergroup' => 'Introdueix quin serà el nombre de participants per equip per a aquesta competició. Deixa 0 si vols que no hi hagi limit.',
	'kpax_leagues:createform_label_categories' => 'Competències',
	'kpax_leagues:createform_desc_categories' => 'Tría les competències que s\'entrenaran en aquesta competició.',
	'kpax_leagues:createform_label_playlist' => 'Llista de reproducció',
	'kpax_leagues:createform_desc_playlist' => 'Aquesta serà la llista de reproducció dels jocs per a la competició. Els usuaris hauran d\'iniciar els jocs en aquest ordre per a poder participar-hi.',
	'kpax_leagues:createform_playlist_addgame' => 'Afegir joc',
	'kpax_leagues:createform_playlist_delete' => 'Eliminar',
	'kpax_leagues:createform_addgame' => 'Afegir un altre joc',
	'kpax_leagues:createform_button_add' => 'Crea la competició',

	'kpax_leagues:createform_name_failed' => 'Error! No has escrit cap nom per a aquesta competició.',
	'kpax_leagues:createform_desc_failed' => 'Error! No has escrit cap descripció per a aquesta competició.',
	'kpax_leagues:createform_start_failed' => 'Error! Una competició no pot començar en un moment passat.',
	'kpax_leagues:createform_end_failed' => 'Error! La data de finalització de la competició és anterior a la data d\'inici.',
	'kpax_leagues:createform_scoretype_failed' => 'Error! Tipus de puntuació incorrecte.',
	'kpax_leagues:createform_distribution_failed' => 'Error! Distribució d\'usuaris incorrecta.',
	'kpax_leagues:createform_error_elgg' => 'Hi ha hagut un error intern de l\'Elgg i no s\'ha pogut crear la competició.',
	'kpax_leagues:createform_sucess' => 'La competició s\'ha creat correctament.',
	'kpax_leagues:createform_error_validation' => 'Error! No tens permís per a crear competicions.',
	'kpax_leagues:createform_error_saving' => 'Error! kPAX no ha pogut emmagatzemar la teva competició. Aquesta no s\'ha creat.',
	'kpax_leagues:createform_error_unknown' => 'S\'ha produit un error desconegut a l\'hora de crear la competició.',
	'kpax_leagues:createform_categories_failed' => 'La competició s\'ha creat però hi ha hagut un error a l\'hora d\'assignar-li algunes de les seves categories.',
	'kpax_leagues:createform_playlist_failed' => 'La competició s\'ha creat però hi ha hagut un error a l\'hora de crear la llista de reproducció de jocs. Si us plau, modifiqueu-la de nou.',

	//Edit
	'kpax_leagues:editform_title' => 'Edita una competició',
	'kpax_leagues:editform_league_not_exists' => 'La competició seleccionada no existeix.',
	'kpax_leagues:editform_not_permission' => 'Ho sentim, no tens permís per a editar aquesta competició.',
	'kpax_leagues:editform_league_finalized' => 'La competició seleccionada va acabar i ja no pot ser editada.',

	//Edit icon
	'kpax_leagues:editicon_title' => 'Editar icona',
	'kpax_leagues:editicon_icon_title' => 'Seleccionar icona',
	'kpax_leagues:editicon_icon_desc' => 'Introdueix aquí l\'arxiu d\'imatge que representarà la icona d\'aquesta competició. La imatge ha d\'estar en format PNG, JPG o GIF.',
	'kpax_leagues:editicon_icon_submit' => 'Editar icona',
	'kpax_leagues:editicon_icon_error_invalid' => 'La imatge que has enviat no era vàlida.',
	'kpax_leagues:editicon_icon_error_not_converted' => 'La imatge que has enviat no s\'ha pogut convertir.',
	'kpax_leagues:editicon_icon_crop_ok' => 'La icona d\'aquesta competició s\'ha modificat correctament.',
	'kpax_leagues:editform_crop_noimage' => 'Abans de retallar la icona s\'ha d\'enviar una imatge.',
	'kpax_leagues:editform_crop_alt' => 'Imatge que s\'ha de retallar.',
	'kpax_leagues:editform_crop_preview' => 'Visualització prèvia',
	'kpax_leagues:editform_crop_button' => 'Retallar imatge',
	'kpax_leagues:editform_crop_explain' => 'Aquesta és la icona per a la competició que ens has enviat, però ara cal retallar la zona de la imatge que consistirà finalment en la icona d\'aquesta competició. Si us plau, selecciona una àrea quadrada sobre la imatge de sota.',
	'kpax_leagues:editform_crop_error_area' => 'L\'àrea seleccionada per a aquesta icona no era quadrada.',

	/**
	 * League User Actions
	 */
	//Leave
	'kpax_leagues:actions_leave_success' => 'Has abandonat la competició.',
	'kpax_leagues:actions_leave_error_validation' => 'Error! No tens permís per a abandonar aquesta competició.',
	'kpax_leagues:actions_leave_error_invalid_league' => 'Error! La competició seleccionada no existeix.',
	'kpax_leagues:actions_leave_error_not_a_member' => 'Error! No pots abandonar aquesta competició perquè no n\'ets un membre.',
	'kpax_leagues:actions_leave_error_subscription_closed' => 'Error! No pots abandonar una competició en curs o finalitzada.',
	'kpax_leagues:actions_leave_error_deleting_error' => 'Error intern a l\'hora d\'abandonar la competició.',
	'kpax_leagues:actions_leave_error_unknown' => 'Error desconegut mentre s\'abandonava la competició',

	//Join
	'kpax_leagues:actions_join_success' => 'Ara ets membre d\'aquesta competició.',
	'kpax_leagues:actions_join_error_validation' => 'Error! No tens permís per a unir-te aquesta competició.',
	'kpax_leagues:actions_join_error_invalid_league' => 'Error! La competició seleccionada no existeix.',
	'kpax_leagues:actions_join_error_league_is_full' => 'Error! No pots unir-te a aquesta competició perquè ja està plena.',
	'kpax_leagues:actions_join_error_already_a_member' => 'Error! No pots unir-te a aquesta competició perquè ja n\'ets un membre.',
	'kpax_leagues:actions_join_error_subscription_closed' => 'Error! No pots unir-te una competició en curs o finalitzada.',
	'kpax_leagues:actions_join_error_saving_error' => 'Error intern a l\'hora d\'unir-se a la competició.',
	'kpax_leagues:actions_join_error_unknown' => 'Error desconegut mentre intentaves unir-te a la competició',

	//Lock
	'kpax_leagues:actions_lock_success' => 'Els membres o equips d\'aquesta competició han estat bloquejats.',
	'kpax_leagues:actions_lock_error_validation' => 'Error! No tens permís per a bloquejar aquesta competició.',
	'kpax_leagues:actions_lock_error_invalid_league' => 'Error! La competició seleccionada no existeix.',
	'kpax_leagues:actions_lock_error_status_error' => 'Error! No es pot bloquejar una competició actualment bloquejada.',
	'kpax_leagues:actions_lock_error_empty_teams' => 'Error! No s\'ha pogut bloquejar aquesta competició perquè hi ha equips buits.',
	'kpax_leagues:actions_lock_error_empty_playlist' => 'Error! No s\'ha pogut bloquejar aquesta competició perquè no hi ha cap joc en la seva llista de reproducció.',
	'kpax_leagues:actions_lock_error_saving_error' => 'Error intern a l\'hora de bloquejar aquesta competició.',
	'kpax_leagues:actions_lock_error_unknown' => 'Error desconegut mentre s\'intentava bloquejar aquesta competició',

	//Unlock
	'kpax_leagues:actions_unlock_success' => 'Els membres o equips d\'aquesta competició han estat desbloquejats.',
	'kpax_leagues:actions_unlock_error_validation' => 'Error! No tens permís per a desbloquejar aquesta competició.',
	'kpax_leagues:actions_unlock_error_invalid_league' => 'Error! La competició seleccionada no existeix.',
	'kpax_leagues:actions_unlock_error_status_error' => 'Error! No es pot desbloquejar una competició actualment desbloquejada.',
	'kpax_leagues:actions_unlock_error_started' => 'Error! No es pot desbloquejar una competició que ja ha començat.',
	'kpax_leagues:actions_unlock_error_saving_error' => 'Error intern a l\'hora de desbloquejar aquesta competició.',
	'kpax_leagues:actions_unlock_error_unknown' => 'Error desconegut mentre s\'intentava desbloquejar aquesta competició',

	/**
	 * League Deleting
	 */
	'kpax_leagues:deleteform_success' => 'La competició s\'ha eliminat correctament.',
	'kpax_leagues:deleteform_error_elgg_entity' => 'Error! La competició s\'ha eliminat pero Elgg no ha pogut eliminar la seva referència.',
	'kpax_leagues:deleteform_error_not_permission' => 'Error! No tens permís per a eliminar aquesta competició.',
	'kpax_leagues:deleteform_error_deleting' => 'Error! Hi ha hagut un error intern a kPAX mentre s\'eliminava la competició.',
	'kpax_leagues:deleteform_error_not_exists' => 'Error! La competició especificada no existeix.',
	'kpax_leagues:deleteform_error_unknown' => 'Error desconegut mentre s\'eliminava la competició',
	);



add_translation('ca', $catalan);
?>