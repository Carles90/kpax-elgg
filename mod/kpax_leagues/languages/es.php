<?php
/**
 * Bookmarks Spanish language file
 */

$spanish = array(
	/**
	 * Main
	 */

	'kpax_leagues:title' => 'Competiciones',
	'kpax_leagues:tab_all_leagues' => 'Todas las competiciones',
	'kpax_leagues:tab_waiting_leagues' => 'Competiciones en espera',
	'kpax_leagues:tab_running_leagues' => 'Competiciones en curso',
	'kpax_leagues:tab_finalized_leagues' => 'Competiciones terminadas',
	'kpax_leagues:button_create' => 'Crear una competición',
	'kpax_leagues:no_leagues' => 'Actualmente no hay ninguna competición para mostrar.',

	/**
	 * League box
	 */

	'kpax_leagues:leaguebox_participants' => 'Participantes',
	'kpax_leagues:leaguebox_view' => 'Ver',
	'kpax_leagues:leaguebox_started' => 'Empezó',
	'kpax_leagues:leaguebox_willstart' => 'Empezará',
	'kpax_leagues:leaguebox_ended' => 'Terminó',
	'kpax_leagues:leaguebox_willend' => 'Terminará',
	'kpax_leagues:leaguebox_status' => 'Estado',
	'kpax_leagues:leaguebox_status_waiting' => 'Esperando participantes...',
	'kpax_leagues:leaguebox_status_running' => 'En curso',
	'kpax_leagues:leaguebox_status_finalized' => 'Finalizada',
	'kpax_leagues:leaguebox_scoretype' => 'Modalidad',
	'kpax_leagues:leaguebox_scoretype_scoretable' => 'Tabla de puntuaciones',
	'kpax_leagues:leaguebox_scoretype_tree' => 'Árbol de enfrentamientos',
	'kpax_leagues:leaguebox_scoretype_knockout' => 'Eliminatoria',
	'kpax_leagues:leaguebox_distribution_single' => 'individual',
	'kpax_leagues:leaguebox_distribution_teams' => 'por equipos',
	'kpax_leagues:leaguebox_abilities' => 'Competencias',
	'kpax_leagues:leaguebox_abilities_none' => 'ninguna',
	'kpax_leagues:leaguebox_games_none' => 'ninguno',
	'kpax_leagues:leaguebox_games' => 'Juegos',
	'kpax_leagues:leaguebox_description' => 'Descripción',
	'kpax_leagues:leaguebox_of' => 'de',

	/**
	 * League View
	 */
	'kpax_leagues:leagueview_overview' => 'Página Inicial',
	'kpax_leagues:leagueview_score' => 'Puntuaciones',
	'kpax_leagues:leagueview_news' => 'Noticias',
	'kpax_leagues:leagueview_teams' => 'Equipos',
	'kpax_leagues:leagueview_notfound_title' => 'Competición no encontrada',
	'kpax_leagues:leagueview_notfound_desc' => 'Lo sentimos, no hemos podido encontrar la competición seleccionada.',
	'kpax_leagues:leagueview_button_delete' => 'Eliminar',
	'kpax_leagues:leagueview_button_edit' => 'Editar',
	'kpax_leagues:leagueview_button_join' => 'Unirse',
	'kpax_leagues:leagueview_button_leave' => 'Abandonar',
	'kpax_leagues:leagueview_button_single_lock' => 'Bloquear miembros',
	'kpax_leagues:leagueview_button_single_unlock' => 'Desbloquear miembros',
	'kpax_leagues:leagueview_button_teams_lock' => 'Bloquear equipos',
	'kpax_leagues:leagueview_button_teams_unlock' => 'Desbloquear miembros',
	'kpax_leagues:leagueview_alert_waiting' => 'Esta competición debería haber comenzado, pero los usuarios no pueden iniciarla debido a que los usuarios o equipos todavía están desbloqueados. Por favor, bloquéalos.',
	'kpax_leagues:leagueview_status_waiting' => 'Esperando participantes',
	'kpax_leagues:leagueview_status_waiting_to_start' => 'Aún no ha empezado',
	'kpax_leagues:leagueview_status_running' => 'En curso',
	'kpax_leagues:leagueview_status_finalized' => 'Finalizada',
	'kpax_leagues:leagueview_playlist' => 'Lista de reproducción',

	//News
	'kpax_leagues:leagueview_news_nonews' => 'Esta competición todavía no tiene ninguna noticia disponible.',
	'kpax_leagues:leagueview_news_edit' => 'Editar noticias',
	'kpax_leagues:leagueview_news_edit_desc' => 'Estas son las noticias que se mostrarán en el apartado de noticias de la competición.',
	'kpax_leagues:leagueview_news_submit' => 'Enviar',
	'kpax_leagues:leagueview_news_success' => 'Las noticias han sido editadas correctamente.',

	//Teams
	'kpax_leagues:leagueview_teams_create_button' => 'Crear equipo',
	'kpax_leagues:leagueview_teams_noteams' => 'Por ahora no hay ningún equipo en esta competición.',
	'kpax_leagues:leagueview_teams_noteam_alert' => 'No estás en ningún equipo. Si no te inscribes a ningún equipo no podrás participar en esta competición cuando haya empezado.',
	'kpax_leagues:leagueview_teams_join' => 'Unirse',
	'kpax_leagues:leagueview_teams_leave' => 'Abandonar',
	'kpax_leagues:leagueview_teams_delete' => 'Eliminar',
	'kpax_leagues:leagueview_teams_write_password' => 'Por favor, introduce la contraseña de este equipo:',
	'kpax_leagues:leagueview_teams_without_team' => 'Usuarios sin equipo',
	'kpax_leagues:leagueview_teams_assign' => 'Asignar a un equipo',

	//Join to Team
	'kpax_leagues:leagueview_teamjoin_success' => 'Te has unido a este equipo.',
	'kpax_leagues:leagueview_teamjoin_error_invalid_team' => 'El equipo seleccionado no existe.',
	'kpax_leagues:leagueview_teamjoin_error_invalid_league' => 'La competición seleccionada no existe.',
	'kpax_leagues:leagueview_teamjoin_error_invalid_password' => 'La contraseña introducida no es correcta.',
	'kpax_leagues:leagueview_teamjoin_error_already_on_a_team' => 'Actualmente ya estás en otro equipo.',
	'kpax_leagues:leagueview_teamjoin_error_not_a_member' => 'No te puedes unir a este equipo porque no participas en esta competición.',
	'kpax_leagues:leagueview_teamjoin_error_team_is_full' => 'El equipo seleccionado está lleno.',
	'kpax_leagues:leagueview_teamjoin_error_validation_error' => 'No tienes permiso para unirte a este equipo.',
	'kpax_leagues:leagueview_teamjoin_error_closed' => 'Esta competición ya está cerrada y sus equipos no se pueden modificar.',
	'kpax_leagues:leagueview_teamjoin_error_saving_error' => 'Error interno mientras te unías a un equipo.',
	'kpax_leagues:leagueview_teamjoin_error_unknown_error' => 'Error desconocido mientras te unías a un equipo',

	//Leave Team
	'kpax_leagues:leagueview_teamleave_success' => 'Has abandonado este equipo.',
	'kpax_leagues:leagueview_teamleave_error_invalid_team' => 'El equipo seleccionado no existe.',
	'kpax_leagues:leagueview_teamleave_error_invalid_league' => 'La competición seleccionada no existe.',
	'kpax_leagues:leagueview_teamleave_error_not_in_this_team' => 'No puedes abandonar un equipo al que no perteneces.',
	'kpax_leagues:leagueview_teamleave_error_not_a_member' => 'No formas parte de esta competición.',
	'kpax_leagues:leagueview_teamleave_error_validation_error' => 'No tienes permiso para abandonar este equipo.',
	'kpax_leagues:leagueview_teamleave_error_closed' => 'Esta competición ya está cerrada y sus equipos no se pueden modificar.',
	'kpax_leagues:leagueview_teamleave_error_unknown_error' => 'Error desconocido mientras se abandonaba el equipo',

	//Remove Team
	'kpax_leagues:leagueview_teamremove_success' => 'El equipo ha sido eliminado.',
	'kpax_leagues:leagueview_teamremove_error_invalid_team' => 'El equipo seleccionado no existe.',
	'kpax_leagues:leagueview_teamremove_error_invalid_league' => 'Error al seleccionar la competición.',
	'kpax_leagues:leagueview_teamremove_error_validation_error' => 'No tienes permiso para eliminar este equipo.',
	'kpax_leagues:leagueview_teamremove_error_closed' => 'Esta competición ya está cerrada y sus equipos no se pueden modificar.',
	'kpax_leagues:leagueview_teamremove_error_unknown_error' => 'Error desconocido mientras se eliminaba el equipo.',

	//Kick User
	'kpax_leagues:leagueview_teamkick_success' => 'Has expulsado a un usuario del equipo.',
	'kpax_leagues:leagueview_teamkick_error_kick_yourself' => 'Te estás expulsando a ti mismo. ¿Quizás preferías eliminar el equipo?',
	'kpax_leagues:leagueview_teamkick_error_invalid_team' => 'El equipo seleccionado no existe.',
	'kpax_leagues:leagueview_teamkick_error_invalid_league' => 'Error al seleccionar la competición.',
	'kpax_leagues:leagueview_teamkick_error_validation_error' => 'No tienes permiso para expulsar usuarios de este equipo.',
	'kpax_leagues:leagueview_teamkick_error_not_in_this_team' => 'Este usuario no se encuentra en el equipo seleccionado.',
	'kpax_leagues:leagueview_teamkick_error_closed' => 'Esta competición ya está cerrada y los equipos no se pueden modificar.',
	'kpax_leagues:leagueview_teamkick_error_unknown_error' => 'Error desconocido mientras se expulsaba a un usuario de un equipo.',

	//Assign User to a Team
	'kpax_leagues:leagueview_teamassign_success' => 'Has asignado a este usuario a un equipo.',
	'kpax_leagues:leagueview_teamassign_error_invalid_team' => 'El equipo seleccionado no existe.',
	'kpax_leagues:leagueview_teamassign_error_invalid_league' => 'Error al seleccionar la competición.',
	'kpax_leagues:leagueview_teamassign_error_validation_error' => 'No tienes permiso para asignar usuarios a ningún equipo.',
	'kpax_leagues:leagueview_teamassign_error_user_in_a_team' => 'Este usuario ya está en un equipo.',
	'kpax_leagues:leagueview_teamassign_error_team_is_full' => 'El equipo seleccionado está lleno.',
	'kpax_leagues:leagueview_teamassign_error_closed' => 'Esta competición ya está cerrada y sus equipos no se pueden modificar.',
	'kpax_leagues:leagueview_teamassign_error_unknown_error' => 'Error desconocido mientras se asignaba a un usuario a un equipo.',

	//Create Team
	'kpax_leagues:leagueview_createteam_not_allowed' => 'Lo sentimos, no tienes permiso para crear equipos en esta competición.',
	'kpax_leagues:leagueview_createteam_name_label' => 'Nombre del equipo',
	'kpax_leagues:leagueview_createteam_name_desc' => 'Introduce un nombre para el equipo que estás creando.',
	'kpax_leagues:leagueview_createteam_password_label' => 'Contraseña',
	'kpax_leagues:leagueview_createteam_password_desc' => 'Si quieres puedes proteger este equipo por contraseña para que entre solamente quién tú quieras. Si no quieres que tenga contraseña, deja este campo en blanco.',
	'kpax_leagues:leagueview_createteam_submit' => 'Crear equipo',
	'kpax_leagues:leagueview_createteam_success' => 'El equipo se ha creado correctamente.',
	'kpax_leagues:leagueview_createteam_error_name' => '¡Error! El equipo tiene que tener un nombre.',
	'kpax_leagues:leagueview_createteam_error_already_on_a_team' => 'No puedes crear un equipo porque ya te encuentras dentro de otro.',
	'kpax_leagues:leagueview_createteam_error_validation' => '¡Error! No tienes permiso para crear un equipo.',
	'kpax_leagues:leagueview_createteam_error_closed' => '¡Error! Esta competición ya está cerrada.',
	'kpax_leagues:leagueview_createteam_error_invalid_league' => '¡Error! La competición seleccionada no existe.',
	'kpax_leagues:leagueview_createteam_error_limit' => '¡Error! No se ha podido crear el equipo porquè se ha excedido el número máximo de equipos para esta competición.',
	'kpax_leagues:leagueview_createteam_error_saving' => '¡Error! Se ha producido un error interno mientras se creaba el equipo.',
	'kpax_leagues:leagueview_createteam_error_unknown' => '¡Error! Se ha producido un error desconocido mientras se creaba el equipo',

	//Score
	'kpax_leagues:leagueview_score_scoretable_user' => 'Usuario',
	'kpax_leagues:leagueview_score_scoretable_team' => 'Equipo',
	'kpax_leagues:leagueview_score_scoretable_score' => 'Puntuación',
	'kpax_leagues:leagueview_score_scoretable_avgscore' => 'Puntuación Media',
	'kpax_leagues:leagueview_score_scoretable_pos' => 'Posición',
	'kpax_leagues:leagueview_score_unavailable' => 'Esta competición no muestra ninguna puntuación porque aún no ha empezado.',
	'kpax_leagues:leagueview_score_tree_winner' => 'Ganador/a',
	'kpax_leagues:leagueview_score_tree_final' => 'Final',
	'kpax_leagues:leagueview_score_tree_semifinal' => 'Semifinal',
	'kpax_leagues:leagueview_score_tree_quarters' => 'Cuartos de final',
	'kpax_leagues:leagueview_score_tree_round' => 'Ronda',

	/**
	 * League Create/Edit Form
	 */
	'kpax_leagues:createform_title' => 'Crear una competición',
	'kpax_leagues:createform_label_title' => 'Nombre de la competición',
	'kpax_leagues:createform_desc_title' => 'Introduce el nombre que quieras que tenga la competición que se va a crear.',
	'kpax_leagues:createform_label_desc' => 'Descripción de la competición',
	'kpax_leagues:createform_desc_desc' => 'Introduce una descripción para esta competición. Esta descripción se mostrará en la página principal de la competición.',
	'kpax_leagues:createform_label_start' => 'Fecha de inicio',
	'kpax_leagues:createform_desc_start' => 'Especifica la fecha en la que comenzará esta competición.',
	'kpax_leagues:createform_label_starttime' => 'Hora de inicio',
	'kpax_leagues:createform_desc_starttime' => 'Especifica la hora en la que comenzará esta competición.',
	'kpax_leagues:createform_label_end' => 'Fecha de finalización',
	'kpax_leagues:createform_desc_end' => 'Especifica la fecha en la que acabará esta competición.',
	'kpax_leagues:createform_label_endtime' => 'Hora de finalización',
	'kpax_leagues:createform_desc_endtime' => 'Especifica la hora en la que acabará esta competición.',
	'kpax_leagues:createform_label_scoretype' => 'Tipo de puntuación',
	'kpax_leagues:createform_desc_scoretype' => 'Escoge el tipo de puntuación que tendrá la competición.',
	'kpax_leagues:createform_scoretype_scoretable' => 'Tabla de puntuaciones',
	'kpax_leagues:createform_scoretype_tree' => 'Árbol de enfrentamientos',
	'kpax_leagues:createform_scoretype_knockout' => 'Clasificatoria',
	'kpax_leagues:createform_label_distribution' => 'Distribución de los participantes',
	'kpax_leagues:createform_desc_distribution' => 'Escoge si quieres que la competición sea individual o que funcione por equipos.',
	'kpax_leagues:createform_distribution_single' => 'Individual',
	'kpax_leagues:createform_distribution_teams' => 'Por equipos',
	'kpax_leagues:createform_label_maxusers' => 'Máximo de participantes',
	'kpax_leagues:createform_desc_maxusers' => 'Introduce cuál será el número máximo de participantes para esta competición. Deja 0 si no quieres que haya límite.',
	'kpax_leagues:createform_label_allowteams' => '¿Quién puede crear los equipos?',
	'kpax_leagues:createform_desc_allowteams' => 'Selecciona si quieres que todos los usuarios puedan organizar equipos o que seas sólo tú quién lo haga.',
	'kpax_leagues:createform_allowteams_me' => 'Sólo yo',
	'kpax_leagues:createform_allowteams_all' => 'Todos los participantes',
	'kpax_leagues:createform_label_maxgroups' => 'Máximo de equipos',
	'kpax_leagues:createform_desc_maxgroups' => 'Introduce cuál será el número máximo de equipos para esta competición. Deja 0 si no quieres que haya límite.',
	'kpax_leagues:createform_label_maxusergroup' => 'Máximo de participantes por equipo',
	'kpax_leagues:createform_desc_maxusergroup' => 'Introduce cuál será el número máximo de participantes por equipo para esta competición. Deja 0 si no quieres que haya límite.',
	'kpax_leagues:createform_label_categories' => 'Competencias',
	'kpax_leagues:createform_desc_categories' => 'Elige las competencias que se van a entrenar en esta competición.',
	'kpax_leagues:createform_label_playlist' => 'Lista de reproducción',
	'kpax_leagues:createform_desc_playlist' => 'Esta será la lista de reproducción de los juegos para la competición. Los usuarios tendrán que iniciar los juegos en el mismo orden para poder participar.',
	'kpax_leagues:createform_playlist_addgame' => 'Añadir juego',
	'kpax_leagues:createform_playlist_delete' => 'Eliminar',
	'kpax_leagues:createform_addgame' => 'Añadir otro juego',
	'kpax_leagues:createform_button_add' => 'Crear la competición',

	'kpax_leagues:createform_name_failed' => '¡Error! No has escrito ningún nombre para esta competición.',
	'kpax_leagues:createform_desc_failed' => '¡Error! No has escrito ninguna descripción para esta competición.',
	'kpax_leagues:createform_start_failed' => '¡Error! Una competición no puede empezar en el pasado.',
	'kpax_leagues:createform_end_failed' => '¡Error! La fecha de finalización de esta competición es anterior a la fecha de inicio.',
	'kpax_leagues:createform_scoretype_failed' => '¡Error! Tipo de puntuación incorrecto.',
	'kpax_leagues:createform_distribution_failed' => '¡Error! Distribución de usuarios incorrecta.',
	'kpax_leagues:createform_error_elgg' => 'Ha habido un error interno de Elgg y este no ha podido crear la competición.',
	'kpax_leagues:createform_sucess' => 'La competición se ha creado correctamente.',
	'kpax_leagues:createform_error_validation' => '¡Error! No tienes permiso para crear competiciones.',
	'kpax_leagues:createform_error_saving' => '¡Error! kPAX no ha podido guardar esta competición, por tanto no se ha creado.',
	'kpax_leagues:createform_error_unknown' => 'Se ha producido un error desconocido mientras se creaba la competición.',
	'kpax_leagues:createform_categories_failed' => 'La competición se ha creado pero ha habido un error en el momento de añadirle algunas de sus categorías.',
	'kpax_leagues:createform_playlist_failed' => 'La competición se ha creado pero ha habido un error a la hora de modificar la lista de reproducción de juegos. Por favor, modifícala de nuevo.',
	
	//Edit
	'kpax_leagues:editform_title' => 'Editar una competición',
	'kpax_leagues:editform_league_not_exists' => 'La competición seleccionada no existe.',
	'kpax_leagues:editform_not_permission' => 'Lo sentimos, no tienes permiso para editar esta competición.',
	'kpax_leagues:editform_league_finalized' => 'La competición seleccionada terminó y ya no puede ser editada.',

	//Edit icon
	'kpax_leagues:editicon_title' => 'Editar icono',
	'kpax_leagues:editicon_icon_title' => 'Seleccionar icono',
	'kpax_leagues:editicon_icon_desc' => 'Introduce aquí el archivo de la imagen que representarà el icono de esta competición. La imagen tiene que estar en formato PNG, JPG o GIF.',
	'kpax_leagues:editicon_icon_submit' => 'Editar icono',
	'kpax_leagues:editicon_icon_error_invalid' => 'La imagen que has enviado era inválida.',
	'kpax_leagues:editicon_icon_error_not_converted' => 'La imagen que has enviado no se ha podido convertir.',
	'kpax_leagues:editicon_icon_crop_ok' => 'El icono de esta competición ha sido modificado correctamente.',
	'kpax_leagues:editform_crop_noimage' => 'Antes de recortar un icono hay que enviar una imagen.',
	'kpax_leagues:editform_crop_alt' => 'Imagen que hay que recortar.',
	'kpax_leagues:editform_crop_preview' => 'Vista previa',
	'kpax_leagues:editform_crop_button' => 'Recortar imagen',
	'kpax_leagues:editform_crop_explain' => 'Este es el icono de la imagen que nos has enviado, pero ahora es necesario recortar la zona de la imagen que consistirá finalmente en el icono de la competición. Por favor, selecciona una zona cuadrada en la imagen de abajo.',
	'kpax_leagues:editform_crop_error_area' => 'El area seleccionada para este icono no era cuadrada.',

	/**
	 * League User Actions
	 */
	//Leave
	'kpax_leagues:actions_leave_success' => 'Has abandonado esta competición.',
	'kpax_leagues:actions_leave_error_validation' => 'Error! No tienes permiso para abandonar esta competición.',
	'kpax_leagues:actions_leave_error_invalid_league' => '¡Error! La competición seleccionada no existe.',
	'kpax_leagues:actions_leave_error_not_a_member' => '¡Error! No puedes abandonar esta competición porque no eres un miembro.',
	'kpax_leagues:actions_leave_error_subscription_closed' => '¡Error! No puedes abandonar una competición en curso o finalizada.',
	'kpax_leagues:actions_leave_error_deleting_error' => 'Error interno mientras se intentaba abandonar una competición.',
	'kpax_leagues:actions_leave_error_unknown' => 'Error desconocido mientras se intentaba abandonar una competición',

	//Join
	'kpax_leagues:actions_join_success' => 'Ahora eres miembro de esta competición.',
	'kpax_leagues:actions_join_error_validation' => 'Error! No tienes permiso para unir-te a esta competición.',
	'kpax_leagues:actions_join_error_invalid_league' => '¡Error! La competición seleccionada no existe.',
	'kpax_leagues:actions_join_error_league_is_full' => '¡Error! No puedes unirte a esta competición porque ya está llena.',
	'kpax_leagues:actions_join_error_already_a_member' => '¡Error! No puedes unirte a esta competición porque ya eres un miembro de ella.',
	'kpax_leagues:actions_join_error_subscription_closed' => '¡Error! No puedes unirte a una competición que está en curso o finalizada.',
	'kpax_leagues:actions_join_error_saving_error' => 'Error interno mientras intentabas unirte a esta competición.',
	'kpax_leagues:actions_join_error_unknown' => 'Error desconocido mientras intentabas unirte a esta competición',

	//Lock
	'kpax_leagues:actions_lock_success' => 'Los miembros o equipos de esta competición han sido bloqueados.',
	'kpax_leagues:actions_lock_error_validation' => '¡Error! No tienes permiso para bloquear esta competición.',
	'kpax_leagues:actions_lock_error_invalid_league' => '¡Error! La competición seleccionada no existe.',
	'kpax_leagues:actions_lock_error_status_error' => '¡Error! No se puede bloquear una competición que ya está bloqueada.',
	'kpax_leagues:actions_lock_error_empty_teams' => '¡Error! No se ha podido bloquear esta competición porque tiene equipos vacíos.',
	'kpax_leagues:actions_lock_error_empty_playlist' => '¡Error! No se ha podido bloquear esta competición porque no tiene ningún juego en su lista de reproducción.',
	'kpax_leagues:actions_lock_error_saving_error' => 'Error interno mientras se bloqueaba esta competición.',
	'kpax_leagues:actions_lock_error_unknown' => 'Error desconocido mientras se bloqueaba esta competición',

	//Unlock
	'kpax_leagues:actions_unlock_success' => 'Los miembros o equipos de esta competición han sido desbloqueados.',
	'kpax_leagues:actions_unlock_error_validation' => '¡Error! No tienes permiso para desbloquear esta competición.',
	'kpax_leagues:actions_unlock_error_invalid_league' => '¡Error! La competición seleccionada no existe.',
	'kpax_leagues:actions_unlock_error_status_error' => '¡Error! No se puede desbloquear una competición que ya está desbloqueada.',
	'kpax_leagues:actions_unlock_error_started' => '¡Error! No se puede desbloquear una competición que ya ha comenzado.',
	'kpax_leagues:actions_unlock_error_saving_error' => 'Error interno mientras se desbloqueaba esta competición.',
	'kpax_leagues:actions_unlock_error_unknown' => 'Error desconocido mientras se desbloqueaba esta competición',

	/**
	 * League Deleting
	 */
	'kpax_leagues:deleteform_success' => 'La competición se ha eliminado correctamente.',
	'kpax_leagues:deleteform_error_elgg_entity' => '¡Error! La competición se ha eliminado pero Elgg no ha podido borrar su referencia.',
	'kpax_leagues:deleteform_error_not_permission' => '¡Error! No tienes permiso para borrar esta competición.',
	'kpax_leagues:deleteform_error_deleting' => '¡Error! Ha habido un error mientras kPAX intentaba eliminar esta competición.',
	'kpax_leagues:deleteform_error_not_exists' => '¡Error! La competición especificada no existe.',
	'kpax_leagues:deleteform_error_unknown' => 'Error desconocido mientras se eliminaba la competición',
	);

add_translation('es', $spanish);
?>