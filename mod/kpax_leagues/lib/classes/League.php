<?php
class League
{
	private $idLeague;
	private $info;
	private $members;
	private $membersinfo;
	private $categories;
	private $playlist;
	private $myteam;
	private $teams;

	public function __construct($id)
	{
		$this->idLeague = $id;
	}

	public function getInfo($objKpax)
	{
		if($this->info == null)
		{
			$this->info = $objKpax->getLeague($_SESSION['campusSession'], $this->idLeague);
		}

		return $this->info;
	}

	public function setInfo($objLeague)
	{
		$this->info = $objLeague;
		$this->idLeague = $objLeague->idLeague;
	}

	public function getMembers($objKpax)
	{
		if($this->members == null)
		{
			$this->members = $objKpax->getLeagueUsers($_SESSION['campusSession'], $this->idLeague);
		}

		return $this->members;
	}

	public function getMembersInfo($objKpax)
	{
		if($this->membersinfo == null)
		{
			$this->membersinfo = $objKpax->getLeagueUsersInfo($_SESSION['campusSession'], $this->idLeague);
		}

		return $this->membersinfo;
	}

	public function getCategories($objKpax)
	{
		if($this->categories == null)
		{
			$this->categories = $objKpax->getLeagueCategories($_SESSION['campusSession'], $this->idLeague);
		}

		return $this->categories;
	}

	public function getPlaylist($objKpax)
	{
		if($this->playlist == null)
		{
			$this->playlist = $objKpax->getLeaguePlaylist($_SESSION['campusSession'], $this->idLeague);
		}

		return $this->playlist;
	}

	public function getId()
	{
		return $idLeague;
	}

	public function getPlayerNum($objKpax)
	{
		$m = $this->getMembers($objKpax);
		return count($m);
	}

	public function removeLeague($objKpax)
	{
		return $objKpax->removeLeague($_SESSION['campusSession'], $this->idLeague);
	}

	public function isUserAMember($objKpax, $idUser)
	{
		$m = $this->getMembers($objKpax);

		foreach($m as $member)
		{
			if($member->idUser == $idUser)
			{
				return true;
			}
		}

		return false;
	}

	public function leaveLeague($objKpax)
	{
		//TODO: Eliminar els seus equips si ja els havia creat
		return $objKpax->leaveLeague($_SESSION['campusSession'], $this->idLeague);
	}

	public function joinLeague($objKpax)
	{
		return $objKpax->joinLeague($_SESSION['campusSession'], $this->idLeague);
	}

	public function lockLeague($objKpax)
	{
		return $objKpax->lockLeague($_SESSION['campusSession'], $this->idLeague);
	}

	public function unlockLeague($objKpax)
	{
		return $objKpax->unlockLeague($_SESSION['campusSession'], $this->idLeague);
	}

	public function getUserTeam($objKpax)
	{
		if($myteam == null)
		{
			$myteam = $objKpax->getUserTeam($_SESSION['campusSession'], $this->idLeague);
		}

		return (int)$myteam;
	}

	public function isUserOnATeam($objKpax)
	{
		$myteam = $this->getUserTeam($objKpax);

		if($myteam != -1)
		{
			return true;
		}
		return false;
	}

	public function getTeams($objKpax)
	{
		if($teams == null)
		{
			$teams = $objKpax->getLeagueTeams($_SESSION['campusSession'], $this->idLeague);
		}

		return $teams;
	}

	public function joinTeam($objKpax, $idTeam, $password)
	{
		return $objKpax->joinLeagueTeam($_SESSION['campusSession'], $idTeam, $password);
	}

	public function leaveTeam($objKpax, $idTeam)
	{
		return $objKpax->leaveLeagueTeam($_SESSION['campusSession'], $idTeam);
	}

	public function removeTeam($objKpax, $idTeam)
	{
		return $objKpax->removeLeagueTeam($_SESSION['campusSession'], $idTeam);
	}

	public function kickUserFromTeam($objKpax, $idTeam, $idUser)
	{
		return $objKpax->kickUserFromTeam($_SESSION['campusSession'], $idTeam, $idUser);
	}

	public function assignUserToTeam($objKpax, $idTeam, $idUser)
	{
		return $objKpax->assignUserToTeam($_SESSION['campusSession'], $idTeam, $idUser);
	}

	public function getScore($objKpax, $distribution, $type)
	{
		if($distribution == 'single')
		{
			if($type == 'scoretable' || $type == 'knockout')
			{
				return $objKpax->getLeagueSingleScore($_SESSION['campusSession'], $this->idLeague);
			}
			elseif($type == 'tree')
			{
				return $objKpax->getLeagueSingleTree($_SESSION['campusSession'], $this->idLeague);
			}
		}
		else
		{
			if($type == 'scoretable' || $type == 'knockout')
			{
				return $objKpax->getLeagueTeamsScore($_SESSION['campusSession'], $this->idLeague);
			}
			elseif($type == 'tree')
			{
				return $objKpax->getLeagueTeamsTree($_SESSION['campusSession'], $this->idLeague);
			}
		}

		return null;
	}
}
?>