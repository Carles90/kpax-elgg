.league_box
{
	margin: 3px;
	padding: 5px;
	background: #DDD;
	position: relative;
	min-height: 114px;
}

.league_box:hover
{
	background: #E6E6E6;
}

.league_box h1
{
	font-size: 14px;
}

.league_box h2
{
	font-size: 12px;
	margin-bottom: 0px;
}

.league_box p
{
	margin-bottom: 0px;
}

.league_box_info
{
	margin-left: 114px;
}

.league_box_icon_tiny
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 25px;
	height: 25px;
}

.league_box_icon_small
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 40px;
	height: 40px;
}

.league_box_icon_medium
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 100px;
	height: 100px;
}

.league_box_icon_large
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 200px;
	height: 200px;
}

.league_box_icon
{
	position: absolute;
}

.league_box_icon_info
{
	font-size: 11px;
}

.league_box_icon_info_item
{
	padding-left: 20px;
	height: 16px;
	margin: 3px;
}

.league_box_icon_info_members
{
	background: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/user_icon.png') ?>") no-repeat;
}

.league_box_icon_info_view
{
	background: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/eye_icon.png') ?>") no-repeat;
}

.league_view
{
	position: relative;
}

.league_view_sidebar
{
	float: right;
	top: 0px;
	right: 0px;
	padding-left: 6px;
	border-left: 1px solid #CCC;
	padding-top: 10px;
	padding-bottom: 10px;
}

.league_view_info
{
	text-align: justify;
	margin-right: 220px;
}

.league_view_playlist
{
	width: 300px;
	margin: auto;
	margin-top: 24px;
}

.league_view_playlist_item
{
	margin: 2px;
	padding: 4px;
	font-size: 13px;
	background-color: #c0c8cc;
	text-align: center;
}

.league_view_playlist_arrow
{
	height: 32px;
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/playlist_arrow.png') ?>");
	background-position: center;
	background-repeat: no-repeat;
}

.league_list_icon_tiny
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 25px;
	height: 25px;
	overflow: hidden;
	position: relative;
}

.league_list_icon_small
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 40px;
	height: 40px;
	overflow: hidden;
	position: relative;
}

.league_list_icon_medium
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 100px;
	height: 100px;
	overflow: hidden;
	position: relative;
}

.league_list_icon_large
{
	padding: 2px;
	border: 1px solid #AAA;
	background: white;
	width: 200px;
	height: 200px;
	overflow: hidden;
	position: relative;
}

.league_list_icon
{
	position: absolute;
}

.league_list_icon_team
{
	background: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/team_avatar_bg.png') ?>");
}

.league_list_icon_team_tiny
{
	float: left;
	width: 12px;
	height: 12px;
	overflow: hidden;
	font-size: 5px;
}

.league_list_icon_team_tiny img
{
	width: 12px;
	height: 12px;
}

.league_list_icon_team_small
{
	float: left;
	width: 20px;
	height: 20px;
	overflow: hidden;
}

.league_list_icon_team_small img
{
	width: 20px;
	height: 20px;
}

.league_list_icon_team_medium
{
	float: left;
	width: 50px;
	height: 50px;
	overflow: hidden;
}

.league_list_icon_team_medium img
{
	width: 50px;
	height: 50px;
}

.league_list_icon_team_large
{
	float: left;
	width: 100px;
	height: 100px;
	overflow: hidden;
}

.league_list_icon_team_large img
{
	width: 100px;
	height: 100px;
}

.league_list_icon_status
{
	position: absolute;
	bottom: 4px;
	right: 4px;
	width: 16px;
	height: 16px;
}

.league_view_sidebar_item
{
	padding: 4px;
	padding-top: 2px;
	padding-left: 28px;
	height: 16px;
	margin: 3px;
	background-color: #EEE;
	background-repeat: no-repeat;
	background-position: 4px 4px;
	border: 1px dashed #BBB;
	font-size: 11px;
}

.league_view_sidebar_item_members
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/user_icon.png') ?>");
}

.league_view_sidebar_item_join
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/user_add.png') ?>");
}

.league_view_sidebar_item_edit
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/edit_icon.png') ?>");
}

.league_view_sidebar_item_delete
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/delete_icon.png') ?>");
}

.league_view_sidebar_item_leave
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/user_delete.png') ?>");
}

.league_view_sidebar_item_lock
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/lock.png') ?>");
}

.league_view_sidebar_item_unlock
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/lock_open.png') ?>");
}

.league_view_sidebar_item_status_running
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/running.png') ?>");
	border: 1px dashed #51b84f;
	background-color: #bfffbe;
}

.league_view_sidebar_item_status_waiting
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/waiting.png') ?>");
	border: 1px dashed #4e72c7;
	background-color: #bed1ff;
}

.league_view_sidebar_item_status_finalized
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/finalized.png') ?>");
	border: 1px dashed #c74e4e;
	background-color: #ffc2c2;
}

.league_create_button
{
	position: absolute;
	top: 20px;
	right: 20px;
}

.league_alert
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/exclamation.png') ?>");
	background-repeat: no-repeat;
	background-color: #ffcfcf;
	background-position: 8px 8px;
	padding: 8px;
	padding-left: 48px;
	border: 1px solid #c14d4d;
	min-height: 32px;
	margin-bottom: 16px;
}

.league_teams
{
	margin: 0 auto;
	text-align: center;
}

.league_teams_box
{
	display: inline-block;
	width: 45%;
	border: 1px solid #AAA;
	border-radius: 10px;
	margin: 5px;
	margin-bottom: 10px;
	padding: 10px;
	position: relative;
	vertical-align: top;
}

.league_teams_box h1
{
	background-color: white;
	position: absolute;
	top: -8px;
	left: 15px;
	font-size: 16px;
	padding: 0px;
	margin: 0px;
	padding-left: 5px;
	padding-right: 5px;
}

.league_teams_box_username
{
	font-weight: bold;
	font-size: 14px;
}

.league_teams_box_members
{
	float: left;
	height: 16px;
	font-size: 11px;
	background: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/user_icon.png') ?>") no-repeat;
	margin: 0px;
	margin-right: 5px;
	padding: 0px;
	padding-left: 18px;
}

.league_teams_box_locked
{
	float: left;
	height: 16px;
	width: 16px;
	background: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/lock.png') ?>");
	margin: 0px;
	padding: 0px;
}

.league_teams_box_join
{
	float: right;
	height: 16px;
	font-size: 11px;
	background: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/user_add.png') ?>") no-repeat;
	margin: 0px;
	margin-left: 5px;
	padding: 0px;
	padding-left: 18px;
}

.league_teams_box_delete
{
	float: right;
	height: 16px;
	font-size: 11px;
	background: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/delete_icon.png') ?>") no-repeat;
	margin: 0px;
	margin-left: 5px;
	padding: 0px;
	padding-left: 18px;
}

.league_teams_box_leave
{
	float: right;
	height: 16px;
	font-size: 11px;
	background: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/user_delete.png') ?>") no-repeat;
	margin: 0px;
	margin-left: 5px;
	padding: 0px;
	padding-left: 18px;
}

.league_teams_box_kick
{
	position: absolute;
	width: 16px;
	height: 16px;
	background: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/delete_icon.png') ?>") no-repeat;
	right: 20px;
	top: 3px;
}

.league_teams_box table
{
	width: 100%;
	margin: 10px;
}

.league_teams_box table td
{
	vertical-align: middle;
}

.league_teams_box_username
{
	padding-left: 5px;
	padding-right: 10px;
	text-align: left;
	position: relative;
}

.league_teams_box_avatar
{
	width: 25px;
	padding: 1px;
}

.league_teams_box_assign select
{
	padding: 0px;
	margin: 2px;
	font-size: 12px;
}

.league_scoretable
{
	width: 100%;
	margin-bottom: 200px;
}

.league_scoretable tr td
{
	text-align: center;
	vertical-align: middle;
}

.league_scoretable_header
{
	background-color: #333;
	border-bottom: 1px solid black;
}

.league_scoretable_header td
{
	color: #EEE;
	height: 24px;
	font-weight: bold;
}

.league_scoretable_zebra td
{
	height: 32px;
}

.league_scoretable_zebra:hover
{
	background-color: #BBBBFF;
}

.league_scoretable_zebra_0
{
	background-color: #EEE;
}

.league_scoretable_zebra_1
{
	background-color: #DDD;
}

.league_scoretable_zebra_me
{
	background-color: #b9e0ff;
}

.league_scoretable_zebra_me td
{
	font-weight: bold;
}

.league_scoretable_username
{
	position: relative;
}

.league_scoretable_details
{
	color: white;
	font-size: 10px;
	background-color: rgba(0, 0, 0, 0.7);
	position: absolute;
	left: 20px;
	top: 28px;
	width: 200px;
	display: none;
	z-index: 1;
}

.league_scoretable_username:hover .league_scoretable_details
{
	display: block;
}

.league_scoretable_details table
{
	height: 12px;
	width: 100%;
	text-align: left;
	font-weight: bold;
}

.league_scoretable_details table tr td
{
	height: 12px;
	text-align: left;
	font-weight: bold;
	padding: 2px;
}

.league_scoretable_teams_details
{
	color: white;
	font-size: 10px;
	background-color: rgba(0, 0, 0, 0.7);
	position: absolute;
	left: 20px;
	top: 28px;
	display: none;
	z-index: 1;
}

.league_scoretable_username:hover .league_scoretable_teams_details
{
	display: inline-block;
}

.league_scoretable_teams_details table 
{
	margin: 2px;
}

.league_scoretable_teams_details table tr td 
{
	text-align: right;
	white-space: nowrap;
	min-width: 50px;
	height: 16px;
	border: 1px solid #666;
	padding: 2px;
}

.league_scoretable_knockout
{
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/knockout_bg.png') ?>");
}

.league_scoretable_knockout_flag
{
	width: 100%;
	height: 100%;
	position: relative;
}

.league_scoretable_knockout_yes
{
	width: 16px;
	height: 16px;
	position: absolute;
	top: 8px;
	left: 40px;
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/flag_knockout.png') ?>");
}

.league_scoretable_knockout_no
{
	width: 16px;
	height: 16px;
	position: absolute;
	top: 8px;
	left: 40px;
	background-image: url("<?php echo(''.elgg_get_site_url().'mod/kpax_leagues/graphics/flag_ok.png') ?>");
}

.league_tree
{
	margin-bottom: 20px;
}

.league_tree_floor
{
	border-bottom: 1px solid #EEE;
	position: relative;
}

.league_tree_floor_big
{
	height: 140px;
}

.league_tree_floor_medium
{
	height: 80px;
}

.league_tree_floor_small
{
	height: 65px;
}

.league_tree_floor h1
{
	margin: 0px;
	margin-top: 4px;
	font-size: 16px;
}

.league_tree_leaf_container
{
	position: absolute;
	top: 28px;
}

.league_tree_leaf
{
	position: relative;
	top: 0px;
	left: -50%;
	background: white;
	padding: 2px;
	border: 1px solid #AAA;
}

.league_tree_leaf_big
{
	width: 100px;
	height: 100px;
}

.league_tree_leaf_medium
{
	width: 40px;
	height: 40px;
}

.league_tree_leaf_small
{
	width: 25px;
	height: 25px;
}

.league_tree_leaf_union
{
	border: 1px solid #AAA;
	border-bottom: none;
	height: 5px;
	position: absolute;
	top: 22px;
}

.league_tree_leaf_union2
{
	border-right: 1px solid #AAA;
	height: 33px;
	position: absolute;
	top: -11px;
}

.league_tree_leaf_username
{
	padding: 4px;
	position: absolute;
	left: 0px;
	bottom: -30px;
	background: rgba(0, 0, 0, 0.7);
	color: white;
	font-size: 10px;
	display: none;
	z-index: 2;
	white-space: nowrap;
}

.league_tree_leaf:hover .league_tree_leaf_username
{
	display: block;
}

#league_icon_tocrop
{
	display: inline-block;
	border: 1px solid #AAA;
	padding: 2px;
}

#league_icon
{
	padding: 0px;
	margin: 0px;
}

#league_icon_preview
{
	display: inline-block;
	padding: 2px;
	border: 1px solid #AAA;
}

#league_icon_preview_container
{
	width: 100px;
	height: 100px;
	padding: 0px;
	overflow: hidden;
	background: black;
}

#league_icon_preview_container img
{
	display: none;
}