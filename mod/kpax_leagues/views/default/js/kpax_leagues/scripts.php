var games_iter = 1;
function addLeagueGame(){
	var sample_div = document.getElementById('add_game_sample');
	var games_div = document.getElementById('add_game_inputs');

	var sample_content = sample_div.innerHTML.split('%GAMENUM%').join(games_iter);
	var new_div = document.createElement('div');
	new_div.id = 'add_game_' + games_iter;
	new_div.innerHTML = '<br/>' + sample_content;
	games_div.appendChild(new_div);

	games_iter++;
}

function deleteLeagueGame(id){
	var to_delete = document.getElementById('add_game_' + id);
	to_delete.parentNode.removeChild(to_delete);
}

function leagueOnChangeDistr(){
	var value = document.league_create_form.league_distribution.value;
	var teamsdiv = document.getElementById('leagueform_teams');

	if(value == 'teams'){
		teamsdiv.style.display = "block";
	}
	else{
		teamsdiv.style.display = "none";
	}
}

function joinTeamWithPassword(idTeam)
{
	var password = prompt("<?php echo(elgg_echo('kpax_leagues:leagueview_teams_write_password')) ?>");

	if(password != null)
	{
		var form = document.createElement("form");
	    form.setAttribute("method", "post");
	    form.setAttribute("action", "<?php echo(elgg_get_site_url()) ?>/kpax_leagues/teamjoin/" + idTeam);

	    var hiddenField = document.createElement("input");
	    hiddenField.setAttribute("type", "hidden");
	    hiddenField.setAttribute("name", "password");
	    hiddenField.setAttribute("value", password);

	    form.appendChild(hiddenField);

	    document.body.appendChild(form);
	    form.submit();
	}
}

function assignUserToTeam(idUser, input)
{
	window.location = "<?php echo(elgg_get_site_url()) ?>kpax_leagues/teamassign/" + input.value + "/" + idUser + "";
}