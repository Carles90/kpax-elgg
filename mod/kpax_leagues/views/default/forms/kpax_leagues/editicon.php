<?php
$idLeague = (int)get_input('idLeague');

echo elgg_view('input/hidden', array('name' => 'idLeague', 'value' => $idLeague));
?>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:editicon_icon_title')); ?></label><br/>
	<?php
	echo elgg_view('input/file', array('name' => 'icon'));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:editicon_icon_desc')); ?></p>
</div>

<div>
<?php
echo elgg_view('input/submit', array('value' => elgg_echo('kpax_leagues:editicon_icon_submit')));
?>
</div>