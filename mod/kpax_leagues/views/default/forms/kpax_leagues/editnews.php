<?php
$idLeague = (int)get_input('idLeague');
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$leagueObj = new League($idLeague);
$leagueInfo = $leagueObj->getInfo($objKpax);

echo elgg_view('input/hidden', array('name' => 'idLeague', 'value' => $idLeague))
?>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:leagueview_news_edit')) ?></label><br/>
	<?php
	echo elgg_view('input/longtext', array('name' => 'news', 'value' => $leagueInfo->news));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:leagueview_news_edit_desc')) ?></p>
</div>

<div>
<?php
echo elgg_view('input/submit', array('value' => elgg_echo('kpax_leagues:leagueview_news_submit')));
?>
</div>