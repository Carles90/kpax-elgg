<?php
$dataroot = kpaxGetDataRoot();

$idLeague = (int)get_input('idLeague');
?>
<script type="text/javascript">
	jQuery(function($){
		var jcrop_api,
	        boundx,
	        boundy,

	        $preview = $('#league_icon_preview'),
	        $pcnt = $('#league_icon_preview #league_icon_preview_container'),
	        $pimg = $('#league_icon_preview #league_icon_preview_container img'),

	        xsize = $pcnt.width(),
	        ysize = $pcnt.height();
	    $('#icon_crop_target').Jcrop({
			onChange: updatePreview,
			onSelect: updatePreview,
			aspectRatio: 1
	    },function(){
			// Use the API to get the real image size
			var bounds = this.getBounds();
			boundx = bounds[0];
			boundy = bounds[1];
			var size = Math.min(boundx, boundy);
			// Store the API in the jcrop_api variable
			jcrop_api = this;
			jcrop_api.setSelect([0,0,size,size])
	    });

	    function updatePreview(c)
	    {
	    	if (parseInt(c.w) > 0)
			{
				var rx = xsize / c.w;
				var ry = ysize / c.h;

				$pimg.css({
					width: Math.round(rx * boundx) + 'px',
					height: Math.round(ry * boundy) + 'px',
					marginLeft: '-' + Math.round(rx * c.x) + 'px',
					marginTop: '-' + Math.round(ry * c.y) + 'px',
					display: 'block'
				});

				$("#crop_x").val(c.x);
				$("#crop_y").val(c.y);
				$("#crop_w").val(c.w);
				$("#crop_h").val(c.h);
			}
	    }
	});
</script>
<p><?php echo(elgg_echo('kpax_leagues:editform_crop_explain')) ?></p>
<div id="league_icon_tocrop">
	<div id="league_icon">
		<img src="<?php echo(elgg_get_site_url()) ?>mod/kpax_leagues/icondirect.php?idLeague=<?php echo($idLeague) ?>&amp;size=cache" id="icon_crop_target" alt="<?php echo(elgg_echo('kpax_leagues:editform_crop_alt')) ?>" />
	</div>
</div>
<br/><br/>
<p><label><?php echo(elgg_echo('kpax_leagues:editform_crop_preview')) ?></label></p>
<div id="league_icon_preview">
	<div id="league_icon_preview_container">
		<img src="<?php echo(elgg_get_site_url()) ?>mod/kpax_leagues/icondirect.php?idLeague=<?php echo($idLeague) ?>&amp;size=cache" alt="<?php echo(elgg_echo('kpax_leagues:editform_crop_preview')) ?>" />
	</div>
</div>
<div>
<?php
echo elgg_view('input/hidden', array('name' => 'crop_x', 'id' => 'crop_x', 'value' => '0'));
echo elgg_view('input/hidden', array('name' => 'crop_y', 'id' => 'crop_y', 'value' => '0'));
echo elgg_view('input/hidden', array('name' => 'crop_w', 'id' => 'crop_w', 'value' => '0'));
echo elgg_view('input/hidden', array('name' => 'crop_h', 'id' => 'crop_h', 'value' => '0'));
echo elgg_view('input/hidden', array('name' => 'idLeague', 'id' => 'idLeague', 'value' => $idLeague));
echo elgg_view('input/submit', array('value' => elgg_echo('kpax_leagues:editform_crop_button')));
?>
</div>