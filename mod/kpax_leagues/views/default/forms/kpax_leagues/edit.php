<?php
$idLeague = get_input("idLeague");
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);

$gameList = $objKpax->getListGames($_SESSION['campusSession'], 0, 0);
$catList = $objKpax->getCategories($_SESSION['campusSession']);
$leagueObj = new League($idLeague);
$leagueInfo = $leagueObj->getInfo($objKpax);

echo elgg_view('input/hidden', array('name' => 'idLeague', 'value' => $idLeague ));
?>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_title')) ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'league_title', 'value' => $leagueInfo->title));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_title')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_desc')) ?></label><br/>
	<?php
	echo elgg_view('input/longtext', array('name' => 'league_description', 'value' => $leagueInfo->description));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_desc')) ?></p>
</div>

<?php
if($leagueInfo->status == 'waiting')
{
	?>
	<div>
		<label><?php echo(elgg_echo('kpax_leagues:createform_label_start')) ?></label><br/>
		<?php
		echo elgg_view('input/date', array('name' => 'league_start_date', 'timestamp' => true, 'value' => ($leagueInfo->start / 1000) - ($leagueInfo->start / 1000) % 86400));
		?>
		<p><?php echo(elgg_echo('kpax_leagues:createform_desc_start')) ?></p>
	</div>

	<div>
		<label><?php echo(elgg_echo('kpax_leagues:createform_label_starttime')) ?></label><br/>
		<?php
		echo elgg_view('input/timepicker', array('name' => 'league_start_time', 'value' => (($leagueInfo->start / 1000) % 86400) / 60));
		?>
		<p><?php echo(elgg_echo('kpax_leagues:createform_desc_starttime')) ?></p>
	</div>

	<?php
}
?>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_end')) ?></label><br/>
	<?php
	echo elgg_view('input/date', array('name' => 'league_end_date', 'timestamp' => true, 'value' => ($leagueInfo->end / 1000) - ($leagueInfo->end / 1000) % 86400));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_end')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_endtime')) ?></label><br/>
	<?php
	echo elgg_view('input/timepicker', array('name' => 'league_end_time', 'value' => (($leagueInfo->end / 1000) % 86400) / 60));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_endtime')) ?></p>
</div>

<?php
if($leagueInfo->status == 'waiting')
{
	?>
	<div>
		<label><?php echo(elgg_echo('kpax_leagues:createform_label_scoretype')) ?></label><br/>
		<?php
		$options = array();
		$options["scoretable"] = elgg_echo('kpax_leagues:createform_scoretype_scoretable');
		$options["tree"] = elgg_echo('kpax_leagues:createform_scoretype_tree');
		$options["knockout"] = elgg_echo('kpax_leagues:createform_scoretype_knockout');
		echo elgg_view('input/dropdown', array('name' => 'league_scoretype', 'options_values' => $options, 'value' => $leagueinfo->scoreType));
		?>
		<p><?php echo(elgg_echo('kpax_leagues:createform_desc_scoretype')) ?></p>
	</div>
	<?php
}
?>

<div> 
    <label><?php echo(elgg_echo('kpax_leagues:createform_label_categories')) ?></label><br />
    <?php
    	$options = array();
    	$categories = $leagueObj->getCategories($objKpax);
    	$cats = array();

    	foreach($categories as $c)
    	{
    		array_push($cats, $c->idCategory);
    	}

    	foreach($catList as $cat)
    	{
    		$options[$cat->name] = $cat->idCategory;
    	}
    	echo elgg_view('input/checkboxes', array('name' => "league_categories", 'options' => $options, 'value' => $cats)); 
    ?>
    <p><?php echo(elgg_echo('kpax_leagues:createform_desc_categories')) ?></p>
</div>

<?php
if($leagueInfo->status == 'waiting')
{
	?>
	<div id="add_game_info">
		<label><?php echo(elgg_echo('kpax_leagues:createform_label_playlist')) ?></label><br/>
		<p><?php echo(elgg_echo('kpax_leagues:createform_desc_playlist')) ?></p>
	</div>
	<div id="add_game_sample" style="display:none;">
		<label><?php echo(elgg_echo('kpax_leagues:createform_playlist_addgame')) ?> %GAMENUM% (<a href="javascript:void(0)" onclick="deleteLeagueGame(%GAMENUM%)"><?php echo(elgg_echo('kpax_leagues:createform_playlist_delete')) ?></a>)</label><br/>
		<?php
		$options = array();
		foreach($gameList as $game){
			$options[$game->idGame] = $game->name;
		}
		echo elgg_view('input/dropdown', array('name' => 'league_addgame_%GAMENUM%', 'options_values' => $options));
		?>
	</div>
	<div id="add_game_inputs">
		<?php
		$playlist = $leagueObj->getPlaylist($objKpax);
		$totaladded = 1;
		foreach($playlist as $p)
		{
			?>
			<div id="add_game_<?php echo($totaladded) ?>">
				<br/>
				<label><?php echo(elgg_echo('kpax_leagues:createform_playlist_addgame')) ?> <?php echo($totaladded) ?> (<a href="javascript:void(0)" onclick="deleteLeagueGame(<?php echo($totaladded) ?>)"><?php echo(elgg_echo('kpax_leagues:createform_playlist_delete')) ?></a>)</label><br/>
				<?php
				$options = array();
				foreach($gameList as $game){
					$options[$game->idGame] = $game->name;
				}
				echo elgg_view('input/dropdown', array('name' => 'league_addgame_'.$totaladded.'', 'options_values' => $options, 'value' => $p->idGame));
				?>
			</div>
			<?php
			$totaladded++;
		}
		?>
		<script>
		games_iter = <?php echo($totaladded); ?>;
		</script>
	</div>
	<div>
		<p><a href="javascript:void(0)" onclick="addLeagueGame()">(+) <?php echo(elgg_echo('kpax_leagues:createform_addgame')) ?></a></p>
	</div>
	<?php
}
?>
<div>
<?php
echo elgg_view('input/submit', array('value' => elgg_echo('kpax_leagues:createform_button_add')));
?>
</div>