<?php
$idLeague = (int)get_input('idLeague');

if(elgg_is_sticky_form('kpax_league_team'))
{
	$sticky = elgg_get_sticky_values('kpax_league_team');
	elgg_clear_sticky_form('kpax_league_team');
}

echo elgg_view('input/hidden', array('name' => 'idLeague', 'value' => $idLeague));
?>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:leagueview_createteam_name_label')) ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'team_name', 'value' => $sticky['team_name']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:leagueview_createteam_name_desc')) ?></p>
</div>
<div>
	<label><?php echo(elgg_echo('kpax_leagues:leagueview_createteam_password_label')) ?></label><br/>
	<?php
	echo elgg_view('input/password', array('name' => 'team_password', 'value' => $sticky['team_password']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:leagueview_createteam_password_desc')) ?></p>
</div>

<div>
<?php
echo elgg_view('input/submit', array('value' => elgg_echo('kpax_leagues:leagueview_createteam_submit')));
?>
</div>