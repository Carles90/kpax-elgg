<?php
$username = elgg_get_logged_in_user_entity()->username;

$objKpax = new kpaxSrv($username);

$gameList = $objKpax->getListGames($_SESSION['campusSession'], 0, 0);
$catList = $objKpax->getCategories($_SESSION['campusSession']);

if(elgg_is_sticky_form('kpax_league'))
{
	$sticky = elgg_get_sticky_values('kpax_league');
	elgg_clear_sticky_form('kpax_league');
}

?>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_title')) ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'league_title', 'value' => $sticky['league_title']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_title')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_desc')) ?></label><br/>
	<?php
	echo elgg_view('input/longtext', array('name' => 'league_description', 'value' => $sticky['league_description']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_desc')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_start')) ?></label><br/>
	<?php
	echo elgg_view('input/date', array('name' => 'league_start_date', 'timestamp' => true, 'value' => $sticky['league_start_date']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_start')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_starttime')) ?></label><br/>
	<?php
	echo elgg_view('input/timepicker', array('name' => 'league_start_time', 'value' => $sticky['league_start_time']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_starttime')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_end')) ?></label><br/>
	<?php
	echo elgg_view('input/date', array('name' => 'league_end_date', 'timestamp' => true, 'value' => $sticky['league_end_date']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_end')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_endtime')) ?></label><br/>
	<?php
	echo elgg_view('input/timepicker', array('name' => 'league_end_time', 'value' => $sticky['league_end_time']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_endtime')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_scoretype')) ?></label><br/>
	<?php
	$options = array();
	$options["scoretable"] = elgg_echo('kpax_leagues:createform_scoretype_scoretable');
	$options["tree"] = elgg_echo('kpax_leagues:createform_scoretype_tree');
	$options["knockout"] = elgg_echo('kpax_leagues:createform_scoretype_knockout');
	echo elgg_view('input/dropdown', array('name' => 'league_scoretype', 'options_values' => $options, 'value' => $sticky['league_scoretype']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_scoretype')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_distribution')) ?></label><br/>
	<?php
	$options = array();
	$options["single"] = elgg_echo('kpax_leagues:createform_distribution_single');
	$options["teams"] = elgg_echo('kpax_leagues:createform_distribution_teams');
	echo elgg_view('input/dropdown', array('name' => 'league_distribution', 'options_values' => $options, 'onchange' => 'leagueOnChangeDistr()', 'value' => $sticky['league_distribution']));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_distribution')) ?></p>
</div>

<div>
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_maxusers')) ?></label><br/>
	<?php
	echo elgg_view('input/text', array('name' => 'league_maxusers', 'value' => isset($sticky['league_maxusers']) ? $sticky['league_maxusers'] : '0'));
	?>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_maxusers')) ?></p>
</div>

<?php
$teamsdisplay = 'none';

if(isset($sticky['league_distribution']))
{
	if($sticky['league_distribution'] == 'teams')
	{
		$teamsdisplay = 'block';
	}
}
?>

<div id="leagueform_teams" style="display: <?php echo($teamsdisplay) ?>;">
	<div>
		<label><?php echo(elgg_echo('kpax_leagues:createform_label_allowteams')) ?></label><br/>
		<?php
		$options = array();
		$options["0"] = elgg_echo('kpax_leagues:createform_allowteams_me');
		$options["1"] = elgg_echo('kpax_leagues:createform_allowteams_all');
		echo elgg_view('input/dropdown', array('name' => 'league_allowteams', 'options_values' => $options, 'value' => $sticky['league_allowteams']));
		?>
		<p><?php echo(elgg_echo('kpax_leagues:createform_desc_allowteams')) ?></p>
	</div>
	<br/>
	<div>
		<label><?php echo(elgg_echo('kpax_leagues:createform_label_maxgroups')) ?></label><br/>
		<?php
		echo elgg_view('input/text', array('name' => 'league_maxgroups', 'value' => isset($sticky['league_maxgroups']) ? $sticky['league_maxgroups'] : '0'));
		?>
		<p><?php echo(elgg_echo('kpax_leagues:createform_desc_maxgroups')) ?></p>
	</div>
	<br/>
	<div>
		<label><?php echo(elgg_echo('kpax_leagues:createform_label_maxusergroup')) ?></label><br/>
		<?php
		echo elgg_view('input/text', array('name' => 'league_maxusergroup', 'value' => isset($sticky['league_maxusergroup']) ? $sticky['league_maxusergroup'] : '0'));
		?>
		<p><?php echo(elgg_echo('kpax_leagues:createform_desc_maxusergroup')) ?></p>
	</div>
</div>

<div> 
    <label><?php echo(elgg_echo('kpax_leagues:createform_label_categories')) ?></label><br />
    <?php
    	$options = array();
    	foreach($catList as $cat)
    	{
    		$options[$cat->name] = $cat->idCategory;
    	}
    	echo elgg_view('input/checkboxes', array('name' => "league_categories", 'options' => $options, 'value' => $sticky['league_categories'])); 
    ?>
    <p><?php echo(elgg_echo('kpax_leagues:createform_desc_categories')) ?></p>
</div>

<div id="add_game_info">
	<label><?php echo(elgg_echo('kpax_leagues:createform_label_playlist')) ?></label><br/>
	<p><?php echo(elgg_echo('kpax_leagues:createform_desc_playlist')) ?></p>
</div>
<div id="add_game_sample" style="display:none;">
	<label><?php echo(elgg_echo('kpax_leagues:createform_playlist_addgame')) ?> %GAMENUM% (<a href="javascript:void(0)" onclick="deleteLeagueGame(%GAMENUM%)"><?php echo(elgg_echo('kpax_leagues:createform_playlist_delete')) ?></a>)</label><br/>
	<?php
	$options = array();
	foreach($gameList as $game){
		$options[$game->idGame] = $game->name;
	}
	echo elgg_view('input/dropdown', array('name' => 'league_addgame_%GAMENUM%', 'options_values' => $options));
	?>
</div>
<div id="add_game_inputs">
</div>
<div>
	<p><a href="javascript:void(0)" onclick="addLeagueGame()">(+) <?php echo(elgg_echo('kpax_leagues:createform_addgame')) ?></a></p>
</div>

<div>
<?php
echo elgg_view('input/submit', array('value' => elgg_echo('kpax_leagues:createform_button_add')));
?>
</div>