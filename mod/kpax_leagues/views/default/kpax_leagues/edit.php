<?php
$idLeague = get_input("idLeague");
$leagueEntity = get_entity($idLeague);
$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$leagueObj = new League($idLeague);
$leagueInfo = $leagueObj->getInfo($objKpax);

if($leagueEntity->title != '')
{
	if($leagueEntity->canEdit())
	{
		if($leagueInfo->status != 'finalized')
		{
			echo elgg_view_form('kpax_leagues/edit', array('name' => 'league_edit_form'));
		}
		else
		{
			echo('<p>'.elgg_echo('kpax_leagues:editform_league_finalized').'</p>');
		}
	}
	else
	{
		echo('<p>'.elgg_echo('kpax_leagues:editform_not_permission').'</p>');
	}
}
else
{
	echo('<p>'.elgg_echo('kpax_leagues:editform_league_not_exists').'</p>');
}
?>