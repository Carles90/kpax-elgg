<div class="league_create_button">
	<?php
	echo elgg_view('input/linkbutton', array(
		'value' => elgg_echo('kpax_leagues:button_create'),
		'href' => 'kpax_leagues/create'
	));
	?>
</div>