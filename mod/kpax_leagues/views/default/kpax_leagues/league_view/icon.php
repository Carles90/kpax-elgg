<?php
$size = $vars['size'];
$idLeague = $vars['idLeague'];
$leagueStatus = $vars['leagueStatus'];
$editLink = $vars['editLink'];

$data_root = kpaxGetDataRoot();
$file = $data_root.'leagues/'.$idLeague.$size.'.png';

?>

<div class="league_list_icon_<?php echo($size) ?>">
	<?php
	//Comprovar si existeix la icona de la lliga
	if(file_exists($file)){
		//Mostrar icona de la lliga
		echo('<img src="'.elgg_get_site_url().'mod/kpax_leagues/icondirect.php?idLeague='.$idLeague.'&amp;size='.$size.'" alt="League Avatar" />');
	}
	else
	{
		//Mostrar icona per defecte
		echo('<img src="'.elgg_get_site_url().'mod/kpax_leagues/graphics/default_'.$size.'.png" alt="League Avatar"/>');
	}

	if($leagueStatus == 'running' || $leagueStatus == 'waiting' || $leagueStatus == 'finalized')
	{
		?>
		<div class="league_list_icon_status">
			<img src="<?php echo(elgg_get_site_url()) ?>mod/kpax_leagues/graphics/<?php echo($leagueStatus) ?>.png" alt="League Status"/>
		</div>
		<?php
	}

	if($editLink != '')
	{
		?>
		<a href="<?php echo(elgg_get_site_url()) ?>kpax_leagues/view/<?php echo($idLeague) ?>/editicon">
			<div class="league_list_icon_status">
				<img src="<?php echo(elgg_get_site_url()) ?>mod/kpax_leagues/graphics/image_edit.png" alt="Edit"/>
			</div>
		</a>
		<?php
	}
	?>
</div>