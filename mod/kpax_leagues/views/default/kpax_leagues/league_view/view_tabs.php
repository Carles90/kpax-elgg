<?php
$objLeague = $vars['objLeague'];
$idLeague = $objLeague->idLeague;

$tabs = array();
$tabs['main'] = array(
		'text' => elgg_echo('kpax_leagues:leagueview_overview'),
		'href' => 'kpax_leagues/view/'.$idLeague.'',
		'priority' => 0
	);
$tabs['score'] = array(
		'text' => elgg_echo('kpax_leagues:leagueview_score'),
		'href' => 'kpax_leagues/view/'.$idLeague.'/score',
		'priority' => 1
	);
$tabs['news'] = array(
		'text' => elgg_echo('kpax_leagues:leagueview_news'),
		'href' => 'kpax_leagues/view/'.$idLeague.'/news',
		'priority' => 2
	);

if($objLeague->distribution == "teams")
{
	$tabs['teams'] = array(
		'text' => elgg_echo('kpax_leagues:leagueview_teams'),
		'href' => 'kpax_leagues/view/'.$idLeague.'/teams',
		'priority' => 3
	);
}


foreach ($tabs as $name => $tab) 
{
	$tab['name'] = $name;
	if ($vars['selected'] == $name) {
		$tab['selected'] = true;
		$selected = true;
	}
	
	elgg_register_menu_item('filter', $tab);
}

$menu = elgg_view_menu('filter', array('sort_by' => 'priority', 'class' => 'elgg-menu-hz'));
echo($menu);
?>