<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);

elgg_load_js('kpax_jcrop');
elgg_push_breadcrumb(elgg_echo('kpax_leagues:editicon_title'));

if($leagueEntity->title != '')
{
	if($leagueEntity->canEdit())
	{
		if($leagueInfo->status != 'finalized')
		{
			$target = kpaxGetDataRoot().'cache/league_'.$leagueInfo->idLeague.'.png';
			if(file_exists($target))
			{
				echo elgg_view_form('kpax_leagues/cropicon', array('name' => 'league_crop_icon'));
			}
			else
			{
				echo('<p>'.elgg_echo('kpax_leagues:editform_crop_noimage').'</p>');
			}
		}
		else
		{
			echo('<p>'.elgg_echo('kpax_leagues:editform_league_finalized').'</p>');
		}
	}
	else
	{
		echo('<p>'.elgg_echo('kpax_leagues:editform_not_permission').'</p>');
	}
}
else
{
	echo('<p>'.elgg_echo('kpax_leagues:editform_league_not_exists').'</p>');
}
?>