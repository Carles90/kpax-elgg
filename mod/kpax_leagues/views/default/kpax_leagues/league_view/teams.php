<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);
$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);
$myteam = $objLeague->getUserTeam($objKpax);

//Alertar a l'usuari si no es troba dins de cap equip
$isUserOnATeam = $objLeague->isUserOnATeam($objKpax);
$isUserAMember = $objLeague->isUserAMember($objKpax, $kpaxUser);
if(!$isUserOnATeam && $isUserAMember && $leagueInfo->distribution == "teams" && $leagueInfo->status != "finalized")
{
	echo elgg_view('kpax_leagues/league_alert', array('text' => elgg_echo('kpax_leagues:leagueview_teams_noteam_alert')));
}

?>
<div class="league_teams">
<?php
//Recuperar llista d'equips i de membres
$teams = $objLeague->getTeams($objKpax);
$members = $objLeague->getMembersInfo($objKpax);

if(count($teams) > 0)
{
	//Mostrar equips
	foreach($teams as $team)
	{
		?>
		<div class="league_teams_box">
			<h1><?php echo($team->teamName) ?></h1>
			<table>
				<?php
				$mnum = 0;
				foreach($members as $member)
				{
					if($member->idTeam == $team->idTeam)
					{
						if($member->idUser == $kpaxUser)
						{
							echo('<tr style="background-color: #b9e0ff;">');
						}
						elseif($member->idUser == $team->idOwner)
						{
							echo('<tr style="background-color: #f6f395;">');
						}
						else
						{
							echo('<tr>');
						}
						?>
							<td class="league_teams_box_avatar">
								<?php
								$elggMember = get_user_by_username($member->username);
								$avatar = $elggMember->getIconURL('tiny');
								echo(elgg_view('kpax_leagues/league_view/user_avatar', array('size' => 'tiny', 'url' => $avatar, 'border' => 1)));
								?>
							</td>
							<td>
								<div class="league_teams_box_username">
									<?php 
									echo($member->username);

									if((($leagueEntity->canEdit() || $team->idOwner == $kpaxUser) && $member->idUser != $kpaxUser) && $leagueInfo->status == 'waiting')
									{
										?>
										<a href="<?php echo(elgg_get_site_url()) ?>/kpax_leagues/teamkick/<?php echo($team->idTeam) ?>/<?php echo($member->idUser) ?>">
											<div class="league_teams_box_kick"></div>
										</a>
										<?php
									}
									?>
								</div>
							</td>
						</tr>
						<?php
						$mnum++;
					}
				}
				?>
			</table>
			<div class="league_teams_box_members">
				<?php
				if($leagueInfo->maxUsersGroup != 0)
				{
					echo($mnum.'/'.$leagueInfo->maxUsersGroup);
				}
				else
				{
					echo($mnum);
				}
				?>
			</div>

			<?php
			if($team->teamPassword != '')
			{
				?>
				<div class="league_teams_box_locked"></div>
				<?php
			}

			if($isUserAMember && !$isUserOnATeam && $leagueInfo->status == "waiting" && ($leagueInfo->maxUsersGroup == 0 || $leagueInfo->maxUsersGroup > $mnum))
			{
				if($team->teamPassword == "")
				{
					?>
					<a href="<?php echo(elgg_get_site_url()) ?>/kpax_leagues/teamjoin/<?php echo($team->idTeam) ?>">
					<?php
				}
				else
				{
					?>
					<a href="javascript:void(0)" onclick="joinTeamWithPassword(<?php echo($team->idTeam) ?>)">
					<?php
				}
				?>
					<div class="league_teams_box_join"><?php echo(elgg_echo('kpax_leagues:leagueview_teams_join')) ?></div>
				</a>
				<?php
			}
			elseif(($isUserAMember && $myteam == $team->idTeam) && $leagueInfo->status == "waiting")
			{
				?>
				<a href="<?php echo(elgg_get_site_url()) ?>/kpax_leagues/teamleave/<?php echo($team->idTeam) ?>">
					<div class="league_teams_box_leave"><?php echo(elgg_echo('kpax_leagues:leagueview_teams_leave')) ?></div>
				</a>
				<?php
			}


			if(($leagueEntity->canEdit() || $team->idOwner == $kpaxUser) && $leagueInfo->status == "waiting")
			{
				?>
				<a href="<?php echo(elgg_get_site_url()) ?>/kpax_leagues/teamremove/<?php echo($team->idTeam) ?>">
					<div class="league_teams_box_delete"><?php echo(elgg_echo('kpax_leagues:leagueview_teams_delete')) ?></div>
				</a>
				<?php
			}
			?>
		</div>
		<?php
	}

	//Usuaris sense equip
	?>
	<div class="league_teams_box">
		<h1><?php echo(elgg_echo('kpax_leagues:leagueview_teams_without_team')) ?></h1>
		<table>
			<?php
			$mnum = 0;
			foreach($members as $member)
			{
				if($member->idTeam == null)
				{
					if($member->idUser == $kpaxUser)
						{
							echo('<tr style="background-color: #b9e0ff;">');
						}
						else
						{
							echo('<tr>');
						}
						?>
						<td class="league_teams_box_avatar">
							<div class="league_teams_box_avatar">
								<?php
								$elggMember = get_user_by_username($member->username);
								$avatar = $elggMember->getIconURL('tiny');
								echo(elgg_view('kpax_leagues/league_view/user_avatar', array('size' => 'tiny', 'url' => $avatar, 'border' => 1)));
								?>
							</div>
						</td>
						<td class="league_teams_box_username">
							<div class="league_teams_box_username">
								<?php 
								echo($member->username);
								if($leagueEntity->canEdit() && $leagueInfo->status == 'waiting')
								{
									?>
									<div class="league_teams_box_assign">
										<select name="team_assign" onchange="assignUserToTeam(<?php echo($member->idUser) ?>, this)">
											<option disabled selected><?php echo(elgg_echo('kpax_leagues:leagueview_teams_assign')) ?></option>
											<?php
											foreach($teams as $team)
											{
												echo('<option value="'.$team->idTeam.'">'.$team->teamName.'</option>');
											}
											?>
										</select>
									</div>
									<?php
								}
								?>
							</div>
						</td>
					</tr>
					<?php
				}
			}
			?>
		</table>
	</div>
	<?php

}
else
{
	?>
	<p><?php echo elgg_echo('kpax_leagues:leagueview_teams_noteams'); ?></p>
	<?php
}

?>
</div>
<?php

if(($leagueEntity->canEdit() || $leagueInfo->allowTeamCreation && $objLeague->isUserAMember($objKpax, $kpaxUser)) && $leagueInfo->status == "waiting")
{
	echo elgg_view('input/linkbutton', array(
		'value' => elgg_echo('kpax_leagues:leagueview_teams_create_button'),
		'href' => 'kpax_leagues/view/'.$leagueInfo->idLeague.'/createteam'
	));
}
?>