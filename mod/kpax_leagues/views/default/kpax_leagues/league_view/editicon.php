<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);

elgg_push_breadcrumb(elgg_echo('kpax_leagues:editicon_title'));

if($leagueEntity->title != '')
{
	if($leagueEntity->canEdit())
	{
		if($leagueInfo->status != 'finalized')
		{
			echo elgg_view_form('kpax_leagues/editicon', array('name' => 'league_edit_icon', 'enctype' => 'multipart/form-data'));
		}
		else
		{
			echo('<p>'.elgg_echo('kpax_leagues:editform_league_finalized').'</p>');
		}
	}
	else
	{
		echo('<p>'.elgg_echo('kpax_leagues:editform_not_permission').'</p>');
	}
}
else
{
	echo('<p>'.elgg_echo('kpax_leagues:editform_league_not_exists').'</p>');
}
?>