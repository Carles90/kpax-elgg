<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);

if($leagueEntity->canEdit() || $leagueInfo->allowTeamCreation)
{
	set_input('lid', $leagueInfo->idLeague);
	echo elgg_view_form('kpax_leagues/create_team');
}
else
{
	echo('<p>'.elgg_echo('kpax_leagues:leagueview_createteam_not_allowed').'</p>');
}
?>