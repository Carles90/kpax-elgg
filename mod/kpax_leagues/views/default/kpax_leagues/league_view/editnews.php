<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);

if($leagueEntity->canEdit())
{
	echo elgg_view_form('kpax_leagues/editnews', array('name' => 'league_editnews_form'));
}
else
{
	echo elgg_echo('kpax_leagues:editform_not_permission');
}
?>