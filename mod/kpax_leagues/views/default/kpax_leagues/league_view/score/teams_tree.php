<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);
$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);

//Obtenir els membres de la competició
$members = $objLeague->getMembersInfo($objKpax);

//Obtenir els equips de la competició
$teams = $objLeague->getTeams($objKpax);
$teamnum = count($teams);

//Obtenir la llista de reproducció de la competició
$playlist = $objLeague->getPlaylist($objKpax);

//Obtenir les puntuacions dels equips
$matches = $objLeague->getScore($objKpax, 'teams', 'tree');

//Obtenir les puntuacions individuals dels usuaris
$scores = $objLeague->getScore($objKpax, 'single', 'scoretable');

//Construcció de totes les posicions de l'arbre (en blanc)
// Calcular profunditat màxima de l'arbre a partir del número de membres
$treeHeight = ceil(log($teamnum) / log(2));

// En cas de que el número d'equips no sigui potència de 2, calcular quants hi haurà a l'última fila
$lastFloorMax = pow(2, $treeHeight);
$lastFloor = 2 * $teamnum - $lastFloorMax;

// Construir
$tree = array();
for($i = 0; $i <= $treeHeight; $i++)
{
	$treeFloor = array();

	if($i < $treeHeight)
	{
		$floorLeafs = pow(2, $i);
		for($j = 0; $j < $floorLeafs; $j++)
		{
			$node = array();
			$node['idTeam'] = 0;
			array_push($treeFloor, $node);
		}
		
	}
	else
	{
		$floorLeafs = $lastFloor;
		for($j = 0; $j < $floorLeafs; $j++)
		{
			$node = array();
			$node['idTeam'] = 0;
			array_push($treeFloor, $node);
		}
	}

	$tree[$i] = $treeFloor;
}

//Posicionar jugadors a la posició correcta
$winner = null;
foreach($matches as $match)
{
	$tree[$match->treeHeight][$match->treePos * 2]['idTeam'] = $match->idTeam1;
	foreach($teams as $team)
	{
		if($team->idTeam == $match->idTeam1)
		{
			$tree[$match->treeHeight][$match->treePos * 2]['team'] = $team;
			if($match->status == 1 || $match->status == 3)
			{
				$tree[$match->treeHeight][$match->treePos * 2]['score'] = $match->scoreTeam1;
			}
			break;
		}
	}

	$tree[$match->treeHeight][$match->treePos * 2 + 1]['idTeam'] = $match->idTeam2;
	foreach($teams as $team)
	{
		if($team->idTeam == $match->idTeam2)
		{
			$tree[$match->treeHeight][$match->treePos * 2 + 1]['team'] = $team;
			if($match->status == 2 || $match->status == 3)
			{
				$tree[$match->treeHeight][$match->treePos * 2 + 1]['score'] = $match->scoreTeam2;
			}
			break;
		}
	}

	//Posició del guanyador
	if($match->treeHeight == 1)
	{
		$tree[0][0]['idTeam'] = $match->winner;
		foreach($teams as $team)
		{
			if($team->idTeam == $match->winner)
			{
				$tree[0][0]['team'] = $team;
				break;
			}
		}
	}
}

//Proporcionar avatars als equips
shuffle($members);
for($i = 0; $i < $teamnum; $i++)
{
	$teams[$i]->avatars = array();
	$teams[$i]->avatars[0] = null;
	$teams[$i]->avatars[1] = null;
	$teams[$i]->avatars[2] = null;
	$teams[$i]->avatars[3] = null;
	$avcount = 0;

	foreach($members as $m)
	{
		if($m->idTeam == $teams[$i]->idTeam)
		{
			$elggUser = get_user_by_username($m->username);
			$teams[$i]->avatars[$avcount] = $elggUser;

			$avcount++;
		}

		if($avcount == 4)
		{
			break;
		}
	}

	shuffle($teams[$i]->avatars);
}

//Visualització de l'arbre
?>
<div class="league_tree" id="league_tree">
<?php
$round = 0;
foreach($tree as $floor)
{
	$totalLeafs = pow(2, $round);

	$size = 'medium';
	$avatarSize = 'small';
	if($round == 0)
	{
		$size = 'big';
		$avatarSize = 'medium';
	}
	elseif($round >= 5)
	{
		$size = 'small';
		$avatarSize = 'tiny';
	}
	?>
	<div class="league_tree_floor league_tree_floor_<?php echo($size) ?>">
		<h1>
			<?php
			if($round == 0)
			{
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_winner'));
			}
			elseif($round == 1)
			{
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_final'));
			}
			elseif($round == 2)
			{
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_semifinal'));
			}
			elseif($round == 3)
			{
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_quarters'));
			}
			else
			{
				$roundnum = $treeHeight - $round + 1;
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_round').' '.$roundnum);
			}

			if($round != 0)
			{
				$idPlaylist = count($playlist) - $round;
				echo(': '.$playlist[$idPlaylist]->name);
			}
			?>
		</h1>
		<?php
		$i = 1;
		foreach($floor as $leaf)
		{
			//Càlcul de la posició del quadrat
			$position = (($i/$totalLeafs) * 100) - (50 / ($totalLeafs));
			?>
			<div class="league_tree_leaf_container" style="left: <?php echo($position) ?>%">
				<div class="league_tree_leaf league_tree_leaf_<?php echo($size) ?>">
					<?php
					if($floor != 0)
					{
						if($leaf['idTeam'] == 0)
						{
							?>
							<img src="<?php echo(elgg_get_site_url().'mod/kpax_leagues/graphics/unknown_'.$avatarSize.'.png') ?>" alt="?" />
							<?php
						}
						else
						{
							$elggAvatar1 = '';
							$elggAvatar2 = '';
							$elggAvatar3 = '';
							$elggAvatar4 = '';

							if($leaf['team']->avatars[0] != null)
							{
								$elggAvatar1 = $leaf['team']->avatars[0]->getIconURL($avatarSize);
							}

							if($leaf['team']->avatars[1] != null)
							{
								$elggAvatar2 = $leaf['team']->avatars[1]->getIconURL($avatarSize);
							}

							if($leaf['team']->avatars[2] != null)
							{
								$elggAvatar3 = $leaf['team']->avatars[2]->getIconURL($avatarSize);
							}

							if($leaf['team']->avatars[3] != null)
							{
								$elggAvatar4 = $leaf['team']->avatars[3]->getIconURL($avatarSize);
							}
							
							echo elgg_view('kpax_leagues/league_view/team_avatar', array('size' => $avatarSize, 
								'url1' => $elggAvatar1,
								'url2' => $elggAvatar2,
								'url3' => $elggAvatar3,
								'url4' => $elggAvatar4));
							?>
							<div class="league_tree_leaf_username">
								<?php 
								echo($leaf['team']->teamName);

								if(isset($leaf['score']))
								{
									echo(' ('.kpaxNumberFormat($leaf['score']).')');

									//TODO: Mostrar taula detallada
								}
								?>
							</div>
							<?php
						}
					}
					?>
				</div>
			</div>
			<?php
			//Dibuixar unions (linies)
			if($i % 2 == 1 && $round != 0)
			{
				$unionWidth = 100 / $totalLeafs;
				?>
				<div class="league_tree_leaf_union" style="width: <?php echo($unionWidth) ?>%; left: <?php echo($position) ?>%;"></div>
				<div class="league_tree_leaf_union2" style="width: <?php echo($unionWidth / 2) ?>%; left: <?php echo($position) ?>%;"></div>
				<?php
			}

			$i++;
		}
		?>
	</div>
	<?php
	$round++;
}

?>
</div>