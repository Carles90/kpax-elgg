<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);
$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);

//Obtenir els membres de la competició
$members = $objLeague->getMembersInfo($objKpax);

//Obtenir la llista de reproducció de la competició
$playlist = $objLeague->getPlaylist($objKpax);

//Obtenir les puntuacions dels membres
$scores = $objLeague->getScore($objKpax, 'single', 'knockout');

//Generar la taula de puntuacions a zero
$scoretable = array();

foreach($members as $member)
{
	$scorerow = array();
	$elggUser = get_user_by_username($member->username);
	$scorerow['elggAvatar'] = $elggUser->getIconURL('tiny');
	$scorerow['username'] = $member->username;
	$scorerow['idUser'] = $member->idUser;
	$scorerow['idLeagueUser'] = $member->idLeagueUser;
	$scorerow['knockout'] = false;

	array_push($scoretable, $scorerow);
}


//Buscar quins usuaris han estat desclassificats
foreach($scores as $s)
{
	for($i = 0; $i < count($scoretable); $i++)
	{
		if($scoretable[$i]['idLeagueUser'] == $s->idLeagueUser)
		{
			if($s->score != 0)
			{
				$scoretable[$i]['knockout'] = true;
			}
		}
	}
}

//Ordenar taula per nom d'usuari
kpaxArraySortByComparer($scoretable, function($a, $b){
	if(strtolower($a['username']) > strtolower($b['username']))
	{
		return 1;
	}
	elseif(strtolower($a['username']) < strtolower($b['username']))
	{
		return -1;
	}
	return 0;
});

//Mostrar taula de puntuació
?>
<table class="league_scoretable">
	<tr class="league_scoretable_header">
		<td style="width: 100px;"></td>
		<td style="width: 36px;">&nbsp;</td>
		<td style="text-align: left;"><?php echo(elgg_echo('kpax_leagues:leagueview_score_scoretable_user')) ?></td>
	</tr>
	<?php
	$zebra = 0;
	foreach($scoretable as $s)
	{
		echo('<tr class="league_scoretable_zebra_'.$zebra.' league_scoretable_zebra'.($s['knockout'] ? ' league_scoretable_knockout' : '').''.($s['idUser'] == $kpaxUser ? ' league_scoretable_zebra_me' : '').'">');
		echo('<td><div class="league_scoretable_knockout_flag">');
			echo('<div class="league_scoretable_knockout_'.($s['knockout'] ? 'yes' : 'no').'"></div>');
		echo('</div></td>');
		echo('<td>'.elgg_view('kpax_leagues/league_view/user_avatar', array('size' => 'tiny', 'url' => $s['elggAvatar'], 'border' => 1)).'</td>');
		echo('<td style="text-align: left;">');
			echo('<div class="league_scoretable_username">');
				echo($s['username']);
			echo('</div>');
		echo('</td>');
		echo('</tr>');

		if($zebra == 0)
		{
			$zebra = 1;
		}
		else
		{
			$zebra = 0;
		}
	}
	?>
</table>