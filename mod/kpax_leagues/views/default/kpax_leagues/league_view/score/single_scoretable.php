<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);
$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);

//Obtenir els membres de la competició
$members = $objLeague->getMembersInfo($objKpax);

//Obtenir la llista de reproducció de la competició
$playlist = $objLeague->getPlaylist($objKpax);

//Obtenir les puntuacions dels membres
$scores = $objLeague->getScore($objKpax, 'single', 'scoretable');

//Generar la taula de puntuacions a zero
$scoretable = array();

foreach($members as $member)
{
	$scorerow = array();
	$elggUser = get_user_by_username($member->username);
	$scorerow['elggAvatar'] = $elggUser->getIconURL('tiny');
	$scorerow['username'] = $member->username;
	$scorerow['idUser'] = $member->idUser;
	$scorerow['idLeagueUser'] = $member->idLeagueUser;
	$scorerow['totalscore'] = 0;
	$scorerow['score'] = array();

	array_push($scoretable, $scorerow);
}


//Emplenar la taula amb les puntuacions aconseguides
foreach($scores as $s)
{
	for($i = 0; $i < count($scoretable); $i++)
	{
		if($scoretable[$i]['idLeagueUser'] == $s->idLeagueUser)
		{
			$scoretable[$i]['totalscore'] += $s->score;
			array_push($scoretable[$i]['score'], $s->score);
		}
	}
}

//Ordenar taula per puntuació
kpaxArraySortByComparer($scoretable, function($a, $b){
	if($a['totalscore'] > $b['totalscore'])
	{
		return -1;
	}
	elseif($a['totalscore'] < $b['totalscore'])
	{
		return 1;
	}
	return 0;
});

//Mostrar taula de puntuació
?>
<table class="league_scoretable">
	<tr class="league_scoretable_header">
		<td style="width: 100px;"><?php echo(elgg_echo('kpax_leagues:leagueview_score_scoretable_pos')) ?></td>
		<td style="width: 36px;">&nbsp;</td>
		<td style="text-align: left;"><?php echo(elgg_echo('kpax_leagues:leagueview_score_scoretable_user')) ?></td>
		<td style="width: 100px; text-align: right; padding-right: 5px;"><?php echo(elgg_echo('kpax_leagues:leagueview_score_scoretable_score')) ?> </td>
	</tr>
	<?php
	$pos = 1;
	$zebra = 0;
	foreach($scoretable as $s)
	{
		echo('<tr class="league_scoretable_zebra_'.$zebra.' league_scoretable_zebra'.($s['idUser'] == $kpaxUser ? ' league_scoretable_zebra_me' : '').'">');
		echo('<td>'.kpaxNumberFormat($pos).'</td>');
		echo('<td>'.elgg_view('kpax_leagues/league_view/user_avatar', array('size' => 'tiny', 'url' => $s['elggAvatar'], 'border' => 1)).'</td>');
		echo('<td style="text-align: left;">');
			echo('<div class="league_scoretable_username">');
				echo($s['username']);
				echo('<div class="league_scoretable_details"><table>');
				$j = 0;
				foreach($s['score'] as $singlescore)
				{
					echo('<tr>');
						echo('<td>'.$playlist[$j]->name.'</td>');
						echo('<td style="text-align: right;">'.kpaxNumberFormat($singlescore).'</td>');
					echo('</tr>');
					$j++;
				}
				echo('</table></div>');
			echo('</div>');
		echo('</td>');
		echo('<td style="text-align: right; padding-right: 5px;">'.kpaxNumberFormat($s['totalscore']).'</td>');
		echo('</tr>');
		$pos++;

		if($zebra == 0)
		{
			$zebra = 1;
		}
		else
		{
			$zebra = 0;
		}
	}
	?>
</table>