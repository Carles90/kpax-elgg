<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);
$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);

//Obtenir els membres de la competició
$members = $objLeague->getMembersInfo($objKpax);
$membernum = count($members);

//Obtenir la llista de reproducció de la competició
$playlist = $objLeague->getPlaylist($objKpax);

//Obtenir les puntuacions dels membres
$matches = $objLeague->getScore($objKpax, 'single', 'tree');

//Construcció de totes les posicions de l'arbre (en blanc)
// Calcular profunditat màxima de l'arbre a partir del número de membres
$treeHeight = ceil(log($membernum) / log(2));

// En cas de que el número d'usuaris no sigui potència de 2, calcular quants hi haurà a l'última fila
$lastFloorMax = pow(2, $treeHeight);
$lastFloor = 2 * $membernum - $lastFloorMax;

// Construir
$tree = array();
for($i = 0; $i <= $treeHeight; $i++)
{
	$treeFloor = array();

	if($i < $treeHeight)
	{
		$floorLeafs = pow(2, $i);
		for($j = 0; $j < $floorLeafs; $j++)
		{
			$node = array();
			$node['idLeagueUser'] = 0;
			array_push($treeFloor, $node);
		}
		
	}
	else
	{
		$floorLeafs = $lastFloor;
		for($j = 0; $j < $floorLeafs; $j++)
		{
			$node = array();
			$node['idLeagueUser'] = 0;
			array_push($treeFloor, $node);
		}
	}

	$tree[$i] = $treeFloor;
}

//Posicionar jugadors a la posició correcta
$winner = null;
foreach($matches as $match)
{
	$tree[$match->treeHeight][$match->treePos * 2]['idLeagueUser'] = $match->idUser1;
	foreach($members as $member)
	{
		if($member->idLeagueUser == $match->idUser1)
		{
			$tree[$match->treeHeight][$match->treePos * 2]['member'] = $member;
			if($match->status == 1 || $match->status == 3)
			{
				$tree[$match->treeHeight][$match->treePos * 2]['score'] = $match->scoreUser1;
			}
			break;
		}
	}

	$tree[$match->treeHeight][$match->treePos * 2 + 1]['idLeagueUser'] = $match->idUser2;
	foreach($members as $member)
	{
		if($member->idLeagueUser == $match->idUser2)
		{
			$tree[$match->treeHeight][$match->treePos * 2 + 1]['member'] = $member;
			if($match->status == 2 || $match->status == 3)
			{
				$tree[$match->treeHeight][$match->treePos * 2 + 1]['score'] = $match->scoreUser2;
			}
			break;
		}
	}

	//Posició del guanyador
	if($match->treeHeight == 1)
	{
		$tree[0][0]['idLeagueUser'] = $match->winner;
		foreach($members as $member)
		{
			if($member->idLeagueUser == $match->winner)
			{
				$tree[0][0]['member'] = $member;
				break;
			}
		}
	}
}

//Visualització de l'arbre
?>
<div class="league_tree" id="league_tree">
<?php
$round = 0;
foreach($tree as $floor)
{
	$totalLeafs = pow(2, $round);

	$size = 'medium';
	$avatarSize = 'small';
	if($round == 0)
	{
		$size = 'big';
		$avatarSize = 'medium';
	}
	elseif($round >= 5)
	{
		$size = 'small';
		$avatarSize = 'tiny';
	}
	?>
	<div class="league_tree_floor league_tree_floor_<?php echo($size) ?>">
		<h1>
			<?php
			if($round == 0)
			{
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_winner'));
			}
			elseif($round == 1)
			{
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_final'));
			}
			elseif($round == 2)
			{
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_semifinal'));
			}
			elseif($round == 3)
			{
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_quarters'));
			}
			else
			{
				$roundnum = $treeHeight - $round + 1;
				echo(elgg_echo('kpax_leagues:leagueview_score_tree_round').' '.$roundnum);
			}

			if($round != 0)
			{
				echo(': '.$playlist[count($playlist) - $round]->name);
			}
			?>
		</h1>
		<?php
		$i = 1;
		foreach($floor as $leaf)
		{
			//Càlcul de la posició del quadrat
			$position = (($i/$totalLeafs) * 100) - (50 / ($totalLeafs));
			?>
			<div class="league_tree_leaf_container" style="left: <?php echo($position) ?>%">
				<div class="league_tree_leaf league_tree_leaf_<?php echo($size) ?>">
					<?php
					if($floor != 0)
					{
						if($leaf['idLeagueUser'] == 0)
						{
							?>
							<img src="<?php echo(elgg_get_site_url().'mod/kpax_leagues/graphics/unknown_'.$avatarSize.'.png') ?>" alt="?" />
							<?php
						}
						else
						{
							$elggUser = get_user_by_username($leaf['member']->username);
							$elggAvatar = $elggUser->getIconURL($avatarSize);
							echo elgg_view('kpax_leagues/league_view/user_avatar', array('size' => $avatarSize, 'url' => $elggAvatar));
							?>
							<div class="league_tree_leaf_username">
								<?php 
								echo($leaf['member']->username);

								if(isset($leaf['score']))
								{
									echo(' ('.kpaxNumberFormat($leaf['score']).')');
								}
								?>
							</div>
							<?php
						}
					}
					?>
				</div>
			</div>
			<?php
			//Dibuixar unions (linies)
			if($i % 2 == 1 && $round != 0)
			{
				$unionWidth = 100 / $totalLeafs;
				?>
				<div class="league_tree_leaf_union" style="width: <?php echo($unionWidth) ?>%; left: <?php echo($position) ?>%;"></div>
				<div class="league_tree_leaf_union2" style="width: <?php echo($unionWidth / 2) ?>%; left: <?php echo($position) ?>%;"></div>
				<?php
			}

			$i++;
		}
		?>
	</div>
	<?php
	$round++;
}
?>
</div>