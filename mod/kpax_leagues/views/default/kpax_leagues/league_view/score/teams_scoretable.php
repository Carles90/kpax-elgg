<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);
$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);

//Obtenir els membres de la competició
$members = $objLeague->getMembersInfo($objKpax);

//Obtenir els equips de la competició
$teams = $objLeague->getTeams($objKpax);

//Obtenir la llista de reproducció de la competició
$playlist = $objLeague->getPlaylist($objKpax);

//Obtenir les puntuacions dels membres
$scores = $objLeague->getScore($objKpax, 'single', 'scoretable');

//Obtenir les puntuacions dels equips
$teamsScores = $objLeague->getScore($objKpax, 'teams', 'scoretable');

//Obtenir equip de l'usuari
$idTeam = -1;
foreach($members as $m)
{
	if($m->idUser == $kpaxUser)
	{
		$idTeam = $m->idTeam;
	}
}

//Generar la taula de puntuacions a zero
$scoretable = array();

foreach($teams as $team)
{
	$scorerow = array();
	$scorerow['teamName'] = $team->teamName;
	$scorerow['idTeam'] = $team->idTeam;
	$scorerow['totalscore'] = 0;
	$scorerow['score'] = array();
	$scorerow['avatars'] = array();
	$scorerow['avatars'][0] = '';
	$scorerow['avatars'][1] = '';
	$scorerow['avatars'][2] = '';
	$scorerow['avatars'][3] = '';

	array_push($scoretable, $scorerow);
}

//Generar informació d'usuaris
$userstable = array();
foreach($members as $m)
{
	$row = array();
	$row['member'] = $m;
	$row['elggUser'] = get_user_by_username($m->username);
	$row['elggAvatar'] = $row['elggUser']->getIconURL('small');
	array_push($userstable, $row);
}

//Assignar avatars a cada equip
shuffle($userstable);
for($i = 0; $i < count($scoretable); $i++)
{
	$tid = $scoretable[$i]['idTeam'];
	$avcount = 0;
	foreach($userstable as $u)
	{
		if($u['member']->idTeam == $tid)
		{
			$scoretable[$i]['avatars'][$avcount] = $u['elggAvatar'];
			$avcount++;

			if($avcount == 4)
			{
				break;
			}
		}
	}

	shuffle($scoretable[$i]['avatars']);
}

//Emplenar la taula amb les puntuacions aconseguides
foreach($teamsScores as $s)
{
	for($i = 0; $i < count($scoretable); $i++)
	{
		if($scoretable[$i]['idTeam'] == $s->idTeam)
		{
			$scoretable[$i]['totalscore'] += $s->avgscore;
			array_push($scoretable[$i]['score'], $s->avgscore);
		}
	}
}

//Ordenar taula per puntuació
kpaxArraySortByComparer($scoretable, function($a, $b){
	if($a['totalscore'] > $b['totalscore'])
	{
		return -1;
	}
	elseif($a['totalscore'] < $b['totalscore'])
	{
		return 1;
	}
	return 0;
});

//Generar les diferents taules de puntuació individual per a cada equip
$teamScoreTables = array();
foreach($teams as $team)
{
	$teamScoreTable = array();
	foreach($members as $member)
	{
		$teamScoreRow = array();
		$teamScoreTable[$member->idLeagueUser] = $teamScoreRow;
	}
	$teamScoreTables[$team->idTeam] = $teamScoreTable;
}

//Emplenar taules individuals de puntuació
foreach($scores as $s)
{
	array_push($teamScoreTables[$s->idTeam][$s->idLeagueUser], $s->score);
}
//Mostrar taula de puntuació
?>
<table class="league_scoretable">
	<tr class="league_scoretable_header">
		<td style="width: 100px;"><?php echo(elgg_echo('kpax_leagues:leagueview_score_scoretable_pos')) ?></td>
		<td style="width: 56px;">&nbsp;</td>
		<td style="text-align: left;"><?php echo(elgg_echo('kpax_leagues:leagueview_score_scoretable_team')) ?></td>
		<td style="width: 100px; text-align: right; padding-right: 5px;"><?php echo(elgg_echo('kpax_leagues:leagueview_score_scoretable_avgscore')) ?> </td>
	</tr>
	<?php
	$pos = 1;
	$zebra = 0;
	foreach($scoretable as $s)
	{
		echo('<tr style="height: 50px;" class="league_scoretable_zebra_'.$zebra.' league_scoretable_zebra'.($s['idTeam'] == $idTeam ? ' league_scoretable_zebra_me' : '').'">');
		echo('<td>'.kpaxNumberFormat($pos).'</td>');
		echo('<td>'.elgg_view('kpax_leagues/league_view/team_avatar', array('size' => 'small', 
			'url1' => $s['avatars'][0], 
			'url2' => $s['avatars'][1], 
			'url3' => $s['avatars'][2], 
			'url4' => $s['avatars'][3], 
			'border' => 1)).'</td>');
		?>
		<td style="text-align: left;">
			<div class="league_scoretable_username">
				<?php echo($s['teamName']); ?>
				<div class="league_scoretable_teams_details">
					<table>
						<tr>
							<td></td>
							<?php
							foreach($members as $m)
							{
								if($m->idTeam == $s['idTeam'])
								{
									echo('<td>'.$m->username.'</td>');
								}
							}
							?>
						</tr>
						<?php
						$plgame = 0;
						foreach($playlist as $p)
						{
							?>
							<tr>
								<td><?php echo($p->name); ?></td>
								<?php
								foreach($members as $m)
								{
									if($m->idTeam == $s['idTeam'])
									{
										echo('<td>');
										if(isset($teamScoreTables[$m->idTeam][$m->idLeagueUser][$plgame]))
										{
											echo(kpaxNumberFormat($teamScoreTables[$m->idTeam][$m->idLeagueUser][$plgame]));
										}
										else
										{
											echo('-');
										}
										
										echo('</td>');
									}
								}
								?>
							</tr>
							<?php
							$plgame++;
						}
						?>
					</table>
				</div>
			</div>
		</td>
		<td style="text-align: right; padding-right: 5px;"><?php echo(kpaxNumberFormat($s['totalscore'])); ?></td>
		<?php
		echo('</tr>');
		$pos++;

		if($zebra == 0)
		{
			$zebra = 1;
		}
		else
		{
			$zebra = 0;
		}
	}
	?>
</table>