<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);
$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);

//Obtenir els membres de la competició
$members = $objLeague->getMembersInfo($objKpax);

//Obtenir els equips de la competició
$teams = $objLeague->getTeams($objKpax);

//Obtenir la llista de reproducció de la competició
$playlist = $objLeague->getPlaylist($objKpax);

//Obtenir les puntuacions dels membres
$scores = $objLeague->getScore($objKpax, 'single', 'knockout');

//Obtenir les puntuacions dels equips
$teamsScores = $objLeague->getScore($objKpax, 'teams', 'knockout');

//Obtenir equip de l'usuari
$idTeam = -1;
foreach($members as $m)
{
	if($m->idUser == $kpaxUser)
	{
		$idTeam = $m->idTeam;
	}
}

//Generar la taula de puntuacions a zero
$scoretable = array();

foreach($teams as $team)
{
	$scorerow = array();
	$scorerow['teamName'] = $team->teamName;
	$scorerow['idTeam'] = $team->idTeam;
	$scorerow['totalscore'] = 0;
	$scorerow['score'] = array();
	$scorerow['avatars'] = array();
	$scorerow['avatars'][0] = '';
	$scorerow['avatars'][1] = '';
	$scorerow['avatars'][2] = '';
	$scorerow['avatars'][3] = '';
	$scorerow['knockout'] = false;
	$scorerow['members'] = $team->members;

	array_push($scoretable, $scorerow);
}

//Generar informació d'usuaris
$userstable = array();
foreach($members as $m)
{
	$row = array();
	$row['member'] = $m;
	$row['elggUser'] = get_user_by_username($m->username);
	$row['elggAvatar'] = $row['elggUser']->getIconURL('small');
	array_push($userstable, $row);
}

//Assignar avatars a cada equip
shuffle($userstable);
for($i = 0; $i < count($scoretable); $i++)
{
	$tid = $scoretable[$i]['idTeam'];
	$avcount = 0;
	foreach($userstable as $u)
	{
		if($u['member']->idTeam == $tid)
		{
			$scoretable[$i]['avatars'][$avcount] = $u['elggAvatar'];
			$avcount++;

			if($avcount == 4)
			{
				break;
			}
		}
	}

	shuffle($scoretable[$i]['avatars']);
}

//Buscar quins equips han estat desclassificats
foreach($teamsScores as $s)
{
	for($i = 0; $i < count($scoretable); $i++)
	{
		if($scoretable[$i]['idTeam'] == $s->idTeam)
		{
			if($s->avgscore != 0 && $s->submits == $scoretable[$i]['members'])
			{
				$scoretable[$i]['knockout'] = true;
			}
		}
	}
}

//Ordenar taula per nom d'equip
kpaxArraySortByComparer($scoretable, function($a, $b){
	if(strtolower($a['teamName']) > strtolower($b['teamName']))
	{
		return 1;
	}
	elseif(strtolower($a['teamName']) < strtolower($b['teamName']))
	{
		return -1;
	}
	return 0;
});

//Mostrar taula de puntuació
?>
<table class="league_scoretable">
	<tr class="league_scoretable_header">
		<td style="width: 100px;"></td>
		<td style="width: 56px;">&nbsp;</td>
		<td style="text-align: left;"><?php echo(elgg_echo('kpax_leagues:leagueview_score_scoretable_team')) ?></td>
	</tr>
	<?php
	$zebra = 0;
	foreach($scoretable as $s)
	{
		echo('<tr style="height: 50px;" class="league_scoretable_zebra_'.$zebra.' league_scoretable_zebra'.($s['knockout'] ? ' league_scoretable_knockout' : '').''.($s['idTeam'] == $idTeam ? ' league_scoretable_zebra_me' : '').'">');
		echo('<td><div class="league_scoretable_knockout_flag">');
			echo('<div class="league_scoretable_knockout_'.($s['knockout'] ? 'yes' : 'no').'"></div>');
		echo('</div></td>');
		echo('<td>'.elgg_view('kpax_leagues/league_view/team_avatar', array('size' => 'small', 
			'url1' => $s['avatars'][0], 
			'url2' => $s['avatars'][1], 
			'url3' => $s['avatars'][2], 
			'url4' => $s['avatars'][3], 
			'border' => 1)).'</td>');
		echo('<td style="text-align: left;">');
			echo('<div class="league_scoretable_username">');
				echo($s['teamName']);
			echo('</div>');
		echo('</td>');
		echo('</tr>');

		if($zebra == 0)
		{
			$zebra = 1;
		}
		else
		{
			$zebra = 0;
		}
	}
	?>
</table>