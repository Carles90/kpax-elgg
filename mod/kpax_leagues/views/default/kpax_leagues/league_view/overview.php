<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);
$kpaxUser = (int)$objKpax->getUserBySession($_SESSION['campusSession']);

$playernum = $objLeague->getPlayerNum($objKpax);

//Obtenir la llista de reproducció de la competició
$playlist = $objLeague->getPlaylist($objKpax);
?>

<?php
if($leagueInfo->status == 'waiting' && $leagueInfo->start < (time() * 1000) && $leagueEntity->canEdit())
{
	echo elgg_view('kpax_leagues/league_alert', array('text' => elgg_echo('kpax_leagues:leagueview_alert_waiting')));
}

if($leagueInfo->distribution == "teams" && $leagueInfo->status != "finalized" && !$objLeague->isUserOnATeam($objKpax) && $objLeague->isUserAMember($objKpax, $kpaxUser))
{
	echo elgg_view('kpax_leagues/league_alert', array('text' => elgg_echo('kpax_leagues:leagueview_teams_noteam_alert')));
}
?>

<div class="league_view">
	<div class="league_view_sidebar">
		<div class="league_view_sidebar_icon">
			<?php
			echo elgg_view('kpax_leagues/league_view/icon', array(
					'idLeague' => $leagueInfo->idLeague,
					'size' => 'large',
					'editLink' => ($leagueEntity->canEdit() && $leagueInfo->status != 'finalized') ? '1' : ''
				));
			?>
		</div>
		<div class="league_view_sidebar_item league_view_sidebar_item_members">
			<?php
			if($leagueInfo->maxUsers == 0)
			{
				echo(''.kpaxNumberFormat($playernum).'');
			}
			else
			{
				echo(''.kpaxNumberFormat($playernum).' '.elgg_echo('kpax_leagues:leaguebox_of').' '.kpaxNumberFormat($leagueInfo->maxUsers).'');
			}
			?>
		</div>
		<?php
		if($leagueInfo->status == 'waiting')
		{
			$statusText = elgg_echo('kpax_leagues:leagueview_status_waiting');
			$status = 'waiting';
		}
		elseif($leagueInfo->status == 'finalized')
		{
			$statusText = elgg_echo('kpax_leagues:leagueview_status_finalized');
			$status = 'finalized';
		}
		else
		{
			if($leagueInfo->start > time() * 1000)
			{
				$statusText = elgg_echo('kpax_leagues:leagueview_status_waiting_to_start');
				$status = 'waiting';
			}
			elseif($leagueInfo->end < time() * 1000)
			{
				$statusText = elgg_echo('kpax_leagues:leagueview_status_finalized');
				$status = 'finalized';
			}
			else
			{
				$statusText = elgg_echo('kpax_leagues:leagueview_status_running');
				$status = 'running';
			}
		}
		?>
		<div class="league_view_sidebar_item league_view_sidebar_item_status_<?php echo($status) ?>"><?php echo($statusText) ?></div>
		<?php
		if($leagueInfo->status == 'waiting')
		{
			if(!$objLeague->isUserAMember($objKpax, $kpaxUser))
			{
			?>
			<a href="<?php echo(elgg_get_site_url()) ?>kpax_leagues/join/<?php echo($leagueInfo->idLeague) ?>"><div class="league_view_sidebar_item league_view_sidebar_item_join"><?php echo(elgg_echo('kpax_leagues:leagueview_button_join')); ?></div></a>
			<?php
			}
			else
			{
			?>
			<a href="<?php echo(elgg_get_site_url()) ?>kpax_leagues/leave/<?php echo($leagueInfo->idLeague) ?>"><div class="league_view_sidebar_item league_view_sidebar_item_leave"><?php echo(elgg_echo('kpax_leagues:leagueview_button_leave')); ?></div></a>
			<?php
			}
		}

		if($leagueInfo->status == 'waiting' && $leagueEntity->canEdit())
		{
			?>
			<a href="<?php echo(elgg_get_site_url()) ?>kpax_leagues/lock/<?php echo($leagueInfo->idLeague) ?>"><div class="league_view_sidebar_item league_view_sidebar_item_lock"><?php echo(elgg_echo('kpax_leagues:leagueview_button_'.$leagueInfo->distribution.'_lock')); ?></div></a>
			<?php
		}
		elseif($leagueInfo->status == 'running' && $leagueInfo->start > (time() * 1000) && $leagueEntity->canEdit())
		{
			?>
			<a href="<?php echo(elgg_get_site_url()) ?>kpax_leagues/unlock/<?php echo($leagueInfo->idLeague) ?>"><div class="league_view_sidebar_item league_view_sidebar_item_unlock"><?php echo(elgg_echo('kpax_leagues:leagueview_button_'.$leagueInfo->distribution.'_unlock')); ?></div></a>
			<?php
		}

		if($leagueEntity->canEdit() && $leagueInfo->status != 'finalized')
		{
		?>
		<a href="<?php echo(elgg_get_site_url()) ?>kpax_leagues/edit/<?php echo($leagueInfo->idLeague) ?>"><div class="league_view_sidebar_item league_view_sidebar_item_edit"><?php echo(elgg_echo('kpax_leagues:leagueview_button_edit')); ?></div></a>
		<a href="<?php echo(elgg_get_site_url()) ?>kpax_leagues/delete/<?php echo($leagueInfo->idLeague) ?>"><div class="league_view_sidebar_item league_view_sidebar_item_delete"><?php echo(elgg_echo('kpax_leagues:leagueview_button_delete')); ?></div></a>
		<?php
		}
		?>
	</div>
	<div class="league_view_info">
		<?php
		echo $leagueInfo->description;
		?>
		<p>
			<b><?php echo elgg_echo('kpax_leagues:leaguebox_abilities') ?>: </b>
			<?php
				$categories = $objLeague->getCategories($objKpax);
				$playlist = $objLeague->getPlaylist($objKpax);

				$cats = array();
				foreach($categories as $c)
				{
					array_push($cats, $c->name);
				}

				echo(count($cats) == 0 ? elgg_echo('kpax_leagues:leaguebox_abilities_none') : implode(', ', $cats));
			?>
		</p>
		<h2><?php echo elgg_echo('kpax_leagues:leagueview_playlist') ?></h2>
		<div class="league_view_playlist">
			<?php
			$count = 0;
			foreach($playlist as $p)
			{
				if($count != 0)
				{
					echo('<div class="league_view_playlist_arrow"></div>');
				}
				echo('<div class="league_view_playlist_item">'.$p->name.'</div>');

				$count++;
			}
			?>
		</div>
	</div>
</div>