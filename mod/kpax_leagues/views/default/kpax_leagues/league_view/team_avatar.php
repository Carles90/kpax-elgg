<?php
$size = $vars['size'];
$url1 = $vars['url1'];
$url2 = $vars['url2'];
$url3 = $vars['url3'];
$url4 = $vars['url4'];

$border = false;
if(isset($vars['border']))
{
	$border = $vars['border'];
}

if($border)
{
	?>
	<div class="league_list_icon_<?php echo($size) ?>">
	<?php
}
else
{
	?>
	<div>
	<?php
}
?>
	<div class="league_list_icon_team">
		<div class="league_list_icon_team_<?php echo($size) ?>">
			<?php
			if($url1 != '')
			{
				echo elgg_view('kpax_leagues/league_view/user_avatar', array('size' => $size, 'url' => $url1));
			}
			?>
		</div>
		<div class="league_list_icon_team_<?php echo($size) ?>">
			<?php
			if($url2 != '')
			{
				echo elgg_view('kpax_leagues/league_view/user_avatar', array('size' => $size, 'url' => $url2));
			}
			?>
		</div>
		<div class="league_list_icon_team_<?php echo($size) ?>">
			<?php
			if($url3 != '')
			{
				echo elgg_view('kpax_leagues/league_view/user_avatar', array('size' => $size, 'url' => $url3));
			}
			?>
		</div>
		<div class="league_list_icon_team_<?php echo($size) ?>">
			<?php
			if($url4 != '')
			{
				echo elgg_view('kpax_leagues/league_view/user_avatar', array('size' => $size, 'url' => $url4));
			}
			?>
		</div>
		<div style="clear: both;"></div>
	</div>
</div>