<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueEntity = $vars['leagueEntity'];
$leagueInfo = $objLeague->getInfo($objKpax);

if($leagueInfo->news == '')
{
	echo elgg_echo('kpax_leagues:leagueview_news_nonews');
}
else
{
	echo($leagueInfo->news);
}
?>