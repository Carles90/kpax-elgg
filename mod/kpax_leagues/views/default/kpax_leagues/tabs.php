<?php

$tabs = array();
$tabs['all'] = array(
		'text' => elgg_echo('kpax_leagues:tab_all_leagues'),
		'href' => 'kpax_leagues/overview/',
		'priority' => 0
	);
$tabs['waiting'] = array(
		'text' => elgg_echo('kpax_leagues:tab_waiting_leagues'),
		'href' => 'kpax_leagues/overview/waiting',
		'priority' => 1
	);
$tabs['running'] = array(
		'text' => elgg_echo('kpax_leagues:tab_running_leagues'),
		'href' => 'kpax_leagues/overview/running',
		'priority' => 2
	);
$tabs['finalized'] = array(
		'text' => elgg_echo('kpax_leagues:tab_finalized_leagues'),
		'href' => 'kpax_leagues/overview/finalized',
		'priority' => 3
	);


foreach ($tabs as $name => $tab) 
{
	$tab['name'] = $name;
	if ($vars['selected'] == $name) {
		$tab['selected'] = true;
		$selected = true;
	}
	
	elgg_register_menu_item('filter', $tab);
}

$menu = elgg_view_menu('filter', array('sort_by' => 'priority', 'class' => 'elgg-menu-hz'));
echo($menu);
?>