<?php
$league = $vars['league'];
$objKpax = $vars['objKpax'];

$leagueInfo = $league->getInfo($objKpax);
$playernum = $league->getPlayerNum($objKpax);
?>
<div class="league_box">
	<div class="league_box_icon">
		<?php
		echo elgg_view('kpax_leagues/league_view/icon', array(
				'idLeague' => $leagueInfo->idLeague,
				'size' => 'medium',
				'leagueStatus' => $leagueInfo->status
			));
		?>
		<div class="league_box_icon_info">
			<div class="league_box_icon_info_members league_box_icon_info_item">
				<?php
				if($leagueInfo->maxUsers == 0)
				{
					echo(''.kpaxNumberFormat($playernum).'');
				}
				else
				{
					echo(''.kpaxNumberFormat($playernum).' '.elgg_echo('kpax_leagues:leaguebox_of').' '.kpaxNumberFormat($leagueInfo->maxUsers).'');
				}
				?>
			</div>
			<div class="league_box_icon_info_view league_box_icon_info_item">
				<a href="<?php echo(elgg_get_site_url()) ?>kpax_leagues/view/<?php echo($leagueInfo->idLeague) ?>"><?php echo(elgg_echo('kpax_leagues:leaguebox_view')) ?></a>
			</div>
		</div>
	</div>
	<div class="league_box_info">
		<h1><?php echo($leagueInfo->title) ?></h1>
		<?php
		$categories = $league->getCategories($objKpax);
		$playlist = $league->getPlaylist($objKpax);

		$cats = array();
		foreach($categories as $c)
		{
			array_push($cats, $c->name);
		}

		$started = elgg_echo('kpax_leagues:leaguebox_started');
		if(kpaxNow() < $leagueInfo->start)
		{
			$started = elgg_echo('kpax_leagues:leaguebox_willstart');
		}

		$ended = elgg_echo('kpax_leagues:leaguebox_ended');
		if(kpaxNow() < $leagueInfo->end)
		{
			$ended = elgg_echo('kpax_leagues:leaguebox_willend');
		}

		echo('<p><b>'.$started.':</b> '.kpaxUnixToDateTime($leagueInfo->start).'</p>');
		echo('<p><b>'.$ended.':</b> '.kpaxUnixToDateTime($leagueInfo->end).'</p>');
		?>
		<p><b><?php echo(elgg_echo('kpax_leagues:leaguebox_scoretype')) ?>:</b> <?php echo(elgg_echo('kpax_leagues:leaguebox_scoretype_'.$leagueInfo->scoreType)) ?> (<?php echo(elgg_echo('kpax_leagues:leaguebox_distribution_'.$leagueInfo->distribution)) ?>)</p>
		<p><b><?php echo(elgg_echo('kpax_leagues:leaguebox_abilities')) ?>:</b> <?php echo(count($cats) == 0 ? elgg_echo('kpax_leagues:leaguebox_abilities_none') : implode(', ', $cats)) ?></p>
		<p>
			<b><?php echo(elgg_echo('kpax_leagues:leaguebox_games')) ?>: </b>
			<?php
			$totalgames = 0;
			foreach($playlist as $g)
			{
				if($totalgames != 0)
				{
					echo(', ');
				}

				echo('<a href="'.elgg_get_site_url().'kpax/view/'.$g->idGame.'">'.$g->name.'</a>');

				$totalgames++;
			}
			if($totalgames == 0)
			{
				echo(elgg_echo('kpax_leagues:leaguebox_games_none'));
			}
			?>
		</p>
		<p>&nbsp;</p>
		<p><b><?php echo(elgg_echo('kpax_leagues:leaguebox_description')) ?>:</b> <?php echo(kpaxCutText($leagueInfo->description, 200)) ?></p>
	</div>
</div>