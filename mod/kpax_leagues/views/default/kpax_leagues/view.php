<?php
$objKpax = $vars['objKpax'];
$objLeague = $vars['objLeague'];
$leagueInfo = $objLeague->getInfo($objKpax);
$leagueEntity = $vars['leagueEntity'];
$tab = get_input('tab');

echo elgg_view_title($objLeague->getInfo($objKpax)->title);

if($tab == 'news' && $leagueEntity->canEdit())
{
	echo elgg_view('kpax_leagues/league_view/buttons/edit_news_button', array('idLeague' => $leagueInfo->idLeague));
}
elseif($tab == '' && $leagueEntity->canEdit())
{
	echo elgg_view('kpax_leagues/league_view/buttons/edit_league', array('idLeague' => $leagueInfo->idLeague));
}

echo elgg_view('kpax_leagues/league_view/view_tabs', array(
		'objLeague' => $objLeague->getInfo($objKpax)
	));

switch($tab)
{
	case "score":
		if($leagueInfo->status != 'waiting')
		{
			echo elgg_view('kpax_leagues/league_view/score/'.$leagueInfo->distribution.'_'.$leagueInfo->scoreType.'', array(
					'objKpax' => $objKpax,
					'objLeague' => $objLeague,
					'leagueEntity' => $leagueEntity
				));
		}
		else
		{
			echo elgg_echo('kpax_leagues:leagueview_score_unavailable');
		}
	break;
	case "news":
		echo elgg_view('kpax_leagues/league_view/news', array(
				'objKpax' => $objKpax,
				'objLeague' => $objLeague,
				'leagueEntity' => $leagueEntity
			));
	break;
	case "editnews":
		echo elgg_view('kpax_leagues/league_view/editnews', array(
				'objKpax' => $objKpax,
				'objLeague' => $objLeague,
				'leagueEntity' => $leagueEntity
			));
	break;
	case "teams":
		echo elgg_view('kpax_leagues/league_view/teams', array(
				'objKpax' => $objKpax,
				'objLeague' => $objLeague,
				'leagueEntity' => $leagueEntity
			));
	break;
	case "createteam":
		echo elgg_view('kpax_leagues/league_view/createteam', array(
				'objKpax' => $objKpax,
				'objLeague' => $objLeague,
				'leagueEntity' => $leagueEntity
			));
	break;
	case "editicon":
		echo elgg_view('kpax_leagues/league_view/editicon', array(
				'objKpax' => $objKpax,
				'objLeague' => $objLeague,
				'leagueEntity' => $leagueEntity
			));
	break;
	case "cropicon":
		echo elgg_view('kpax_leagues/league_view/cropicon', array(
				'objKpax' => $objKpax,
				'objLeague' => $objLeague,
				'leagueEntity' => $leagueEntity
			));
	break;
	default:
		echo elgg_view('kpax_leagues/league_view/overview', array(
				'objKpax' => $objKpax,
				'objLeague' => $objLeague,
				'leagueEntity' => $leagueEntity
			));
	break;
}
?>