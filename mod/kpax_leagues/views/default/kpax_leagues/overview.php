<?php
echo elgg_view_title(elgg_echo('kpax_leagues:title'));
echo elgg_view('kpax_leagues/league_list/createbutton');
echo elgg_view('kpax_leagues/tabs');

$filter = get_input('filter');

if($filter == '')
{
	$filter = 'all';
}

$objKpax = new kpaxSrv(elgg_get_logged_in_user_entity()->username);
$leaguesList = $objKpax->getLeagues($filter);

$orderBy = 'start';

//$leaguesList = objectToArray($leaguesList);
kpaxArraySortByComparer($leaguesList, function($a, $b) use ($orderBy){
	if($a->$orderBy > $b->$orderBy)
	{
		return -1;
	}
	elseif($a->$orderBy < $b->$orderBy)
	{
		return 1;
	}
	return 0;
});

$total = 0;
foreach($leaguesList as $league)
{
	$tmpLeague = new League($league->idLeague);
	$tmpLeague->setInfo($league);
	echo elgg_view('kpax_leagues/league_box/box', array('league' => $tmpLeague, 'objKpax' => $objKpax));
	$total++;
}

if($total == 0)
{
	echo(elgg_echo('kpax_leagues:no_leagues'));
}
?>