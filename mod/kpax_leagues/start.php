<?php

elgg_register_event_handler('init', 'system', 'kpax_leagues_init');

function kpax_leagues_init(){
	$root = dirname(__FILE__);

	elgg_register_page_handler('kpax_leagues', 'kpax_leagues_page_handler');

	//CSS
	elgg_extend_view('css/elgg', 'css/kpax_leagues/style');

	//Javascript
	elgg_register_simplecache_view('js/kpax_leagues/scripts');
	$url = elgg_get_simplecache_url('js', 'kpax_leagues/scripts');
	elgg_register_js('leagues_scripts', $url);

	//Libs
	elgg_register_library('elgg:kpax_leagues_classes:league', "$root/lib/classes/League.php");
	elgg_load_library('elgg:kpax_leagues_classes:league');

	// actions
    $action_path = "$root/actions";
	elgg_register_action('kpax_leagues/create', "$action_path/create.php");
	elgg_register_action('kpax_leagues/edit', "$action_path/edit.php");
	elgg_register_action('kpax_leagues/editnews', "$action_path/editnews.php");
	elgg_register_action('kpax_leagues/editicon', "$action_path/editicon.php");
	elgg_register_action('kpax_leagues/cropicon', "$action_path/cropicon.php");
	elgg_register_action('kpax_leagues/create_team', "$action_path/createteam.php");

	//Menú Tab
	elgg_register_menu_item('site', array(
        'name' => 'leagues',
        'text' => elgg_echo('kpax_leagues:title'),
        'href' => 'kpax_leagues/overview'
    ));
}


function kpax_leagues_page_handler($page){
	$pages = dirname(__FILE__) . '/pages/kpax_leagues';
	$actions = dirname(__FILE__) . '/actions';

	elgg_push_breadcrumb(elgg_echo('kpax_leagues:title'), 'kpax_leagues/overview');
	elgg_load_js('leagues_scripts');

	switch ($page[0]) {
		case "view":
			set_input('idLeague', $page[1]);
			set_input('tab', $page[2]);
			include("$pages/league_view.php");
		break;
		case "create":
			include("$pages/create.php");
		break;
		case "edit":
			set_input('idLeague', $page[1]);
			include("$pages/edit.php");
		break;
		case "delete":
			set_input('idLeague', $page[1]);
			include("$actions/delete.php");
		break;
		case "leave":
			set_input('idLeague', $page[1]);
			include("$actions/leave.php");
		break;
		case "join":
			set_input('idLeague', $page[1]);
			include("$actions/join.php");
		break;
		case "lock":
			set_input('idLeague', $page[1]);
			include("$actions/lock.php");
		break;
		case "unlock":
			set_input('idLeague', $page[1]);
			include("$actions/unlock.php");
		break;
		case "teamjoin":
			set_input('idTeam', $page[1]);
			include("$actions/teamjoin.php");
		break;
		case "teamleave":
			set_input('idTeam', $page[1]);
			include("$actions/teamleave.php");
		break;
		case "teamremove":
			set_input('idTeam', $page[1]);
			include("$actions/teamremove.php");
		break;
		case "teamkick":
			set_input('idTeam', $page[1]);
			set_input('idUser', $page[2]);
			include("$actions/teamkick.php");
		break;
		case "teamassign":
			set_input('idTeam', $page[1]);
			set_input('idUser', $page[2]);
			include("$actions/teamassign.php");
		break;
		default:
			set_input('filter', $page[1]);
			include("$pages/overview.php");
		break;
	}

	return true;
}
?>