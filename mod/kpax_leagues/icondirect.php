<?php

require_once(dirname(dirname(dirname(__FILE__))). '/engine/settings.php');

global $CONFIG;

$mysql_dblink = @mysql_connect($CONFIG->dbhost, $CONFIG->dbuser, $CONFIG->dbpass, true);
if ($mysql_dblink) {
	if (@mysql_select_db($CONFIG->dbname, $mysql_dblink)) {
		$result = mysql_query("select name, value from {$CONFIG->dbprefix}datalists where name='dataroot'", $mysql_dblink);
		if ($result) {
			$row = mysql_fetch_object($result);
			while ($row) {
				if ($row->name == 'dataroot') {
					$data_root = $row->value;
				}
				$row = mysql_fetch_object($result);
			}
		}

		@mysql_close($mysql_dblink);

		if(isset($data_root)){
			$idLeague = (int)$_GET['idLeague'];
			$size = $_GET['size'];

			if($size == 'cache'){
				$filename = "$data_root/kpax/cache/league_".$idLeague.".png";
			}
			else{
				$filename = "$data_root/kpax/leagues/".$idLeague.$size.".png";
			}

			header('Content-type: image/png');
			readfile($filename);
			exit;
		}
	}
}

?>