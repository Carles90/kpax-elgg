<?php
/**
 * View a bookmark
 *
 * @package ElggBookmarks
 */
$kpax = get_entity(get_input('guid'));
$guid = (int)get_input('guid');

$crumbs_title = $page_owner->name;

$title = $kpax->title;

if($title == ''){
	$content = elgg_echo('kpax:game_not_exists');
}
else{
	elgg_push_breadcrumb($title);

	$content = elgg_view_title($title);
	$content .= elgg_view('kpax/game/tabs', array('selected' => $page[2]));

	elgg_load_js('ach_scripts');

	switch($page[2])
	{
		case "comments":
			$content .= elgg_view_comments($kpax);
			break;
		case "achievements":
			if(elgg_view_exists('kpax_ach/game/ach')){
				$content .= elgg_view('kpax_ach/game/ach');
			}
			else{
				$content .= elgg_echo('kpax:mod_under_housekeeping');
			}
			break;
		default:
			$content .= elgg_view_entity($kpax, array('full_view' => true));
			break;
	}
}
$body = elgg_view_layout('content', array(
    'content' => $content,
    'title' => $title,
    'filter' => '',
    'header' => '',
        ));

echo elgg_view_page($title, $body);
?>