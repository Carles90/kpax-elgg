<?php
$title = "Test Page";

elgg_push_breadcrumb($title);

//MENÚ SUPERIOR DE PESTANYES
$tabs = array(
	'newest' => array(
		'text' => 'Option 1',
		'href' => 'kpax/testview/opt1',
		'priority' => 200,
	),
	'popular' => array(
		'text' => 'Option 2',
		'href' => 'kpax/testview/opt2',
		'priority' => 300,
	),
	'discussion' => array(
		'text' => 'Option 3',
		'href' => 'kpax/testview/opt3',
		'priority' => 400,
	),
);

foreach ($tabs as $name => $tab) {
	$tab['name'] = $name;

	if ($vars['selected'] == $name) {
		$tab['selected'] = true;
	}

	elgg_register_menu_item('filter', $tab);
}

$menu = elgg_view_menu('filter', array('sort_by' => 'priority', 'class' => 'elgg-menu-hz'));

//CONTINGUT

$content = $menu;
$content .= elgg_echo('kpax:testview_content');

$params = array(
'content' => $content
);

$body = elgg_view_layout('content', array(
    'content' => $content,
    'title' => $title,
    'filter' => '',
    'header' => '',
        ));

echo elgg_view_page($title, $body);

?>