<?php
function kpaxNumberFormat($number, $decimals = 0)
{
	return number_format($number, $decimals, elgg_echo('kpax:number_format_decimal'), elgg_echo('kpax:number_format_thousands'));
}

function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}

function kpaxGetDataRoot(){
	$path = elgg_get_data_path();
	$data_root = $path.'kpax/';
	if(!is_dir($data_root)){
		mkdir($data_root);
	}

	return $data_root;
}

// Comprova si l'arxiu que s'ha enviat és realment una imatge
function kpaxIsImage($file){
	//Comprovar el tipus indicat per PHP
	if(!stristr($file['type'], 'image')){
		return false;
	}

	//Verificar la extensió de l'arxiu
	$ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));

	if($ext != 'png' && $ext != 'jpg' && $ext != 'jpeg' && $ext != 'gif'){
		return false;
	}

	//Verificar segons el tipus MIME
	$imageinfo = getimagesize($file['tmp_name']);
	$mime = $imageinfo['mime'];

	if($mime != 'image/gif' && $mime != 'image/jpeg' && $mime != 'image/jpg' && $mime != 'image/png'){
		return false;
	}

	//Verificar que l'arxiu no tingui un doble tipus
	if(substr_count($file['type'], '/') > 1){
		return false;
	}

	return true;
}

//Converteix qualsevol tipus d'imatge a PNG
function kpaxConvertImageToPng($src, $target){
	//Verificar la extensió de l'arxiu
	$ext = strtolower(pathinfo($src['name'], PATHINFO_EXTENSION));

	if($ext == 'jpg' || $ext == 'jpeg'){
		$img = imagecreatefromjpeg($src['tmp_name']);
	}
	elseif($ext == 'gif'){
		$img = imagecreatefromgif($src['tmp_name']);
	}
	elseif($ext == 'png'){
		$img = imagecreatefrompng($src['tmp_name']);
	}
	else
	{
		return false;
	}

	return imagepng($img, $target);
}

function kpaxArraySortByComparer(&$array, $comparer)
{
	uasort($array, $comparer);
}

function kpaxNow()
{
	return time() * 1000;
}

function kpaxUnixToDateTime($timestamp)
{
	$timestamp = $timestamp / 1000;
	return date(elgg_echo('kpax:date_format'), $timestamp).' '.elgg_echo('kpax:date_at').' '.date(elgg_echo('kpax:hour_format'), $timestamp);
}

function kpaxUnixToDate()
{
	$timestamp = $timestamp / 1000;
	return date(elgg_echo('kpax:date_format'), $timestamp);
}

function kpaxCutText($text, $length)
{
	$text = strip_tags($text);
	if(strlen($text) > $length)
	{
		$text = substr($text, 0, $length).'...';
	}

	return $text;
}
?>