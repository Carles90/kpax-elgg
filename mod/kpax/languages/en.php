<?php

/**
 * Bookmarks English language file
 */
$english = array(
    /**
     * Menu items and titles
     */
    'kpax:all' => "All products",
    'kpax:add' => "Add game",
    'kpax:save:success' => "save product",
    'kpax:save:failed:entity' => "Error get entity",
    'kpax:save:failed' => "Fail save product",
    'kpax:delete:failed' => "delete failed product",
    'kpax:delete:success' => "delete product",
    'kpax:none' => "No products",
    'kpax:unknown_kpax' => "unknow product",
    'kpax:friends' => "friends product",
    'kpax:owner' => "owner product",
    'kpax:edit' => "edit product",
    'kpax:game:score' => "Game score",

    'kPAX:play' => "Games",
    'kPAX:devs' => "Develop",
    'kPAX:add' => "Add game",
    'kPAX:game:name' => "Game name (*)",
    'kPAX:game:description' => "Description",
    'kPAX:game:skills' => "Skills (*)",
    'kPAX:game:category' => "Game category/ies",
    'kPAX:game:platforms' => "Available platforms",
    'kPAX:game:creationDate' => "Creation date",
    'kPAX:game:tags' => 'Related tags',
    'kPAX:game:csr_file' => "Certificate request file [.csr] (*)",
    'kPAX:game:send' => "Send my game!",
    'kPAX:myGames' => "My games",
    'kPAX:noGames' => "You do not have games in kPAX yet.",
    'kPAX:my_dev_games' => "My (developed) games",
    'kpax:tagline' => 'Play seriously!',
    'kpax:dragdrop' => 'You can drag and drop an image from the desktop',
	'kpax:testview_content' => 'This is a test page for kPAX network',
	'kpax:game_overview' => 'Game Overview',
	'kpax:game_comments' => 'Comments',
	'kpax:game_leagues' => 'Leagues',
	'kpax:edit_game' => 'Edit Game',
	'kpax:game_achievements' => 'Achievements',
    'kpax:mod_under_housekeeping' => 'We\'re sorry, this page is currently under housekeeping.',
    'kpax:game_not_exists' => 'We\'re sorry, the selected game does not exist.',
	
	/**
	 * Language Options
	 */
	'kpax:number_format_decimal' => '.',
	'kpax:number_format_thousands' => ',',
    'kpax:date_format' => 'm/d/Y',
    'kpax:hour_format' => 'H:i:s',
    'kpax:date_at' => 'at'
);

add_translation('en', $english); 

?>