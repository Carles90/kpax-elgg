<?php
$guid = (int)get_input('guid');
$kpax = get_entity($guid);

$tabs = array();
$tabs['overview'] = array(
		'text' => elgg_echo('kpax:game_overview'),
		'href' => 'kpax/view/'.$guid.'',
		'priority' => 1
	);
$tabs['comments'] = array(
		'text' => elgg_echo('kpax:game_comments'),
		'href' => 'kpax/view/'.$guid.'/comments',
		'priority' => 2
	);

if(elgg_view_exists('kpax_ach/game/ach')){
	$tabs['achievements'] = array(
			'text' => elgg_echo('kpax:game_achievements'),
			'href' => 'kpax/view/'.$guid.'/achievements',
			'priority' => 4
		);
}
	
if($kpax->canEdit())
{
	$tabs['edit'] = array(
			'text' => elgg_echo('kpax:edit_game'),
			'href' => 'kpax/view/'.$guid.'/edit',
			'priority' => 5
		);
}


foreach ($tabs as $name => $tab) 
{
	$tab['name'] = $name;
	if ($vars['selected'] == $name) {
		$tab['selected'] = true;
		$selected = true;
	}
	
	elgg_register_menu_item('filter', $tab);
}

$menu = elgg_view_menu('filter', array('sort_by' => 'priority', 'class' => 'elgg-menu-hz'));
echo($menu);
?>