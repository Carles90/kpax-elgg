<?php

// Generat per la traducció del navegador 

$catalan = array( 
	 'blog'  =>  "Bloc" , 
	 'blogs'  =>  "Blocs" , 
	 'blog:user'  =>  "Bloc de %s" , 
	 'blog:user:friends'  =>  "Bloc d'amistats de %s" , 
	 'blog:your'  =>  "Bloc personal" , 
	 'blog:posttitle'  =>  "Bloc de %s: %s" , 
	 'blog:friends'  =>  "Blocs de les teves amistats" , 
	 'blog:yourfriends'  =>  "Blocs recents de les teves amistats" , 
	 'blog:everyone'  =>  "Tots els blocs de la xarxa" , 
	 'blog:read'  =>  "Llegir bloc" , 
	 'blog:addpost'  =>  "Escriure notícia al bloc" , 
	 'blog:editpost'  =>  "Edita el missatge del bloc" , 
	 'blog:text'  =>  "Text del bloc" , 
	 'blog:strapline'  =>  "%s" , 
	 'item:object:blog'  =>  "Comentaris en el blog" , 
	 'blog:never'  =>  "mai" , 
	 'blog:preview'  =>  "Pre-visualitzar" , 
	 'blog:draft:save'  =>  "Desar esborrany" , 
	 'blog:draft:saved'  =>  "Darrer esborrany" , 
	 'blog:comments:allow'  =>  "Acceptar comentaris" , 
	 'blog:preview:description'  =>  "Això és una pre-visualització del seu enviament." , 
	 'blog:preview:description:link'  =>  "Continuar editant o enregistrar el seu enviament, clica aquí." , 
	 'blog:river:created'  =>  "%s ha escrit" , 
	 'blog:river:updated'  =>  "%s actualitzat" , 
	 'blog:river:posted'  =>  "%s enregistrat" , 
	 'blog:river:create'  =>  "una nova entrada titulada:" , 
	 'blog:river:update'  =>  "títol de la notícia" , 
	 'blog:river:annotate'  =>  "un comentari a la notícia:" , 
	 'blog:posted'  =>  "La notícia ha estat desada al bloc" , 
	 'blog:deleted'  =>  "La notícia ha estat eliminada del bloc" , 
	 'blog:save:failure'  =>  "La notícia no pot ser desada. Prova de nou o posat en contacte amb el grup administrador" , 
	 'blog:moreblogs'  =>  "Més entrades de bloc" , 
	 'blog:numbertodisplay'  =>  "Nombre de notícies del bloc a desplegar" , 
	 'blog:blank'  =>  "Has d'omplir el títol i el cos del missatge per a poder enviar-lo." , 
	 'blog:notfound'  =>  "No trobem cap enviament a aquest bloc. Prova una altra cerca." , 
	 'blog:notdeleted'  =>  "No podem esborrar aquest enviament" , 
	 'blog:newpost'  =>  "Nova notícia del bloc desada" , 
	 'blog:via'  =>  "via blocs" , 
	 'blog:enableblog'  =>  "Habilitar bloc del grup" , 
	 'blog:group'  =>  "Blog del grup" , 
	 'blog:error'  =>  "Alguna cosa ha anat malament, prova de nou." , 
	 'groups:enableblog'  =>  "Habilitar blog del grup" , 
	 'blog:nogroup'  =>  "Aquest grup no té cap post encara" , 
	 'blog:more'  =>  "Més comentaris del bloc" , 
	 'blog:read_more'  =>  "Llegir notícia sencera" , 
	 'blog:widget:description'  =>  "Aquest widget mostra les seves darreres entrades al bloc
" , 
	 'blog:conversation'  =>  "Conversa"
); 

add_translation('ca', $catalan); 

?>